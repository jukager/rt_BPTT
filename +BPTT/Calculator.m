% Class for calculator objects
% Last modified AGO 18.05.2014
% y = f(x,p)
% See also
% 
% Reference page in Help browser 
% <a href="matlab:doc BPTT.Calculator;">doc BPTT.Calculator</a>
%  

classdef Calculator < handle
% Calculators are capable of performing calculations using the TimeVariable
% object of a Bioprocess (or any other experiment object for that matter).
% Calculators are designed to be added to Bioprocess objects and do not
% function as a stand-alone object.
    
    properties
        
        Name                        % Name of the calculator
        Version=1                   % Version of the calculator; default = 1, Prediction = 2, only online = 3     
        x                           % Name of one or more input Variables
        f                           % Handle to a function e.g. @BPTT.calculators.calc_offgas
        p                           % Parameters required by the function
        o                           % Handle to the Bioprocess object where the calculator will be added to
        deadband=5                 % Time interval between lastinputtime and lastoutputtime before calculation is triggered [seconds] 
        Quality                     % Quality code of input data (calculator will be applied only to data that satisfy the quality
        QualityExt                  % External Quality criteria e.g. {'data<0',0,'data>=0',1}
        QualityExtVec               % External Quality Vector (used for mirroring)
        Mode= 'offline'             % Mode of the calculator: 'offline' vs. 'online'. In offline mode the calculator will always use the entire input data for its calculation. In online mode the calculator monitors its input data and can calculate only for newly arrived data.
        minpoints=1;                % Minimum number of data points for a partial calculation to take place
        pastpoints=0;               % Number of historical points to use for a partial calculation
        calctimer=[0 0];            % Matrix to hold a history of calculation times
        OutputType='calculated';    % Type of the calculator output
        persist=struct;             % a persistent structure
        y                           % Output of the calculator
        Forced= 0                   % Set to 1 forces calculator to calculate after a predefined deadeband default = 0 (not forced)
        
        
    end
    
    properties (SetAccess = private)
        
        ModificationTime = now;                     % Modification time of the calculator object
        CreationTime = now;                         % Creation time of the calculator object
        CalculationTime = 0;                        % The last time when outputs were calculated
         LastOutputTime                          % Last time point of outputs
        LastInputModificationTime = 0;              % Last time one of the inputs were modified
        
        
    end
    
    properties (SetAccess = private, Dependent = true)
       
        LastInputTime           % Last time point of inputs
        x_ts                    % Input TimeVariable (time series) objects for the calculator obtained from a bioprocess object   
        
    end 
    
    methods
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Calculator()
        % Function for creating a calculator
            obj.CreationTime = now;  
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function y = get.y(obj)
            % Calculates the output of the calculator if necessary!
            % check whether outputs need to be re-calculated or not

            if isempty(obj.y) || obj.ModificationTime > obj.CalculationTime || (strcmp(obj.Mode,'offline') && (obj.LastInputModificationTime > obj.CalculationTime) )
                
                disp(['>>> Calculating (all): ' obj.Name ' ' obj.o.Name])
                
                input_ts = obj.x_ts; % input_ts is a structure that contains the input TimeVariables to be calculated
                
                tic
                % perform the calculation
                [y] = obj.f( input_ts , obj); 
                % keeping track of the calculations and the required time
                calc_time = toc;
                obj.calctimer = [obj.calctimer ; now , calc_time];

                
                % add the name of the calculator to the outputs
                 output_names = fieldnames(y);
                 for i=1:length(output_names)
                     y.(output_names{i}).Calculator = obj.Name;
                     y.(output_names{i}).Type = obj.OutputType;
                     y.(output_names{i}).ModificationTime = now;
                     
                     % add calculator outputs to the bioprocess object
                     obj.o.Variables.(output_names{i}) = y.(output_names{i});
                 end
                
                % set the output value of the calculator object
                obj.y = y;
                
                obj.CalculationTime = now;
                obj.setLastOutputTime(y);
            
            else
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Online calculation mode calculates the output only for new data if new data has
                % been added to the inputs and if the mode of the calculator has been specified as 'online'
                if strcmp(obj.Mode,'online')
                         
                     if (obj.LastInputTime - obj.LastOutputTime) > (obj.deadband / 24 / 3600) && obj.LastInputModificationTime > obj.CalculationTime %|| obj.Version == 2  && obj.LastInputTime -(obj.LastOutputTime) > (obj.deadband / 24 / 3600) 
                     % conditions for execution of online calculations
                     % including the assignment of deadband
                     % obj.Forced == 1 && ((now-obj.calctimer(end)) > obj.deadband/(3600*24))
                     %((obj.calctimer((end-1),1)-obj.calctimer((end),1))*24*3600) > (obj.deadband) && 
                     %if (obj.LastInputTime - obj.LastOutputTime) > (obj.deadband / 24 / 3600) &&  obj.LastInputModificationTime > obj.CalculationTime || ((now-obj.calctimer((end),1))*24*3600) > (obj.deadband) && obj.Forced(true)
                     %if obj.LastInputModificationTime > obj.CalculationTime
                     %if obj.LastInputTime > obj.CalculationTime
                     %if obj.LastInputTime > obj.LastOutputTime

                         x_ts = obj.x_ts;                   % get the input timeseries objects from the calculator object itself
                         input_names = fieldnames(x_ts);    % extract the names of input variables of the calculator

                         % loop through input timeseries to extract the new data
                         numnewdata = zeros(length(input_names),1); % matrix to hold number of new data points
                
                        for i=1:length(input_names)

                            % extract only new data, but look a bit behind 

                            % first extract the new data
                     %if obj.Version == 2
                      %obj.LastOutputTime = (obj.LastOutputTime-(obj.p.Prediction/24));     
                      %end    
                       % if obj.Version == 2    
                         %   partial_input.(input_names{i}) = getsampleusingtime(x_ts.(input_names{i}), (obj.LastOutputTime-(obj.p.Prediction/24)), obj.LastInputTime);
                        %else
                            partial_input.(input_names{i}) = getsampleusingtime(x_ts.(input_names{i}), (obj.LastOutputTime) , obj.LastInputTime);
                        %end   
                            partial_input.(input_names{i}).Name = input_names{i};

                       numnewdata(i) = partial_input.(input_names{i}).Length;     
                            % extracting past calculated data for the new calculation
                            if obj.pastpoints > 0  % check whether it is needed to get any past points
                                
                                % get the inputs between beginning and the last time any outptut has been calculated
                                calculated_data = getsampleusingtime(x_ts.(input_names{i}), 0, obj.LastOutputTime);

                                if calculated_data.Length > obj.pastpoints
                                    
                                    m1 = calculated_data.Length;    % endpoint of the new data                                    
                                    m2 = m1 - obj.pastpoints + 1;   % start point of the new data
                                    
                                    s.data = calculated_data.Data(m2:m1);
                                    s.time = calculated_data.Time(m2:m1);
                                    s.overwriteflag = true;

                                    % add the last point of the input
                                    partial_input.(input_names{i}) = addsample(partial_input.(input_names{i}),s);
                                end
                                if obj.pastpoints == 1e64 % the case where all of the pastpoints are given to the calculator
                                    
                                    	%m1 = calculated_data.Length;    % endpoint of the new data                                    
                                        %m2 = 1;                         % start point of the new data

                                        %s.data = calculated_data.Data(m2:m1);
                                        %s.time = calculated_data.Time(m2:m1);
                                        %s.overwriteflag = true;
                                        %partial_input.(input_names{i}) = addsample(partial_input.(input_names{i}),s);
                                        
                                        %TS2 = BPTT.TimeVariable;
                                        %TS2.Time = calculated_data.Time(m2:m1);
                                        %TS2.Data = calculated_data.Data(m2:m1);
                                        %TS2.Name = partial_input.(input_names{i}).Name;
                                        partial_input.(input_names{i}) = x_ts.(input_names{i});

                                end
                                
                            end

                            

                        end
                         minpoints_logic = (numnewdata < obj.minpoints); % creates logic matrix witch has to be 0 in all cases 
                         % do not perform the partial calculation if the inputs do not have enough data points
                         %if sum(numnewdata >=  obj.minpoints) == length(numnewdata)
                         if minpoints_logic == 0 %sum(numnewdata >=  obj.minpoints) == length(numnewdata)
                            
                            % Performing the actual partial calculation
                            disp(['>>> Calculating (new data): ' obj.Name ' ' obj.o.Name])
                            
                            % calling the calculator function
                            tic
                            y_partial = obj.f( partial_input , obj);
                            % keeping track of the calculation time
                            calc_time = toc;
                            obj.calctimer = [obj.calctimer ; now , calc_time];
                            
                            % get the previously calculated values
                            y_existing = obj.y;
                            y = y_existing;
                             
                             output_names = fieldnames(y_partial);

                             for i=1:length(output_names)
                                 
                                 %try
                                    
                                 % remove any outputs which have been
                                 % calculated previously JKA deactivated
                                 % 31.03.2016
                                 idx = find(y_partial.(output_names{i}).Time - obj.LastOutputTime<=1.1574e-05); % a second in hours to avoid duplicate items
                                 y_partial.(output_names{i}) = delsample(y_partial.(output_names{i}),'Index',idx);
                                 
                                 % deletes already existing calculates so that  
                                 % new calculates are given to the
                                 % bioprocess
                                 if length(y_partial.(output_names{i}).Time)>= 1;
                                 idx = find(y_existing.(output_names{i}).Time > y_partial.(output_names{i}).Time(1));
                                 y_existing.(output_names{i}) = delsample(y_existing.(output_names{i}),'Index',idx);
                                 end
                                 if y_partial.(output_names{i}).Length > 0
                                 
                                     % for multidimensional signals use the traditional addsample method, otherwise use the alternative method which is faster!
                                     if size(y_partial.(output_names{i}).Data,2)>1
                                         %preparing a structure to add the new calculated values using the addsample function
                                         s.data = y_partial.(output_names{i}).Data;
                                         s.time = y_partial.(output_names{i}).Time;
                                         s.overwriteflag = false;
                                         y.(output_names{i}) = addsample(y_existing.(output_names{i}) , s);

                                     else % this method is just faster, but wont work for multidimensional y variables for unknown reasons!
                                         
                                         y.(output_names{i}) = delsample(y.(output_names{i}),'Index',1:y.(output_names{i}).Length);
                                         y.(output_names{i}).Time = [y_existing.(output_names{i}).Time ; y_partial.(output_names{i}).Time];
                                         y.(output_names{i}).Data = [y_existing.(output_names{i}).Data ; y_partial.(output_names{i}).Data];
                                         

                                     end
                                    
                                     % update the modification time of the output
                                     y.(output_names{i}).ModificationTime = now;
                                      % add calculator outputs to the bioprocess object
                                     obj.o.Variables.(output_names{i}) = y.(output_names{i});
                                 end                                   

                                 %catch exception 
                                     %y.(output_names{i}) = obj.y.(output_names{i});

                                     % display the error and abort
                                     %warning(exception.message);
                                 %end

                             end
                             % update the LastOutputTime property
                             obj.setLastOutputTime(y);

                             % get the new y data into the object
                             obj.y = y;
                             obj.CalculationTime = now;
                          else % else for the case when inputs do not have enough points
                              % just return the original values nothing will be calculated here.
                              y = obj.y;
                              %obj.CalculationTime = now;
                         end
                      else % JKA wieder activiert da ansonster y nicht in calculator structur gesetzt wird
                         % just return the original values nothing will be calculated here.
                         y = obj.y;
                        
                     end
                 else
                     % just return the original values nothing will be calculated here.
                     y = obj.y;
                     
                 end
            end
            
            % setting external quality if specified / overwrites internal
            % quality from the calculator function file. Apply to all
            % outputs of a calculator? Yes, for now. Supports only one
            % criterial condition for now.
            
            if ~isempty(obj.QualityExt)
                fn = fieldnames(y);
                for i=1:length(fn) % loop through all outputs of the calculator
                    varname = fn{i};
                    data = y.(varname).Data;
                    time = y.(varname).Time;
                    
                    criteria = obj.QualityExt{1};
                    qualitycode = obj.QualityExt{2};
                    
                    idx = eval(criteria); % get the index of time/data points where the condition is satisfied
                    y.(varname).Quality = ones(length(data),1).*-128;
                    y.(varname).Quality(idx)=qualitycode; % apply the quality code
                    
                end 
             
            % applying external quality vector for mirroring based on
            % time/quality vec of anothe timevariable object.
            
            elseif isempty(obj.QualityExt) && ~isempty(obj.QualityExtVec)
                
                fn = fieldnames(y);
                for i=1:length(fn) % loop through all outputs of the calculator
                    varname = fn{i};
                    time1 = y.(varname).Time; % time of the current output
                    time2 = obj.QualityExtVec.Time; % time of the external variable
                    quality2 = obj.QualityExtVec.Quality; % quality of the external variable
                    quality1 = interp1(time2, quality2, time1 , 'nearest'); % interpolate to get quality
                    quality1(isnan(quality1)) = -128; % replace the nans
                    y.(varname).Quality=quality1; % apply the quality code

                end
                
            end
            
            
        end % end of function    
                     
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        function LastInputTime= get.LastInputTime(obj)
        % function for monitoring input variables and updating the last availabe time stamp   
            
            LastInputTime = 0;
            % loop through input time series
            x_ts_local = obj.x_ts;
            
            input_names = fieldnames(x_ts_local);
            for i=1:length(input_names)
                if LastInputTime < max(x_ts_local.(input_names{i}).Time)
                    LastInputTime = max(x_ts_local.(input_names{i}).Time);
                end  
            end
           LastInputTime = LastInputTime; 
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function LastInputModificationTime = get.LastInputModificationTime(obj)
        % Function for monitoring input variables and updating the last modification time of inputs 
            %obj.Name
            LastInputModificationTime = 0;
            
            x_ts_local = obj.x_ts;  % get the inputs
            input_names = fieldnames(x_ts_local);
            
            for i=1:length(input_names) % loop through input time series
                
                 if LastInputModificationTime < x_ts_local.(input_names{i}).ModificationTime;
                     LastInputModificationTime = x_ts_local.(input_names{i}).ModificationTime;
                 end
            end
            LastInputModificationTime = LastInputModificationTime;
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function  setLastOutputTime(obj , y) 
        % function for monitoring output variables and updating the last availabe time stamp
            
        LastOutputTime = 0;
            output_names = fieldnames(y);
            
            if isfield(obj.p,'Prediction') % sets prediction field to zero if non existend
                for i=1:length(output_names) % determine last real-time output; excluding prediction
                if LastOutputTime < (max(y.(output_names{i}).Time)-(obj.p.Prediction/24));
                    LastOutputTime = max(y.(output_names{i}).Time)- (obj.p.Prediction/24);
                end
                end
                    else
            
            for i=1:length(output_names) % determine last real-time output; excluding prediction
                if LastOutputTime < max(y.(output_names{i}).Time);
                    LastOutputTime = max(y.(output_names{i}).Time);
                end
            end
            end
            obj.LastOutputTime = LastOutputTime;

        end
        
        %% function for getting the time series (TimeVariable) object for
        % inputs out of the Bioprocess object
        function x_ts = get.x_ts(obj)
            % get the variables using the getvars function of the Bioprocess class
            %disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
            %disp(['GETTING the following Xs for calculator: ' obj.Name] )
            
            x_ts = obj.o.getvars(obj.x,'Quality',obj.Quality);

        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function set.Name(obj,Name)    
           obj.Name = Name;
           obj.ModificationTime = now; 
        end
         
        %% set function for Version
        function set.Version(obj,Version)
            obj.Version = Version;
            obj.ModificationTime = now; 
        end
        
        %% set function for inputs x
        function set.x(obj,x)
            obj.x = x;
            obj.ModificationTime = now; 
        end            
        
        %% set function function file f
        function set.f(obj,f)
            obj.f = f;
            obj.ModificationTime = now; 
        end            
        
        %% set function for parameters p
        function set.p(obj,p)
            obj.p = p;
            %obj.ModificationTime = now; 
        end 
        
        %% set function for Quality
        function set.Quality(obj,Quality)
            obj.Quality = Quality;
            obj.ModificationTime = now; 
        end
        
        %% set function for Quality
        function set.QualityExt(obj,Quality)
            obj.QualityExt = Quality;
            obj.ModificationTime = now; 
        end 
        

        
    end
    
        
end