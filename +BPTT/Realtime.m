% This class fetches real-time data from an inCyght data Server through
% a REST Api (default) or from a Database
% To enable the functionality one has to set realtime_enable and define 
% realtime_url in the inCyght configuration file. 
%
% Please consult the Documentation file for a complete list of features and
% options or start by running one of the example files to try out this
% class.
%
% Author: Fabian Pollesboeck (FPO)
% If you have any questions, please contact fabian.pollesboeck@exputec.com
% Company: Exputec GmbH

classdef Realtime < handle
    
    properties
        api_url = '';       % URL to access DB
        header = []         % header to perform requests to webserver
    
        update_interval = 60          % Update interval in seconds
        process_start_time = 0      % Runtime of process in h since start
        
        update_calculators = true   % whether to automatically update calculators
        update_plots = true         % whether to automatically update plots
        keep_plot_view = true       % whether to keep zoom and view of user on plot
        
        opc_server = [];
        opc_fields = [];
        
        main_gui = [];              % main gui to get handles
        
        debug = false;              % set debug = true to have more output
    end
    
    properties (Access = private)
        bioprocesses = struct       % Holds all Bioprocesses selected by user
        timer_tag = 'update_realtime_data'  % tag to identify the realtime-timer
        DATETIME_EXACT = 'yyyy-MM-dd HH:mm:ss.SSS' % matlab datetime format str up to microseconds
        DATETIME_DB = 'yyyy-MM-dd''T''HH:mm:ss.SSSSSSXXX'  % postgresql json timestamp format
    end
    
    methods
        
        % =============== Constructor ======================
        function obj = Realtime(api_url, varargin)
            if nargin >= 1
                obj.api_url = api_url;
            end
            if nargin >= 3
                obj.set_authentication(varargin{1}, varargin{2});
            end
        end
        
        
        %  ============== Generic function =====================
        function obj = clear_bioprocesses(obj)
            obj.stop();
            obj.bioprocesses = [];
        end
        
        % ================ REST Functions =====================
        
        function data = compress(~, data)
            data = BPTT.tools.m2json(data);
            data = zlibencode(data);
        end
        
        function data = decompress(~, data)
            data = zlibdecode(data);
            data = char(data);
            data = BPTT.tools.json2m(data);
        end
        
        function obj = set_authentication(obj, username, password)
            encoder  = sun.misc.BASE64Encoder();
            str      = java.lang.String([username ':' password]); 
            
            obj.header = http_createHeader('Authorization',['Basic ' char(encoder.encode(str.getBytes()))]);
            obj.header(end+1) = http_createHeader('Content-Type','incyght-win/zlib_data');
            obj.header(end+1) = http_createHeader('Accept','incyght-win/zlib_data');
        end
        
        function ret = is_authenticated(obj)
            ret = ~isempty(obj.header);
        end
        
        function response = get_from_db(obj, url, varargin)
            % get paginate if it exists
            try
                paginate = varargin{find(strcmpi('paginate', varargin))+1};
            catch
                paginate = false;
            end
            
            [content, extras] = urlread2([obj.api_url url], 'GET', '', obj.header);
            
            if extras.isGood && extras.status.value == 200 && ~isempty(content)
                content = obj.decompress(uint8(content));
                if isfield(content, 'results') && ~paginate
                    content = content.results;
                end
                
                if numel(content) > 0 && isa(content, 'cell')
                    response.content = cell2mat(content);
                else
                    response.content = content;
                end
            else
                response.content = [];
            end
            response.status = extras.status;
            response.isGood = extras.isGood;
        end
        
        function response = post_to_db(obj, data, url, varargin)
            method = 'POST';
            if numel(data) == 1 && isfield(data, 'id')
                if isa(data.id, 'char')
                    data.id = str2double(data.id);
                end
                if data.id > 0
                    url = [url num2str(data.id) '/'];
                    method = 'PUT';
                end
            end
            
            % get method parameter if it exists
            try
                method = varargin{find(strcmpi('method', varargin))+1};
            catch
            end
            
            if strcmp(method, 'DELETE')
                post_data = [];
            else
                post_data = obj.compress(data);
            end
            
            [content, status] = urlread2([obj.api_url url], method, post_data, obj.header);
            
            try
                response.content = obj.decompress(uint8(content));
            catch 
                response.content = content;
            end
            response.isGood = status.isGood;
            response.status = status.status;
        end
        
        
        function response = get_active_batches(obj, varargin)
            url = 'active=1';
            if ~isempty(varargin)
                url = [url '&' varargin{1}];
            end
            response = obj.get_all_batches(url);
        end
        
        function response = get_all_batches(obj, varargin)
            url = 'rtbatches/';
            if ~isempty(varargin)
                url = [url '?' varargin{1}];
            end
            response = obj.get_from_db(url);
            if response.isGood
                response = response.content;
            else
                response = [];
            end
        end
        
        function bp = create_bioprocess(obj, varargin)
            bp = []; %BPTT.Bioprocess
            if isa(varargin{1}, 'char')
                if strcmp(varargin{1}, 'reactor') || strcmp(varargin{1}, 'reactor_name')
                    condition = ['reactor_name=' varargin{2}];
                elseif strcmp(varargin{1}, 'batch') || strcmp(varargin{1}, 'batch_name')
                    condition = ['batch_name=' varargin{2}];
                else
                    condition = [];
                end
                ret = obj.get_active_batches(condition);
                if ~isempty(ret)
                    bp = obj.create_bioprocess_from_rtb(ret(1), varargin{:});
                end
            else
                bp = obj.create_bioprocess_from_rtb(varargin{1}, varargin{:});
            end
            
        end
        
        % Fetches all variables for given realtime-batch from REST-Server 
        % and saves references in private variable bioprocesses for later
        % updates
        function bp = create_bioprocess_from_rtb(obj, rtb, varargin)
            param_idx = find(strcmp(varargin, 'parameter'));
            if ~isempty(param_idx)
                if isa(varargin{param_idx + 1}, 'char')
                    parameter_names = strtrim(strsplit(varargin{param_idx + 1},','));
                else
                    parameter_names = varargin{param_idx + 1};
                end
            else
                parameter_names = [];
            end
            
            start_datenum = datenum(datetime(rtb.start_time));
            bp = BPTT.Bioprocess;
            bp.Process_Start_Time = start_datenum;
            bp.Name = BPTT.tools.rm_special_characters(rtb.reactor_name);
            bp.Comment = { rtb.name; ['Start: ' datestr(start_datenum)]};
            bp.db_id = rtb.id;
            
            obj.bioprocesses.(bp.Name).object = bp;
            obj.bioprocesses.(bp.Name).last_update = 0;
            obj.bioprocesses.(bp.Name).parameter_names = parameter_names;
    
            if obj.debug
                disp(['Getting initial data for ' bp.Name ' (this might take some time...)']);
            end
            obj.update_rest_data(bp);

        end
        
        function data_received = update_rest_data(obj, bp, varargin)
            param_idx = find(strcmp(varargin, 'parameter'));
            if ~isempty(param_idx)
                if isa(varargin{param_idx + 1}, 'char')
                    parameter_names = strtrim(strsplit(varargin{param_idx + 1},','));
                else
                    parameter_names = varargin{param_idx + 1};
                end
            else
                parameter_names = [];
            end
            
            if ~isempty(parameter_names)
                obj.bioprocesses.(bp.Name).parameter_names = parameter_names;
            else
                parameter_names = obj.bioprocesses.(bp.Name).parameter_names;
            end
            
            data_received = false;
            
            % create url to fetch 
            url = ['rtseries_light/?query_mode=db&batch_id=' int2str(bp.db_id)];
            
            % add last update timestamp to only fetch new data from server
            if obj.bioprocesses.(bp.Name).last_update > 0
                % get newest timestamp and add 1e-8 to not always get the
                % lastest series again
                iso_datestr = datestr(obj.bioprocesses.(bp.Name).last_update + 1e-8, 'yyyymmddTHHMMSS.FFF');
                url = [ url '&created__gt=' iso_datestr];
            end
                
            % add pre-selected parameter names
            if ~isempty(parameter_names)
                url = [ url '&parameter_names=' strjoin(parameter_names,',') ];
            end
            
            if obj.debug
                disp(['API Request: ' url]);
            end
            
            response = obj.get_from_db(url);
            
            if ~response.isGood
                warning(['Could not load data: ' response.content]);
                return
            end
            
            if isempty(response.content)
                if obj.debug
                    disp(['Got no new data for ' bp.Name]);
                end
                return
            end
            
            dataIn = response.content;
            if obj.debug
                disp([ num2str(numel(dataIn)) ' new datapoints received for ' bp.Name]);
            end
            data_received = true;
            
            % then add to bioprocess, object (faster in performance)
            ts_names = unique({dataIn.name});
%             ts_names = fieldnames(all_timeseries);
            for i=1:numel(ts_names)
                tmp_name = ts_names{i};
                
                ts_data = dataIn(strcmp({dataIn.name}, tmp_name));
                
                % If first time, create time series. Add new data to
                % existing timeseries.
                if ~isfield(bp.Variables, tmp_name)
                    % If timeseries does not exist, create new one
                    TS = BPTT.TimeVariable;
                    % Set timeseries name and Id
                    TS.Name = BPTT.tools.rm_special_characters(tmp_name);
                    TS.DataInfo.Units = ts_data(1).unit;
                    TS.TimeInfo.Units = 'days';
                else
                    TS = bp.Variables.(BPTT.tools.rm_special_characters(tmp_name));
                end
                
                try
%                     new_time = datenum(datetime({ts_data.created}, ...
%                             'InputFormat', obj.DATETIME_DB, ...
%                             'TimeZone', 'local'))';
                    new_time = datenum(datetime([ts_data.created],...
                            'ConvertFrom', 'posixtime', ...
                            'TimeZone', 'local'))';
                    new_data = cellfun(@double, {ts_data.value})';
                    % in case of update
                    if obj.bioprocesses.(bp.Name).last_update > 0 && ...
                            numel(TS.Time) > 0
                        % fix for not adding duplicate timeseries
                        j = 1;
                        while TS.Time(end) >= new_time(j)
                            j = j + 1;
                        end
                        new_time = [TS.Time; new_time(j:end)];
                        new_data = [TS.Data; new_data(j:end)]; 
                    end
                    
                    if isempty(TS.Quality)
                        TS.set('Time', new_time, 'Data', new_data);
                    else
                        last_idx = find(TS.Quality ~= -1);
                        last_idx = last_idx(end);
                        new_quality = numel(new_time) - numel(TS.Quality);
                        new_quality = [TS.Quality; ones(new_quality,1)*TS.Quality(last_idx)];
                        TS.set('Time', new_time, 'Data', new_data, 'Quality', new_quality);
                    end
                    
                    bp.AddVariable(TS);
                catch err
                    %BPTT.tools.catch_error(err); %unsopported in VTU
                    %Toolbox JKA
                end 
            end
            
            % update last_update for bioprocess
            % check if this timeseries contains newest timestamp
            % and save this timestamp as last_timestamp
            if numel(new_time) > 1 && ...
                    new_time(end) > obj.bioprocesses.(bp.Name).last_update
                obj.bioprocesses.(bp.Name).last_update = new_time(end);
            end
        end
        
        function status = write_setpoints(obj, bp, setpoints)
            status = obj.write_data(bp, setpoints, 'rtsetpoints/');
        end
        
        function status = write_data(obj, bp, values, varargin)
            if ~isempty(varargin)
                url = varargin{1};
            else
                url = 'rtseries/';
            end
            
            if isa(bp, 'BPTT.Bioprocess')
                data.batch = bp.db_id;
            else
                data.batch = bp;
            end
            
            status = [];
            if isa(values, 'BPTT.TimeVariable')
                for i=1:numel(values)
                    data.name = values(i).Name;
                    data.value = values(i).Data(end);
                    response = obj.post_to_db(data, url);
                    status = [status response];
                end     
            else
                for i=1:numel(values)
                    data.name = values(i).name;
                    data.value = values(i).value;
                    response = obj.post_to_db(data, url);
                    status = [status response];
                end
            end
        end
        
        
        % ================= OPC functions ==================
        function values = get_opc_values(obj)
            server = obj.opc_server;
            values = BPTT.python.pyopc_read(server, obj.opc_fields);
            
        end
        
        function bp = create_opc_bioprocess(obj, sel_field_idx, varargin)
            bp_idx = find(strcmp(varargin,'Bioprocess'));
            if ~isempty(bp_idx)
                bp = varargin{bp_idx+1};
            else
                bp = BPTT.Bioprocess('ProcessTime', obj.process_start_time);
                bp.Name = 'OPC_Realtime';
                bp.Comment = 'Realtime values via OPC';
            end
            obj.bioprocesses.(bp.Name).object = bp;
            
            fields = strsplit(obj.opc_fields, ';');
            obj.opc_fields = strjoin(fields(sel_field_idx), ';');

            bp_variables = fieldnames(bp.Variables);
            
            for i=1:numel(sel_field_idx)
                field_name = strrep(fields{i}, '.', '_');

                if ~any(strcmp(field_name, bp_variables))
                    tv = BPTT.TimeVariable;
                    tv.Name = field_name;
                    bp.AddVariable(tv);
                end 
            end
        end
        
        function new_data = update_opc_data(obj, bp)
            fields = strsplit(obj.opc_fields, ';');
            field_values = BPTT.python.pyopc_read(obj.opc_server, obj.opc_fields);
            field_time = now;
            
%             fields = fieldnames(bp.Variables);
            
            for i=1:numel(fields)
                field_name = strrep(fields{i}, '.', '_');
                field_value = field_values(i);
                
                if ~isfield(bp.Variables, field_name)
                    tv = BPTT.TimeVariable;
                    tv.Name = field_name;
                    bp.AddVariable(tv);
                end
                bp.AddSampletoVariable(field_name, field_time, field_value);
            end
            
            new_data = true;
        end
        
        
        % ================= TIMER functions ====================
        
        function obj = start(obj)
            % Start timer
            delete(timerfind('Tag', obj.timer_tag));
            
            % start update once
             %obj.update_all_data();
            
            t = timer;
            t.TimerFcn = @(~, ~)obj.update_all_data();
            if obj.update_interval > 0
                if obj.update_interval < 0.1
                    obj.update_interval = 0.1; % minimum update interval
                end
                t.StartDelay = obj.update_interval;
                t.Period = obj.update_interval;
                t.ExecutionMode = 'fixedSpacing';
                t.Tag = obj.timer_tag;
                start(t);
            end
        end
        
        function any_new_data = update_all_data(obj)
            any_new_data = false;
            % Request the data
            bp_names = fieldnames(obj.bioprocesses);
            
%             tic
            for i=1:numel(bp_names)
                % check if handle still valid, otherwise remove bioprocess
                if ~isvalid(obj.bioprocesses.(bp_names{i}).object)
                    obj.bioprocesses = rmfield(obj.bioprocesses, bp_names{i});
                    continue;
                end
                bioprocess = obj.bioprocesses.(bp_names{i}).object;
                
                % fetch new data
                new_data = false;
                try
                    new_data = obj.update_data(bioprocess);
                catch err
                    obj.useful_error(err);
                end
                
                % run calculators, if new data received
                if new_data
                    any_new_data = true;
                    
                    if obj.update_calculators
                        try
                            obj.run_calculators(bioprocess);
                        catch err
                            obj.useful_error(err);
                        end
                    end
                end
            end
%             toc

            % check if no valid bioprocesses are left, then stop timer
            if isempty(fieldnames(obj.bioprocesses))
                obj.stop();
            end
        end

        function new_data = update_data(obj, bp)
            new_data = false;
            
            if ~isempty(obj.api_url) && bp.db_id ~= 0
                new_data = obj.update_rest_data(bp);
            elseif ~isempty(obj.opc_server) && ~isempty(obj.opc_fields)
                new_data = obj.update_opc_data(bp);
            end
            
            if new_data
                %bp.AddVariable(bp.Variables.ProcessTime);
                bp.AddVariable(bp.Variables.ProcTime);
            end
        end
      
        
        function obj = run_calculators(obj, bp)
            % Check if a experiment exists
            if isempty(bp) || ~isvalid(bp)
                warning('No Bioprocess given');
                return;
            end
            
            % rerun all calculators, set mode to online (true) (EXPUTEC)
            % bp.rerunAllCalculators(true);
            % calculateAll executes all calculators (VTU)
            bp.calculateAll
        end
       
        
        function run_update_plots(obj)
            data_plot = findobj('Tag', 'data_plot'); 
            if isempty(data_plot)
                return
            end
            
            handles=guidata(obj.main_gui);
            
            old.xlimits = data_plot.CurrentAxes.XLim;
            old.ylimits = data_plot.CurrentAxes.YLim;
            
            opts = handles.ext_plot_fig.UserData.opts;
            if obj.keep_plot_view
%                 xlim(old.xlimits);
%                 ylim(old.ylimits);
                axes = findobj(data_plot, 'Type', 'Axes');
                yrange = {};
                for i=numel(axes):-1:1
                    xrange = {axes(i).XLim};
                    yrange = [yrange {axes(i).YLim}];
                end
                
                if isfield(handles, 'plot_mult_b')
                    handles.plot_mult_b.prev_selected_xrange = xrange;
                    handles.plot_mult_b.prev_selected_yrange = yrange;
                end
                opts.refresh = 1;
            else
                if isfield(handles, 'plot_mult_b')
                    handles.plot_mult_b.prev_selected_xrange = {[0 0]};
                    handles.plot_mult_b.prev_selected_yrange = {[0 0]};
                end
                guidata(obj.main_gui, handles);
                
                opts.refresh = 0;
            end
                        
            BPTT_GUI.plot.update(handles, opts);
        end

        
        function stop(obj)
            t = timerfind('Tag', obj.timer_tag );
            if ~isempty(t)
                stop(t);
                delete(t);
            end
            if obj.debug
                disp('Data request loop stopped. Finishing already running calculations if any.');
            end
        end
        
        function useful_error(~, err)
            warn_settings = warning;
            if strcmp(warn_settings(1).state, 'off')
                warning('on');
                warn_was_off = 1;
            else
                warn_was_off = 0;
            end

            namestack = err.stack.name;
            linestack = err.stack.line;
            warning on;
            warning([namestack ' in line ' num2str(linestack) ': ' err.message]);

            if warn_was_off
                waring('off');
            end
        end
    end
    
    % ==== general static class functions ============
    methods(Static)
      function status = status_field_all(handles, field, value)
         status = true;
         
         realtime_objects = {'realtime', 'realtime_opc', 'realtime_edb'};
         for i=1:numel(realtime_objects)
            if isfield(handles, realtime_objects{i})
                status = handles.(realtime_objects{i}).(field);
                
                if exist('value', 'var')
                    handles.(realtime_objects{i}).(field) = value;
                    status = value;
                end
            end
         end
      end
    end
end

