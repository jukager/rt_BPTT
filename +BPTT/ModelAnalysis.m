% Class for Model Analysis
% (c) Aydin Golabgir & Julian Kager April 2018
% includes Sensitivity, Identifiability and Observability Analysis and
% Parameter estimation
% See also
% 
% Reference page in Help browser 
% <a href="matlab:doc BPTT.ModelAnalysis;">doc BPTT.ModelAnalysis</a>
%  
classdef ModelAnalysis < handle
    properties
        
        verbose=0;          % print out stuff or not
        Experiments         % Bioprocess object(s) holding experimental data
        Model               % Model struct
        ParEst              % Name of parameters to be estimated or to be analyzed for sensitivity
        ParEst_UB           % upper bound multiplier e.g. [1.05 1.1]
        ParEst_LB           % lower bound multiplier e.g. [0.95 0.9]
        Timeframe           % time frame of analysis e.g. for parameter estimation in model hours
        comp_data           % name of variables to be compared (in experiments)
        comp_model          % name of model outputs to be compared
        Quality             % Quality/phase code of the experimental data
        xint                % Initial values for state variables
        y_error =0.05       % Error of model states
        meas_error          % relative error on measurements
        estimated_p         % values of estimated parameters
        output_format=0;    % output format of cost function 1:vectorized, 0: a value
        Cost                % value of the cost function
        mT                  % time vector of parameter estimation simulations
        xsim                % simulated state variables
        ysim               % simulated model outputs
        exp_data            % experimental data returned by cost function
        optmethod=0         % optimization method 0: fminsearch 1: patternsearch
        L                   % Likelihood values
        pert                % relative parameter pertubation for sensitivity analysis
        Sens_dydpc                % raw unscaled (unstructured) absolute Sensitivtiy Matrix
        Sens_yref               % reference simulation outputs (used for Sensitivity Scaling)
        Sens                % Scaled sensitivity matrix
        Sens_norm           % Normalized sensitivity timecourse
        Sens_norm_median         % Sensitivity Median
        sens_timecourse     % Storing the time course of parameter sensitivities
        sens_timecourse_norm % Storing the time course of normalized parameter sensitivities
        Identifiability_plot = 0 %activate Identifiability plot
        downsample          % interval of model input data (hours) - empty no downsampling 
        nLies               % Number of lies derivatives to be computed
        lie_D               % Lie derivatives in sympolic form
        f_lie_D             % Lie derivatives in function form
        jacobian            % Jacobian matrix
        o_idx               % Observability index
        o_rank              % Observability rank
        rank_cutoff         % cutoff criteria for rank determination - units of SNR
        state_error         % absolute error on states - scaling observability 
        y_selected          % selected outputs for calculation of the observability index
        x_selected          % selected states for calculation of observability index
        x_sel_data          % corresponding experimental dataset for testing observability
        x_sel_std           % standard deviation of experimental data
        PopulationSize      % variables for GA
        Generations         % variables for GA
        meas_error_obs      % measurement error for observability calculations
        data_freq           % data frequency for measurements (when testing practical observability)
        bestset             % stores best identifiable parameter set
        EstimationTime      % timepoint of last parameter estimation
        
    end
    
    properties (SetAccess = private)
        ModificationTime = now;
        CreationTime = now; 
        orig_p              % original parameter values
        u                   % u is the struct containing input data
    end
    
    methods
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ModelBasedOptimization(obj)  
            % perform a simulation with model and experimental inputs
            %[mT,ysim,xsim]=BPTT.tools.model_sim_conf(u,xint(j,:),td,model,1);
            % A OWN OPTIMIZATION CLASS WAS CREATED!!!
        end
        %%%%
        function selectmodeloutputs(obj,model,varargin)
        % function to select model outputs which are included within ModelAnalysis
         if ~isempty(varargin)
             %y_sel = str;
             y_sel = varargin %shows selected outputs
             y_sel = strtrim(strsplit(varargin{1},','));
             y_sel = y_sel';
             newmodel= model;
             newmodel.y = struct;
                for ii = 1 : length(y_sel)
                newmodel.y.(y_sel{ii}) = model.y.(y_sel{ii});
                end
           obj.SetModel(newmodel);
         else
           y_all = strjoin(fieldnames(model.y)',' , ') %shows and selects original outputs
           obj.SetModel(model);
         end
         
        end
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ParameterEstimation(obj)
% function estimating model parameter based on measurements
% Syntax: 
%  MA.ParameterEstimation
% Inputs:
%   MA.SetModel(model); adding Process Model
%   MA.ParEst{'parmeter1','parameter1','...'}; set parameters to estimate
%   MA.ParEst_UB = ones(1,length(MA.ParEst)).* 3; upper bound multiplier for allowed parameter value range
%   MA.ParEst_LB = ones(1,length(MA.ParEst)).* 0.3; lower bound multiplier for allowed parameter value range
%   MA.comp_data ={'y1_measured', 'y2_measured'}; measured model outputs
%   MA.comp_model ={'y1', 'y2'}; model outputs for comparison 
%   MA.meas_error = [] ;relative error on measurements
%   MA.optmethod = [] ;optimization method 0: fminsearch 1: patternsearch 2: genetic algorithm 3:global fmincon 
% Outputs:
%   MA.Cost; cost function of fminsearch
%   MA.estimated_p; estimated parameters
% Visualisation:   
%   function BPTT.tools.parest_cost; estimated parameters with boundaries,
%   model fit with liklehood measure
            
            obj.Cost = [];
            obj.estimated_p = [];
            obj.L= struct;
            obj.xsim= struct;
            obj.ysim = struct;
            
            if isempty(obj.ParEst)
                error('The ParEst property is empty.')
            end
            
            % get the initial values of parameters from model struct
            % if the estimated_p property has a value then take these as
            % the value for p0
            
            if isempty(obj.estimated_p)
                p0 = obj.orig_p;
            else
                p0 = obj.estimated_p(end,:);
            end
           
            %out = BPTT.tools.parest_cost(p0, obj.Timeframe, obj.ParEst, obj.Model, exp, obj.comp_data, obj.comp_model, output_format, obj.Quality, obj.xint, obj.meas_error)
            
            lb = obj.orig_p .* obj.ParEst_LB';
            ub = obj.orig_p .* obj.ParEst_UB';
            
            % perform the actual optimization
            % fminsearch
            if obj.optmethod==0
                obj.output_format=0;
                plotflag=1;
                options = optimset('TolFun',1e-1); % Choose exiting criteria TolFun minimal differnce in time for cost function or MaxFunEval for maximum function evaluations 
                p0 = BPTT.tools.fminsearchbnd(@BPTT.tools.parest_cost,p0,lb,ub,options, obj,plotflag)
                           
            % pattern search
            elseif obj.optmethod==1
                % requires the global optimization toolbox
                obj.output_format=1;
                opts = psoptimset('UseParallel','always');
                plotflag=1;
                p0 = patternsearch(@(p0)BPTT.tools.parest_cost(p0,obj,plotflag),p0,[],[],[],[],lb,ub,[],opts)
                
            % genetic algorithm
            elseif obj.optmethod==2
                options = gaoptimset('Display','iter','PopulationSize',obj.PopulationSize,'Generations',obj.Generations);
                plotflag=0;
                p0 = ga(@(p0)BPTT.tools.parest_cost(p0,obj,plotflag),length(p0),[],[],[],[],lb,ub,[],[],options)
            
            % global fmincon
            elseif obj.optmethod==3
                
                obj.output_format = 0;
                plotflag=1;
                opts = optimoptions('fmincon','Algorithm','sqp');
                problem = createOptimProblem('fmincon','objective', ...
                @(p0)BPTT.tools.parest_cost(p0,obj,plotflag),'x0',p0,'lb',lb,'ub',ub, ...
                'options',opts);
               
                %p0 = fmincon(problem);
                
                gs = GlobalSearch;
                p0 = run(gs,problem);
                
            end
            
            plotflag=1;
            BPTT.tools.parest_cost(p0, obj, plotflag);
            
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function Sensitivity(obj)
% function for performing sensitivity analysis
% Syntax: 
%   MA.Sensitivity; to execute analysis 
% Inputs:
%   MA.SetModel(model); adding Process Model
%   MA.pert = 0.05; relative perturbation on paramaeters
%   MA.ParEst = {'parmeter1','parameter1','...'}; set parameters to estimate
% Outputs:
%   MA.sens_timecourse contains the 3D sensitivity matrix        

        [~, y, dydpc_] = BPTT.tools.model_sens(obj); %function to calculate sensitivity timecourse

        obj.Sens_dydpc = dydpc_;
        obj.Sens_yref = y;
        
        end
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
        function ScaleSensitivity(obj,Scalemethod)
% function to Scale Sensitivity Matrix
% Input:
%   1 = no scaling; 2 = scaling with state value; 3 = max - min scaling)
% Output:
%   MA.Sens 
%   MA.sens_timecourse
%   MA.sens_timecourse_norm

            S = obj.Sens_dydpc;                  % absolute 3-way sensitivity matrix
            y = obj.Sens_yref;
            [nt, ny]=size(y');            % nt: number of time points ny: number of outputs
            np = length(obj.ParEst);     % np: number of parameters
            Sndim = zeros(ny,np);                  % matrix to hold the non-dimensional sensitivity matrix (integral)
            Sndim_idf = zeros(ny,np);              % matrix to hold the non-dimensional sensitivity matrix (median)
            timecourse = zeros(ny,np,length(obj.Timeframe));            % absolute timecourse
            timecourse_norm =zeros(ny,np,length(obj.Timeframe));        % normalized timecourse
            
            % Sorting, scaling and converting from 3D to 2D by eliminating the time dimension
            for ip = 1:np % loop through parameters
                for iy = 1:ny % loop through model outputs
                    if Scalemethod == 1 %scaling factor for sensitivity 
                    scf = 1;
                    elseif Scalemethod == 2 
                    scf = (y(iy,:)'); 
                    scf(scf==0)=0.001; % avoid division to 0 
                    elseif Scalemethod == 3
                    scf = abs(max((y(iy,:)')- min((y(iy,:)')))); 
                    end
                    ndim_s = squeeze(S(:,iy,ip))./scf ;   % scaled sensitivities
                    ndim_s(isnan(ndim_s))=0; 
                     % restructure Sensitivity timecourse
                    timecourse(iy,ip,:) = ndim_s;
                    Sndim(iy,ip) = trapz(obj.Timeframe, abs(ndim_s)); % integrate sensitivities
                   
                    % restructure normalized Sensitivity Matrix
                    %ndim_n = squeeze(S_(:,iy,ip));
                    %ndim_n(isnan(ndim_n))=0;
                    %timecourse_norm(iy,ip,:) = ndim_n;
                end
            end
            
            % Normation with the euclidian norm of the state column
            % according to Paper( brun et. al. 2002) as basis for
            % Identifiability Analysis
            for iy = 1:ny
                y_norm = [];
                y_norm = squeeze(timecourse(iy,:,:));
                y_norm = y_norm/norm(y_norm);
                timecourse_norm(iy,:,:) = y_norm;
                for ip = 1:np
                Sndim_idf(iy,ip) = median(squeeze(timecourse(iy,ip,:))); % median for steady state/ continous/ single phase processes    
                end
            end
            obj.Sens = Sndim;                        % absolute Sensitivity Integral for Parameter importance ranking
            obj.Sens_norm_median = Sndim_idf;             % absolute Sensitivity median for Sensitivity matrix & Parameter identifiability (only for steady state/ continous/ single phase processes) !!!Aydins Implementation!!!
            obj.sens_timecourse = timecourse;          % to show Sensitivity Timeline
            obj.sens_timecourse_norm = timecourse_norm; % for Sensitivity Headmap
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
           
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function Identifiability(obj,outputs,maxsetsize,collinearitylimit,varargin)
% function for determining sets of identifiable parameters 
% Syntax: 
%   MA.Identifiability(obj,outputs,maxsetsize,collinearitylimit); to execute analysis 
%Inputs:
%   MA.sens; 2D sensitivity matrix from sensitivity analysis
%   outputs; model outputs {'y1','y2','...'}
%   maxsetsize; maximum size of parameter sets to be analyzed for identifiability          
%   collinearitylimit; is the collinearity treshold for accepting identifiable parameter sets
%Outputs:
%   collinearity plot with best observable parameter set
%   3 best sets of every set size under the treshold 
            clc
            % check whether sensitivities have been calculated or not
            if isempty(obj.sens_timecourse_norm)
                error('Sensitivities have not been calculated.')
            end
            
            if maxsetsize<2
                error('The maximum set size must be greater or equal to 2.')
            end

            [ny,np] = size(obj.Sens_norm_median); % get the size of the sensitivity matrix
            
            if maxsetsize>np
                error('The maximum set size cannot be greater than the number of parameters.')
            end
            
            if ~isempty(varargin)
                timepoint= [];
                timepoint(1) = varargin{1}; 
            %S= [];
            S = obj.sens_timecourse_norm(:,:,timepoint+1); %Sensitivity Matrix @ selected timepoint
            else
            S = obj.Sens_norm_median; % overall median Sensitivity (Aydin Golabgir) (Brun et al took the normalized Sensitivity Submatrix)
            end
            [ny,np] = size(S);
            Y = zeros(1,length(outputs));   % matrix to hold the index of included outputs / measurements
            for i=1:length(outputs)
                Y(i) = find(strcmp(fieldnames(obj.Model.y),outputs{i}));
            end
            
            % preallocate a vector for storing the minimum collinearity index of each set size
            out=zeros(maxsetsize-1,1);
            
            % check all of the set sizes starting from set size 2 to the specified maxsetsize
            for setex=2:maxsetsize

                v = [1:np];
                K = nchoosek(v,setex);    % matrix to hold the index of included parameters
                [r,~] = size(K);
                %disp(['Number of parameter sets: ' num2str(r)])

                % calculating the collinearity index and determinant measure
                gamma_K = zeros(r,1);
                rho_K = zeros(r,1);
                for i=1:r
                    K_ = K(i,:);
                    K_(K_==0)=[];
                    S_K = S(Y,K_);                       % SK is a subset of S (for some parameters)
                    lambda_K = abs(min(eig(S_K' * S_K)));       % lambda_K is the smallest eigenvalue of SK'*SK
                    gamma_K(i) = 1/sqrt(lambda_K);              % gamma_K is the collinearity index
                    rho_K(i) = det(S_K' * S_K)^(1/(2*setex));     % determinant measure - combines sensitivity and collinearity
                    if gamma_K(i)<= 100 
                        nrofidep = setex;
                    end
                end
                if  exist('nrofidep','var') && setex==nrofidep
                    disp('best identifiable set')
                    disp('Outputs/measurements used: ')
                    fn = fieldnames(obj.Model.y);
                    disp(BPTT.tools.cell2str(fn(Y)',', '))
                    disp(['Number of parameters in each set: ' num2str(setex)])
                    temp = rho_K;
                    
                    for j = 1:3
                        disp('--------------------------------------------------')
                        
                        [~,ind]=max(temp);
                        while gamma_K(ind) > 100
                            temp(ind)=0;
                            [maxval,ind]=max(temp);
                            if maxval == 0
                                j=4;
                                ind= 1;
                                warning('NON VALID PARAMETER SET DETECTED')
                                break
                            end    
                        end
                        disp(['Parameter set rak: ' num2str(j)])
                        disp(['Determinant measure: ' num2str(rho_K(ind))]);
                        disp(['Collinearity index: ' num2str(gamma_K(ind))]);
                        disp(' ')
                        
                        for i=1:setex
                            disp([num2str(K(ind',i)) obj.ParEst(K(ind',i))]);
                            
                            % remember the best parameter set of the largest set size
                            if j==1
                                bestset = obj.ParEst(K(ind',:));
                                obj.bestset= bestset;
                            end
                        end
                       temp(ind)=0; 
                    end
                end
                out(setex-1) = min(gamma_K);
            
            end
            if exist('nrofidep','var') == 0
            disp('No parameter sets identifiable') 
            end
            if collinearitylimit > 10
            warning('limit of published collinearity threshold (<= 10) exceeded')
            end
            
            % plotting the minimum of collinearity index vs. set size and
            % the best set
            if obj.Identifiability_plot == 1
            figure(3) % that it does not overwrite the plot from Interactive Sensitivity
            
                semilogy(2:2+length(out)-1,out,'-o')
            
            ylabel(['Min collinearity index',10, 'for parameter sets of different sizes' ])
            xlabel('parameter set size')
            line([1 2+length(out)-1],[collinearitylimit collinearitylimit],'Color','r')
            title(['measurements used: ' strrep(BPTT.tools.cell2str(outputs,', '),'_',' ')])
            limsy=get(gca,'YLim');
            limsx=get(gca,'XLim');
            ylim ([0, limsy(2)]);
            xlim ([2, limsx(2)]);
            grid off 
            ax=axis;
            set(gca, 'XTick', 2:1:(limsx(2)));
            try
            text(2.2+((ax(2)-ax(1))/10),(ax(4)-ax(3))/4,BPTT.tools.cell2str(strrep(bestset,'_',' ')'),'FontWeight','Bold','HorizontalAlignment', 'center','VerticalAlignment', 'baseline')
            catch
            disp('no identifiable sets')
            end
            end
            end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_sensitivitytimecourse(obj)
        % function to visulaize the sensitivity timecourse of selected
        % model outputs. Click on the legend to slect parameters of
        % interset
            fn = fieldnames(obj.Model.y);
            A= ceil(sqrt(length(fn))); % subplot rows
            B= ceil(sqrt(length(fn))); % subplot colums
            for ii=1:length(fn)
                subplot(A,B,ii)
                plot(obj.Timeframe, squeeze(obj.sens_timecourse_norm(ii,:,:)))
                xlabel('Time [h]')
                title(strrep(fn{ii},'_',' '))
            l{ii}=legend(strrep(obj.ParEst,'_',' '),'visible','off');
            set(l{ii},'visible','off')
            end
            l{ii}=legend(strrep(obj.ParEst,'_',' '),'Selected','on','SelectionHighlight','on');
            l{ii}.ItemHitFcn = @obj.hitcallback_ex1; % call back function for clicking on the legend
        end
        
       
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function plot_sensitivity(obj,maxsens,tp)
        % Function for visualization of the sensitivity matrix
        % maxsens is the upper limit for the plotted sensitivity values.
        % can be used to make the sensitivities visible
        % maxsens... Headmap limits
        % tp... timepoint to be considered
        
        tp = round(tp)+1; % round timepoint to next integer
        
        % new implementaiton based on timecourse to visulaize a certain
        % timepoint
        %Sens_t = (obj.sens_timecourse(:,:,tp)) ;   % absolute sensitivities 
        Sens_t= obj.sens_timecourse_norm(:,:,tp);   % normalized sensitivities
        
        Sens_t(isnan(Sens_t))=0;
        
            % ny number of outputs, np number of parameters
            [ny,np] = size(Sens_t);
            
            
            % create a matrix for visualization of sensitivites
            m =zeros(ny+1,np+1);
            
            
            for i=1:ny % looping through parameters
                for j=1:np % outputs
                    m(i,j) = Sens_t(i,j);
                    %limit to +-maxsens
                    if abs(m(i,j)) > maxsens, m(i,j) = maxsens*sign(m(i,j)); end
                    %m(i,j) = maxsens*sign(m(i,j));
                 end
            end
            Colorlimit = abs(max(m(:)));
            
            pcolor(m')
            
            title('Sensitivity matrix')
            set(gca,'YTick',1.5:1:length(obj.ParEst)+0.5)
            set(gca,'YTickLabel',strrep(obj.ParEst,'_',' '))
            set(gca,'XTick',1.5:1:length(fieldnames(obj.Model.y))+1.5)
            set(gca,'XTickLabel',strrep(fieldnames(obj.Model.y),'_',' '))
            colorbar
            caxis([-Colorlimit Colorlimit])
            
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_parameter_importance(obj,outputs)
        % function for plotting the parameter importance rankings for selected model outputs {'y1','y2','...'} 
        % Plots the parameter importance rankings for given outputs:
        % MA.plot_parameter_importance(outputs) where
        % outputs is a cell-array of name-stings for desired Outputs.
         
            % check whether sensitivities have been calculated or not
            if isempty(obj.Sens)
                error('Sensitivities have not been calculated.')
            end
            
            S = obj.Sens;
            
            Y = zeros(1,length(outputs));   % matrix to hold the index of included outputs / measurements
            for i=1:length(outputs)
                if sum(strcmp(fieldnames(obj.Model.y),outputs{i}))==0
                    error(['Defined measurement ' outputs{i} ' not found in model.'])
                end
                Y(i) = find(strcmp(fieldnames(obj.Model.y),outputs{i}));
            end
            
            Sndim2 = S(Y,:);
            val = sqrt(diag(Sndim2'*Sndim2));
            %val = sum(Sndim(Y,:));

            [a1,b1] = sort(val,'descend');
            a1 = a1 ./ sum(a1);
            bar(a1)
            title(['Relative parameter importance ranking',10, 'Outputs: ' strrep(BPTT.tools.cell2str(outputs,', '),'_',' ')])
            ylabel('\delta^{msqr}')
            %ylabel('Normalized parameter importance')
            xlabel('Parameter count')
            for i=1:length(b1)
                if 1
                    offset = 0.2;
                    text(i-offset , a1(i)+0.01,strrep(obj.ParEst{b1(i)},'_',' '),'FontSize',11);
                end
            end
            
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function orig_p = get.orig_p(obj)
        	orig_p = zeros(length(obj.ParEst),1);
            for i=1:length(obj.ParEst)
                orig_p(i) = obj.Model.p.(obj.ParEst{i}).Value;
            end
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddExperiment(obj, experimentobj)
        obj.AddExperiments(experimentobj);
            % !!! will be replaced by AddExperiments !!!
        % Add an experiment to the collection MA.
        %   MA.AddExperiment(experimentobj) where
        %       experimentobj is a BPTT.Bioprocess-Object containing Data
        %       (BPTT.TimeVariables) of an experiment.
        
            %obj.Experiments = struct; % delete previous experiments if any
            %obj.Experiments.(experimentobj.Name) = genvarname(experimentobj); 
            %obj.ModificationTime = now; % refresh the modification time
        end
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddExperiments(obj, experimentobj)
        % Add an experiment to the collection MA.
        %   MA.AddExperiment(experimentobj) where
        %       experimentobj is a BPTT.Bioprocess-Object containing Data
        %       (BPTT.TimeVariables) of an experiment.    
        %Name= (['B_' experimentobj.Name]);
        fieldname = genvarname(experimentobj.Name);    
        obj.Experiments.(fieldname) = experimentobj;
            obj.ModificationTime = now; % refresh the modification time
            disp([experimentobj.Name ' added to MA.Experiments.' fieldname ])
        % replacing genvarnme with matlab.lang.makeValidName since R2014a
        end
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Disable(obj, varargin)
        % Exclude selected Experiment from Analysis 
        % MA.Disable('Bioprocess_Name','Bioprocess_Name'...)
        % first step enable all Experimetns    
        fn = fieldnames(obj.Experiments);
            for i = 1:length(fn)
                fieldname = fn{i};
                obj.Experiments.(fieldname).Enabled = true ;
            end
        % second step Disable choosen Experimetns
        input = varargin;
            for i = 1:length(input)
                fieldname= genvarname(input{i});
                obj.Experiments.(fieldname).Enabled = 0 ;         
            end
        disp('Enabled Experiments are:')
        obj.AllEnabled
        end
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = SetModel(obj, modelstruct)
        % Set the model property of the class.
        %   MA.SetModel(modelstruct) where
        %       modelstruct is a structure descriping the model.
        % - additional checks on the model struct can be implemented.
        
            obj.Model = modelstruct; 
            obj.ModificationTime = now; % refresh the modification time
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function exp_list = AllEnabled(obj)
        % Returns a list of all enabled experiments in the collection MA.
        %   exp_list = MA.AllEnabled()
        %       where exp_list is a cell-array with the name-strings of all
        %       enabled experiments (BPTT.Bioprocess-Objects) added to MA.
        %   see AddExperiment.
            fn = fieldnames(obj.Experiments);
            count=1;
            for i = 1:length(fn)
                if obj.Experiments.(fn{i}).Enabled
                    exp_list{count} = obj.Experiments.(fn{i}).Name;
                    count = count + 1; 
                end
            end
            exp_list=exp_list';
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_estimated(obj)
        % Plotts the course of the estimated parameters and cost function.
        % (into currend figure)
        
            if isempty(obj.estimated_p)
                error('Parameters have not been estimated.')
            end
            
            % appropriate subploting
            N = length(obj.ParEst)+1;
            sb = ceil(N/4);
            
            for i=1:N-1
                
                 if N==2
                     sbv = [1 2];
                 elseif N==3 
                     sbv = [1 3];
                 elseif N >= 4
                     sbv = [sb 4];
                 end
                 
                subplot(sbv(1),sbv(2),i)
                x = 1:size(obj.estimated_p,1);
                plot(x,obj.estimated_p(:,i))
                title(strrep(obj.ParEst{i},'_',' '))
                
                lb = obj.orig_p .* obj.ParEst_LB';
                ub = obj.orig_p .* obj.ParEst_UB';
                
                %line([min(x) max(x)],[obj.orig_p(i) obj.orig_p(i)],'LineStyle','--','Color','k')
                
%               lb = obj.ParEst_LB';
%               ub = obj.ParEst_UB';
                
                line([min(x) max(x)],[lb(i) lb(i)],'LineStyle','--','Color','r')
                line([min(x) max(x)],[ub(i) ub(i)],'LineStyle','--','Color','r')
                
            end
            
            subplot(sbv(1),sbv(2),N)
            plot(x,obj.Cost)
            title('Cost function')
            
            disp('%% ----------------------------------------')
            for i=1:N-1
                disp(['p.' obj.ParEst{i} '.Value = ' num2str(obj.estimated_p(end,i)) ';'])
            end
            
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function OutputIdx = getOutputIdx(obj)
        % find the index of the specified model outputs to be compared.
        
            model_outputs = fieldnames(obj.Model.y);
            OutputIdx = zeros(length(obj.comp_model),1);
            for i=1:length(obj.comp_model)
                OutputIdx(i) = find(strcmp(obj.comp_model{i},model_outputs));
            end
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_variables(obj)
            % Plots the time course of the simulation vs
            % measured variables with the estimated parameters.
            % (into currend figure)
            
            enabled = obj.AllEnabled;
            enabled = genvarname(enabled); % generate fieldnames of enabled experiments
            exp = struct;
            batchnames = cell(length(enabled),1);
            for i=1:length(enabled) % copy enabled experiments to exp. struct
                exp.(enabled{i}) = obj.Experiments.(enabled{i});
                batchnames{i} = obj.Experiments.(enabled{i}).Name;
            end
            fn = fieldnames(exp);   % get the names of enabled input experiments
            
            for j=1:length(fn)
                figure(j+1)
                N = length(obj.comp_data);
                sb = ceil(N/4);
                OutputIdx = obj.getOutputIdx;
                
                for i=1:N
                    if N==2 || N==1
                        sbv = [1 2];
                    elseif N==3
                        sbv = [1 3];
                    elseif N >= 4
                        sbv = [sb 4];
                    end
                    subplot(sbv(1),sbv(2),i)
                    
                    try
                        ysim_local = obj.ysim.(fn{j})(OutputIdx(i),:,end);
                        exp_time_local = obj.exp_data{1,j}.(obj.comp_data{i}).Time;
                        exp_data_local = obj.exp_data{1,j}.(obj.comp_data{i}).Data;
                    catch % in case of numerical observability
                        ysim_local = obj.ysim(OutputIdx(i),:,end);
                        exp_time_local = obj.exp_data.(obj.comp_data{i}).Time;
                        exp_data_local = obj.exp_data.(obj.comp_data{i}).Data;
                    end
                    if length(exp_data_local)>50 % select offline or online markers (online data with more then 50 points)
                        % activate in case you want reduce to a certain
                        % number of dots
                        %ds =50;
                        %plot(obj.mT, ysim,'k', exp_time(1:ds:end), exp_data(1:ds:end),'k.')
                        plot(obj.mT, ysim_local,'k', exp_time_local, exp_data_local,'k.')
                    else
                        plot(obj.mT, ysim_local,'k', exp_time_local, exp_data_local,'ko')
                    end
                    try
                        ylabel([strrep(obj.comp_model{i},'_',' ') ' L=' num2str(obj.L.(fn{j})(end,i))])
                    catch
                        ylabel([strrep(obj.comp_model{i},'_',' ') ' L=' num2str(obj.L(end,i))])
                    end
                    xlabel('Process time [h]')
                    
                    grid on
                    
                    if isfield(obj.Model.y.(obj.comp_model{i}),'Unit')
                        
                        %ylabel(obj.Model.y.(obj.comp_model{i}).Unit)
                    end
                    xlim([obj.Timeframe(1) obj.Timeframe(end)])
                    hold off
                end
                title([batchnames{j}])
            end
        end
          
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_observability(obj)
        % Plots the observability index and rank.
        % (into currend figure)
        
            subplot(1,2,1)
            plot(obj.Timeframe, obj.o_idx)
            title('Observability index')
            xlabel('Process Time [h]')
            grid on
            ylim([0 100])
            
            subplot(1,2,2)
            plot(obj.Timeframe, obj.o_rank)
            title('Observability rank')
            xlabel('Process Time [h]')
            grid on
            ylim([0 1.2])
   
        end
        
        %% calculating the observability index based on included measurements
        function [o_idx, o_rank, J_partial] = calc_oidx(obj,J)
            
            if nargin<2
                if isempty(obj.jacobian)
                    error('Jacobian is empty.')
                end
                
                J = obj.jacobian;
            end
            
            % get numbers out of the observability matrix
            [p.meas,p.states,p.times] = size(J);
            
            % number of lie derivatives in the observability matrix
            p.measderiv = obj.nLies;
            
            % number of included lie derivatives for the calculation of
            % observability index - currently using the maximum
            p.wantedderiv = obj.nLies;
            
            % find the index of the included measurements
            for i=1:length(obj.y_selected)
                p.measincl(i) = find(strcmp(fieldnames(obj.Model.y),obj.y_selected{i}));
            end
            
            % find the index of the included states
            for i=1:length(obj.x_selected)
                p.states_incl(i) = find(strcmp(fieldnames(obj.Model.x),obj.x_selected{i}));
            end
            
            % number of measurement equations
            neq = p.meas / (p.measderiv + 1);

            % index of included rows of the jacobian based on number of wanted derivatives
            m = [];
            for i=1:neq
                m = [m 0:p.measderiv];
            end

            inc1 = m<=p.wantedderiv;
            
            % select rows of the jacobian based on the included measurements
            inc2 = zeros(1,neq*(p.measderiv+1));
            for i=1:neq
                if sum(p.measincl==i)==1
                    s=i*(p.measderiv+1)-p.measderiv;
                    inc2(s:s+p.wantedderiv)=1; 
                end
            end
        
            % combine the two criteria and create the new truncated
            % observability matrix
            inc = logical(inc1 .* inc2);
            O1 = J(inc,:,:);
            O1 = O1(:,p.states_incl,:);
            J_partial = O1;
            
           [m,s,a] = size(O1);
           % observability index
            obj.o_idx = zeros(a,1);

            % observability rank
            obj.o_rank = zeros(a,1);

            % why do we need R?
            %R = ones(a,s)*s;

            % loop through time
            for i=1:a
            
               % the Jacobian (observability) matrix 
               J = squeeze(O1(:,:,i));
                
               
               % singular value decomposition of observability matrix
               [U, S, V] = svd(J);

%                size(U); % measurements
%                size(S); % measurements x states
%                size(V); % states
%                U;
%                S;
%                V';
%                V = V';
                
                % calculating the rank condition
                obj.o_rank(i) = rank(J,obj.rank_cutoff)/s;
                %obj.o_rank(i) = rank(J)/s;
                
                % calculating observability index
                %if size(S,2)>1 && obj.o_rank(i)==1
                    
                    % rank condition observability index
                    %obj.o_idx(i) = max(max(S))/min(max(S));
                    
                    % geometric mean observability index
                    obj.o_idx(i) = prod(diag(S)).^(1/length(diag(S)));
                    
                    %obj.o_idx(i) = sum(diag(S));
     
                %elseif obj.o_rank(i)<1
                %    obj.o_idx(i) = 0;
                %elseif size(S,2)==1
                %    obj.o_idx(i) = 1/S(1);
                %end

%                 for j=1:s
%                     col = V(:,j);
%                     r2 = find(abs(col)==max(abs(col)));
%                     if (sum(R(i,:)==j))==0, R(i,r2)=j;end
%                 end

            end
            
            o_idx = obj.o_idx;
            o_rank = obj.o_rank;
            
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function J = compute_jacobian(obj, select,tv,xv)
        % Method for computing the jacobian (observability matrix)
        % J = MA.compute_jacobian(select,tv,xv) where
        %   select: how many 
        %   tv: time vector
        %   xv: state vector.
            
            if nargin<3
                [tv,~,xv] = obj.simulate(0);
                if nargin==1
                    select = [];
                end
            end
            
            if isempty(select)
                select = 1:length(obj.lie_D);
            end
            
            if isempty(obj.f_lie_D)
                error('Lie derivatives are not calculated.')
            end
            
            % calc jacobian (complex step)
            h = 10^(-8);
            % check measurement equation selection
            select = select(select<=length(obj.lie_D));
            if length(select)<length(fieldnames(obj.Model.x))
                fprintf('warning: not enough lie derivatives chosen?\n')
            end
                        
            % calculate jacobian
            obj.jacobian = zeros(length(select), length(fieldnames(obj.Model.x)), length(tv));
            
            % get the inputs from the experiment object
            u_exp = obj.u;
            fn = fieldnames(u_exp);
            
            
            % interpolate inputs to match tv
            uv = zeros(length(tv),length(fn)); % create an empty matrix to hold interpolated input values
            for i=1:length(fn)
                u_time = u_exp.(fn{i}).Time;
                u_data = u_exp.(fn{i}).Data;
                uv(:,i) = interp1(u_time,u_data,tv,'linear','extrap');
            end
            
            % calculate y confidence intervals based on meas_error
           
            dh_err = [];
            for i=1:length(fieldnames(obj.Model.y))
              %dh_err = [ dh_err ; ones(obj.nLies+1,1).* sqrt(2) .* obj.meas_error_obs(i) ];
              
              dh_err_(1,1)=obj.meas_error_obs(i);
              for j=2:obj.nLies+1
                  %dh_err_(j,1) = dh_err_(j-1,1)/(obj.Timeframe(2)-obj.Timeframe(1));
                  dh_err_(j,1) = dh_err_(j-1,1);
              end
              
              dh_err = [ dh_err ; dh_err_];
            end
            
            state_norm = mean(xv);
            %state_norm = obj.state_error;
            
            for j = 1:length(fieldnames(obj.Model.x))
                xh = xv';
                xh(j,:) = xh(j,:) + h*1i;
                xu_cell = num2cell([xh;uv'],2);
                for k=1:length(select)
                    lie_D_select(k,:) = obj.f_lie_D(select(k)).function(xu_cell{:}) ;
                end
                
                % calculating dhdx and scalin it
                im = imag(lie_D_select)/h;
                for i=1:size(im,2)
                    im(:,i) = im(:,i)./ dh_err .* state_norm(j); % expected state accuracy
                end
                obj.jacobian(:,j,:) =   im;
            end
            
            J = obj.jacobian;
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = create_lie_D(obj)
        % creating lie derivatives.
        % only works with symbolic models and is used for structural
        % observability
        
            obj.lie_D = sym([]);
            
            if isempty(obj.nLies)
                error('Number of lie derivatives is undefied (nLies).')
            end
            
            % checking requirements
            if isempty(obj.Model)
                error('Undefined model.')
            end
            if ~isempty(obj.Model)
                if isempty(obj.Model.sym_y)
                    error('Symbolic expressions not found.')
                end
            end
            
            n_Ldevs = ones(1,length(fieldnames(obj.Model.y))).* obj.nLies;
            
            % define lie derivatives for observation equations
            if obj.verbose
                disp('Define lie derivatives')
                disp(' ')
            end
            
            k=0; %counter for lie derivatives
            
            for i = 1:length(obj.Model.sym_y)
                
                if n_Ldevs(i)>0 % iterate over number of derivatives for each observation eq
                    
                    num = n_Ldevs(i);
                    if obj.verbose
                        disp(['adding L',int2str(n_Ldevs(i)-num),' of H',int2str(i)]);
                    end
                    
                    L_local = obj.Model.sym_y(i);
                    
                    % compute the values of EQ and substitute in symbolic expressions
                    % Works only if assuming EQs only depend on parameters
                    
                    p = obj.Model.p;
                    fn = fieldnames(obj.Model.EQ);
                    for j=1:length(fn)
                       sym(fn{j},'real');
                       L_local = subs(L_local,fn{j},eval(obj.Model.EQ.(fn{j})));
                    end
                    
                    % substitute the values of parameters in the symbolic expressions
                    fn = fieldnames(obj.Model.p);
                    for j=1:length(fn)
                       sym(fn{j},'real');
                       L_local = subs(L_local,fn{j},p.(fn{j}).Value);
                    end
                    
                    obj.lie_D = [obj.lie_D; L_local];k=k+1;
                    
                    if obj.verbose
                        fprintf('create matlab function of lie_D(%i)\n',k);
                    end
                    
                    % creating matlab funciton, for speeding up calculation
                    X = sym('X',[length(fieldnames(obj.Model.x)),2]); % define symbolic state variables (mol/l)                      
                    X = X(:,1);
                    X = sym(X, 'real');
                    obj.f_lie_D(k).function = matlabFunction(L_local,'vars',[X' , obj.Model.sym_u]);
                    
                    while num>0
                        
                        pd = [];
                        num = num-1;
                        
                        for j = 1:length(fieldnames(obj.Model.x))
                            if obj.Model.sym_f(j)~=0
                                % compute partial derivatives after state j
                                pd = [pd (diff(L_local, obj.Model.sym_x(j)))];
                            else
                                pd = [pd 0];
                            end
                        end
                        
                        if obj.verbose
                            disp(['adding L',int2str(n_Ldevs(i)-num),' of H',int2str(i)]);
                        end
                        
                        L_local = (pd*obj.Model.sym_f);
                                      
                        % compute the values of EQ and substitute in symbolic expressions
                        % Works only if assuming EQs only depend on parameters
                        p = obj.Model.p;
                        fn = fieldnames(obj.Model.EQ);
                        for j=1:length(fn)
                           sym(fn{j},'real');
                           L_local = subs(L_local,fn{j},eval(obj.Model.EQ.(fn{j})));
                        end

                        % substitute the values of parameters in the symbolic expressions
                        fn = fieldnames(obj.Model.p);
                        for j=1:length(fn)
                           sym(fn{j},'real');
                           L_local = subs(L_local,fn{j},p.(fn{j}).Value);
                        end

                        %if isequal(L,0)
                        %disp('warning: lie derivative = 0, omitting this derivative')
                        %disp(' ')
                        %else
                        obj.lie_D = [obj.lie_D; L_local];k=k+1;
                        %end
                        
                        if obj.verbose
                            fprintf('create matlab function of lie_D(%i)\n',k);
                        end

                        % creating matlab funciton, for speeding up calculation
                        obj.f_lie_D(k).function = matlabFunction(L_local,'vars',[obj.Model.sym_x' , obj.Model.sym_u]);
                    end
                end
            end
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [t,y,x,h] = simulate(obj,flag,TF)
        % Simulation of a model with enabled Experiment in the collection MA:
        %   [t,y,x,h] = MA.simulate(flag,TF) where
        %       flag ==0: calculate states x(t) only for each timepoint t
        %              1: calculate states x(t) and outputs y(t) for each t
        %              2: calculate state x and outputs y for endpoint.
        %       TF (TimeFrame) as Vector of timepoints (unit: [h]). 
            
            if nargin==1
                flag=0;
            end
            
            if nargin<3
                TF = obj.Timeframe;
            end
            
            if isempty(obj.xint)
                error('Initial conditions (xint) must be defined.');
            end
            if isempty(obj.Timeframe)
                error('Timeframe of the simulation must be defined.');
            end
            if isempty(obj.Model)
                error('Missing model structure for performing the simulation.');
            end
            
            tic
            [t,y,x,h]=BPTT.tools.model_sim_conf(obj.u,obj.xint,TF,obj.Model,flag);
            elapsedTime = toc;
            
            if obj.verbose
                disp(['Model: ' obj.Model.Name]);
                fprintf('Simulation time: %f sec \n', elapsedTime );
            end
            
        end
            
       %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       function u = get.u(obj)
           % function to get variable model inputs from Bioprocess Object 
           
            % only perform the u-getting operations if it has not been done before
            if isempty(obj.u) || 1
                
                if isempty(obj.Experiments)
                    error('Experiment not defined.')
                end

                % currently using the first experiment for getting the
                % inputs !!!! IMPORTANT!!!! effecting --> MA.Simulate and
                % Observability Analysis
                fn = fieldnames(obj.Experiments);
                u = obj.Experiments.(fn{1}).getvars(obj.Model.u_data,'Quality',obj.Quality);

                % downsample if specified
                if ~isempty(obj.downsample)
                   u = BPTT.tools.downsample(u,obj.downsample); 
                end

                % define the reference datetime which is equivalent to sim time zero
                % here the reftimee must be defined in MetaData
                reftime = obj.Experiments.(fn{1}).MetaData.StartTime;

                % convert units of inputs from days to hours
                for i=1:length(obj.Model.u_data)
                    u.(obj.Model.u_data{i}).Time = (u.(obj.Model.u_data{i}).Time - reftime) .* 24;
                end
                
            else
                % return the existing structure
                u = obj.u;
            end
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [O] = test_observability(obj,lb,ub)
% numerical Observability analysis cna be used to predict the initial state
% values based on measurements and the process model
% Syntax: 
%  MA.test_observability(lb,ub)
% Inputs:
%   MA.SetModel(model); adding Process Model
%   MA.comp_data ={'y1_measured', 'y2_measured'}; measured model outputs
%   MA.comp_model ={'y1', 'y2'}; model outputs for comparison 
%   MA.meas_error = [] ; relative error on measurements
%   MA.data_freq = [1 1]; for downsampling online signals
%   MA.x_selected = {'y3','...'}; selected model outputs with variable x0
%   MA.x_sel_data = {'y3_measured','...'}; measured state for NRMSE
%   evaluation
%   MA.x_sel_std = {''}; standard deviation of measurement, can be empty 
% Outputs:
%   MA.NRMSE; Root mean square error of prediction
% Visualisation:   
%   function BPTT.tools.observability_test_cost; estimated initial state conditions with NRMSE evaluation,
%   model fit with liklehood measure between comp_data and comp_model
            
% clear some variables used by the cost function during optimization
            obj.L = [];
            obj.Cost = [];
            obj.estimated_p = [];
            obj.xsim =[];
            obj.ysim=[];
            
            % find the index of the included states
            for i=1:length(obj.x_selected)
                p.states_incl(i) = find(strcmp(fieldnames(obj.Model.y),obj.x_selected{i}));
            end
            
            
            obj.ParEst = obj.x_selected;
            obj.ParEst_LB = lb;
            obj.ParEst_UB = ub;
            
            % select a random point for the start of the optimization
            x0 = lb + rand(1,length(obj.x_selected)).* (ub - lb);
            
            plotflag = 1;
            x0=BPTT.tools.fminsearchbnd(@BPTT.tools.observability_test_cost,x0,lb,ub,[], obj,plotflag);
            
            %options=psoptimset('MaxIter',100,'InitialMeshSize',mean(x0)/2,'Display','iter');
            %options=psoptimset('InitialMeshSize',mean(x0)/2,'Display','iter');
            %options=psoptimset('UseParallel','always','Display','iter');
            %x0 = patternsearch(@(x0)BPTT.tools.observability_test_cost(x0,obj,plotflag),x0,[],[],[],[],lb,ub,[],options)
            
            
            plotflag = 0;
            %options = gaoptimset('Display','iter','UseParallel','always','PopulationSize',50,'PlotFcns',{@gaplotgenealogy,@gaplotbestf,@gaplotscores,@gaplotbestindiv});
            %options = gaoptimset('Display','iter','PopulationSize',obj.PopulationSize,'Generations',obj.Generations);
            %x0=ga(@(x0)BPTT.tools.observability_test_cost(x0,obj,plotflag),length(x0),[],[],[],[],lb,ub,[],[],options)
            
            % replace the value of initial conditions with the optimum
            xint_local = obj.xint;
            xint_local(p.states_incl) = x0;
            MA.xint = xint_local;
            
            % perform a simulation to calculate observability parameters
            %[tsim,~,xsim] = obj.simulate(0);
            %J = obj.compute_jacobian([],tsim, xsim);
            %[o_idx, o_rank] = obj.calc_oidx(J);
            %O(1,1)= sum(o_idx);
            %O(1,2) = sum(o_rank)/length(o_rank);
            O=[];
            
            % plotting using the cost function
            plotflag=1; % forces plotting regardless of number of simulations
            BPTT.tools.observability_test_cost(x0, obj, plotflag);
            
        end
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [M,O,R]=observability_meas_combo(obj,meas)
        % Testing all combinations of measurements for observability:
        %   [M,O,R]=MA.observability_meas_combo(meas) where
        %       meas: a cell of name-strings containing choice of measurements for
        %             the combinatorial analysis.
        %   Results: M - index matrix holding 0/1 for measurements
        %            O - vector for mean of observability matrix
        %            R - vector 0/1 - is it observable all the time?
            
            %meas = fieldnames(obj.Model.y);
            N = length(meas);
            
            % looping through all measurement possibilities
            M = []; % matrix holding 0/1 for measurements
            O = []; % vector for mean of observability matrix
            R = []; % vector 0/1 - is it observable all the time?
            
            for i=1:N
                
                idx = nchoosek(1:N,i);
                
                for j=1:size(idx,1)
                    m = zeros(1,N);
                    m(1,idx(j,:)) = 1;
                    obj.y_selected = meas(idx(j,:))';
                    [o_idx, o_rank, ~]=obj.calc_oidx;
                 
                    M = [M ; m];
                    length(M)
                    
                    O = [O; mean(o_idx(3:end))];
                    
                    if sum(o_rank<1)>0
                        R = [R; 0]; % not observable all the time
                    else
                        R = [R; 1]; % observable all the time
                    end
                end
            end
            
            %%
            idx0 = find(R==1);
            if length(idx0)>0
                idx0 = sortrows([O(idx0) idx0]);
                idx = idx0(:,2);
                
                %subplot(1,3,1:2)
                M1 = M(idx,:);
                M1 = [M1 zeros(size(M1,1),1)];
                M1 = [M1 ; zeros(1,size(M1,2))];
                pcolor(M1)
                %colorbar
                colormap(gray(2))
                %freezeColors
                %title('Measurement matrix','FontSize',10)
                set(gca,'YTick',1.5:1:length(M1)+0.5)
                %set(gca,'YTickLabel',num2str(sum(M(idx,:),2)))
                set(gca,'YTickLabel',num2str(round(O(idx))))
                set(gca,'XTick',1.5:1:length(meas)+1.5)
                set(gca,'XTickLabel',strrep(meas,'_',' '))
            else
                disp('No observable configurations found!')
            end
            
        end 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function interactiveSensitivity(obj,method)
            % Multiple View Sensitivity visualization to examine Sensitivity
            % timecourse (method 1 = explore only Sensitivity; method 2 = show identifiable sets)
            
            fh_sl= figure;
            set(fh_sl,'position',[450,400,500,400])
            size(obj.sens_timecourse);
            plot(obj.Timeframe, squeeze(sum(obj.sens_timecourse,1))) % show sum of sensitivity timecourse
            legend(strrep(obj.ParEst,'_',' '))
            td=obj.Timeframe;
            % creates interactive slider
            slhan = uicontrol('Parent',fh_sl,'Style','slider','Position',[65,50,390,20],...
                'value',td(2), 'min',min(td), 'max',max(td),'Callback',@obj.updateinteractiveSensitivity);
            %b.Callback = @(es,ed) obj.updateinteractiveSensitivity(es.Value,method);
            fh_sens=figure;
            set(fh_sens,'position',[1000,400,500,400])
            set(slhan,'Userdata',[2,fh_sens,fh_sl]);
        end
        
              %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ParameterErrors(obj)
            %Joseph DiStefano III-Dynamic Systems Biology Modeling and
            %Simulation-Academic Press (2013).
            %Kap. 10,11,12
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %NEEDS TO BE PREPARED BEFORE START:
            %---------------------------------
            %MA OBJECT
            %x.init
            %model to MA (memory)
            %paremeters (f.e. MA.ParEst = {'alpha_PP' 'mu_p'};)
            %perturbation (f.e. MA.pert = 1e-2;)
            %----------------------------------
            %% find min time and use it
            l=[];nameexp=fieldnames(obj.Experiments);
            for i=1:length(obj.comp_data)
                l=[l;length(obj.Experiments.(nameexp{1}).Variables.(obj.comp_data{i}).Data)];
            end
            TimeToUse=(obj.Experiments.(nameexp{1}).Variables.(obj.comp_data{find(min(l))}).Time-obj.Experiments.(nameexp{1}).Variables.(obj.comp_data{find(min(l))}).Time(1))*24;
            %% get sensitivity matrix
            obj.Timeframe = TimeToUse;
            [t, y, dydpc_] = BPTT.tools.model_sens(obj);
            %% get only available model outputs (Y)
            yy=[];
            for i=1:length(obj.comp_model)
                yy=[yy find(strcmp(fieldnames(obj.Model.y),obj.comp_model{i}))];
            end
            %%
            dydpc=dydpc_(:,yy,:);
            %%
            [m n f]=size(dydpc); % m timepoints, n Y-values and f parameter
            MASumm=0;
            for i=1:m
                MAi=[];
                for j=1:f
                    col=dydpc(i,:,j)';
                    MAi=[MAi col];
                end
                MASumm=MASumm+MAi;
            end
            
            %% W according to hard coded rel. errors
            sigma=[];
            for i=1:n
                sigma(i,1)=obj.meas_error(i)*mean(y(i,:));
            end
            sigmasq=1./(sigma.^2);
            W=diag(sigmasq);
            %%
            COV_FIM=(sqrt(diag((MASumm'*W*MASumm)^(-1))))*100;
            for i=1:length(COV_FIM)
                l=(obj.ParEst{i});
                disp(['Relative error in ',l,' is ',num2str(COV_FIM(i)),'%'])
            end
            
        end  
          
    end
    
    
    
    
    
    methods(Access = protected)
        % private Methods which can be accessed only by Object and Subobjects
        
        function updateinteractiveSensitivity(obj,sh,temp)
            % Plot update function to visualize time resolved sensitivity and
            % identifiability
            
            currentvalue = get(sh,'value');
            userdata=get(sh,'userdata');
            method=userdata(1);
            fh=userdata(2);
            fh_sl=userdata(3);
            currentvalue = round(currentvalue);
            ylim= get(gca,'Ylim');
            figure(fh_sl),
            plot(obj.Timeframe, squeeze(sum(obj.sens_timecourse,1)))
            line([currentvalue currentvalue],ylim,'Color',[1 0 0]);
            title(['Timepoint ',num2str(currentvalue),' h'])
            
            figure(fh);
            obj.plot_sensitivity(1,currentvalue)
            
            % find identifiable parameter set for current timepoint
            if method == 2
                outputs = obj.y_selected;
                maxsetsize= length(obj.ParEst);
                collinearitylimit = 10;
                obj.Identifiability(outputs,maxsetsize,collinearitylimit,currentvalue)
            end
        end
        
        function hitcallback_ex1(obj,src,evnt)
            % Plot update function to select/ unselect Legend entries
            
            legendobjs = (findall(gcf,'type','Line'));% Find all items in this category (all subplots)
            
            for oid = 1:length(legendobjs) %go througth subplot objects & check
                if strcmp((legendobjs(oid).DisplayName),evnt.Peer.DisplayName) % selected legend entry
                    if strcmp(legendobjs(oid).Visible, 'off') % turn on if it was off
                        legendobjs(oid).Visible = 'on';
                    else
                        legendobjs(oid).Visible = 'off'; % turn off
                    end
                elseif strcmp(legendobjs(oid).Visible, 'off') %if not selected stays off
                    %remains off
                else
                    legendobjs(oid).Visible= 'on'; %if not slected stays on
                end
                
            end
        end
    

        
        
        
    end
end