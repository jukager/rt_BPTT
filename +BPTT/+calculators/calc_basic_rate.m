function [ y ] = calc_basic_rate ( x,obj )
    % Variable x input (in that order):
    %   - conc
    %   - biomass
    %   - reactor_volume
    %   - substrate_feed
    %   - nitrogen_feed
    %   - acid_feed
    %   - base_feed
    %   - supplementary_feed
    %   - growth_rate

    [conc_orig, biomass_orig, growth_rate_orig, feeds, term_rates] = name_inputs(x);
    
    P = fill_missing_inputs_in_parameters(obj, conc_orig);
    
    if strcmp(P.replicates,'mean')
        [conc, biomass, growth_rate] = calculate_mean(conc_orig, biomass_orig, growth_rate_orig);
    end
    
    rate_number = numel(P.phases);
    
    rate_data = zeros(rate_number,1);
    residual_sums = zeros(rate_number,1);
    biomass_interpolant = cell(rate_number,1);
    interpolated_feeds = create_feed_interpolant_2(feeds);
    growth_rate_interpolant = create_rate_interpolant(growth_rate);
    
    for index=1:rate_number
        
        biomass_intv = create_interval(biomass.Data, biomass.Time, index, P);
        conc_intv = create_interval(conc.Data, conc.Time, index, P);
        time_intv = create_interval(conc.Time, conc.Time, index, P);
        
        if (P.specific_growth_rate == false) && ~isnan(time_intv(1))
            if isnan(biomass_intv(1))
                
                start_helper_index = find(~isnan(biomass.Data) & biomass.Time<time_intv(1),1,'last');
                start_helper_index = start_helper_index(1);
                end_helper_index = find(~isnan(biomass.Data) & biomass.Time>time_intv(1));
                end_helper_index = end_helper_index(1);

                helper_time_intv = [biomass.Time(start_helper_index), biomass.Time(end_helper_index)];
                helper_biomass_intv = [biomass.Data(start_helper_index), biomass.Data(end_helper_index)];
                helper_biomass_interpolant = create_biomass_interpolant(helper_time_intv, helper_biomass_intv, growth_rate_interpolant, interpolated_feeds);
                biomass_intv(1) = helper_biomass_interpolant(time_intv(1));
            
            elseif isnan(biomass_intv(end))
                
                end_helper_index = find(~isnan(biomass.Data) & biomass.Time>time_intv(1),1,'last');
                end_helper_index = end_helper_index(1);
                
                helper_time_intv = [time_intv(1), biomass.Time(end_helper_index)];
                helper_biomass_intv = [biomass_intv(1), biomass.Data(end_helper_index)];
                helper_biomass_interpolant = create_biomass_interpolant(helper_time_intv, helper_biomass_intv, growth_rate_interpolant, interpolated_feeds);
                biomass_intv(end) = helper_biomass_interpolant(time_intv(end));
            end
        end
        
        deletion_indices = logical(isnan(biomass_intv) + isnan(conc_intv) + isnan(time_intv));
        
        biomass_intv(deletion_indices) = [];
        conc_intv(deletion_indices) = [];
        time_intv(deletion_indices) = [];
        
        if P.specific_growth_rate == false
            biomass_interpolant{index} = create_biomass_interpolant(time_intv, biomass_intv, growth_rate_interpolant, interpolated_feeds);
        end
        
        guessed_specific_rate = guess_specific_rate(conc_intv, ...
            time_intv, ...
            biomass_intv, ...54
            interpolated_feeds, ...
            P);
        
        [rate_data(index,1),residual_sums(index,1),~,output] = fminsearch(@(spec_rate)optimize_specific_rate(spec_rate, time_intv, conc_intv, interpolated_feeds, biomass_intv,P,biomass_interpolant{index},term_rates),...
            guessed_specific_rate);

        rate_time(2*index-1:2*index) = [time_intv(1), time_intv(end)];
    end
     
    y = create_calc_output(rate_data,rate_time, residual_sums, conc, P);
    call_names = fieldnames(y);
    calc_conc = calculate_theoretical_concentrations(y.(call_names{1}), conc, feeds, P, biomass_interpolant,biomass, term_rates);
   
    if P.plot == true
        names = fieldnames(y);
        rate_time_variable = names{1};
        BPTT.plots.rate_and_concentration(y.(rate_time_variable), conc, calc_conc)
    end
    
    y.(BPTT.tools.rm_special_characters(calc_conc.Name,'warning',0)) = calc_conc;
    
end

function [varargout] = calculate_mean(varargin)
    input_number = numel(varargin);
    varargout = cell(input_number,1);
    for index = 1:input_number
        varargout{index} = varargin{index};
        if size(varargin{index}.Data,2)>1
            varargout{index}.Data = nanmean(varargout{index}.Data,2);
        end
    end
end

function [P] = fill_missing_inputs_in_parameters(obj, conc)
    P = obj.p;
    feed_concentrations = {'glc_feed_conc', ...
                           'nitrogen_feed_conc', ...
                           'acid_feed_conc', ...
                           'base_feed_conc', ...
                           'supp_feed_conc',...
                           'specific_growth_rate'};
                       
    for index = 1:numel(feed_concentrations)
        if ~isfield(P,feed_concentrations{index})
            P.(feed_concentrations{index}) = 0;
        end
    end

%     if ~isfield(P,'mechanism_term')
%         P.mechanism_term = '@(t,x,biomass_conc,term_rates) 0';
%     end
    
    if isfield(P,'mechanism_term') && ischar(P.mechanism_term)
        P.mechanism_term = eval(P.mechanism_term);
    end
    
    
    if ~isfield(P,'plot')
        P.plot = true;
    end
    
    if ~isfield(P,'replicates')
        P.replicates = 'mean';
    end
    
    if ~isfield(P,'phases') || isempty(P.phases)
        time = conc.Time;
        time(isnan(nanmean(conc.Data,2))) = [];
        P.phases = cell(numel(time)-1,1);
        for index = 1:numel(time)-1
            P.phases{index} = [time(index), time(index+1)];
        end
    elseif strcmp(P.phases, 'start2end')
        P.phases = {[conc.Time(1), conc.Time(end)]};
    end
    
    if strcmp(P.phases{1}, 'fromStart')
        time = conc.Time;
        time(isnan(nanmean(conc.Data,2))) = [];
        start_time = P.phases{2}(1);
        additional_phases = cell(1,numel(time(time<start_time))-1);
        for index = 1:numel(time(time<start_time))-1
            additional_phases{index} = [time(index), time(index+1)];
        end
        P.phases = [additional_phases, P.phases(2:end)];
    end
    
    if strcmp(P.phases{end},'toEnd')
        time = conc.Time;
        time(isnan(nanmean(conc.Data,2))) = [];
        start_time = P.phases{end-1}(2);
        start_time_index = find(start_time == time);
        additional_phases = cell(1,numel(time(time>start_time)));
        for index = 1:numel(time(time>start_time))
            additional_phases{index} = [time(start_time_index + index-1), time(start_time_index + index)];
        end
        P.phases = [P.phases(1:end-1), additional_phases];
    end
    
end

function [conc, biomass, growth_rate, feeds, term_rates] = name_inputs(x)
    x_names = fieldnames(x);
    conc = x.(x_names{1});
    biomass = x.(x_names{2});
    growth_rate = x.(x_names{9});
    
    feeds = create_feed_structure(x, x_names);
    
    % The so-called term_rates are everything that 'only' is needed for 
    % the so-called mechanism term in the differential equation.
    
    term_rates = struct;
    if numel(x_names)>9
        for index = 1:numel(x_names(10:end))
            term_rates.(x_names{9+index}) = create_rate_interpolant(x.(x_names{9+index}));
        end
    end
end

function [y] = create_calc_output(rate_data, rate_time, residual_sums, conc_orig, P)
    
    rate_data_new = zeros(1,2*numel(rate_data));
    residual_sums_new = zeros(1,2*numel(rate_data));
    
    for index = 1:numel(rate_data)
        rate_data_new(2*index-1:2*index)=rate_data(index);
        residual_sums_new(2*index-1:2*index)=residual_sums(index);
    end

    specific_rate = BPTT.TimeVariable;
    specific_rate.Name = P.output_name;
    specific_rate.DataInfo.unit = ['g/g/' conc_orig.TimeInfo.Units];
    specific_rate.Data = rate_data_new';
    specific_rate.Time = rate_time';
    specific_rate.MetaData.residual_square_sums = residual_sums_new';
    %specific_rate.Residual_sums = residual_sums_new';
        
    y.(BPTT.tools.rm_special_characters(P.output_name,'warning',0)) = specific_rate;
end

function [feed] = create_feed_structure(x, x_names)

    TS = BPTT.tools.interphighest(x);
    
    feed = struct();
    feed.reactor_volume = TS.(x_names{3});
    feed.substrate = TS.(x_names{4});
    feed.nitrogen = TS.(x_names{5});
    feed.acid = TS.(x_names{6});
    feed.base = TS.(x_names{7});
    feed.supplementary = TS.(x_names{8});
    
    feed.total = get_total_feed(feed.substrate, feed.nitrogen, feed.acid, feed.base, feed.supplementary);

end

function [guess] =  guess_specific_rate(conc_intv, time_intv, biomass_intv, interpolated_feeds,P)
    conc_diff = conc_intv(end)-conc_intv(1);
    time_diff = time_intv(end)-time_intv(1);
    
        f_at_t = zeros(numel(interpolated_feeds,1));
    for index = 1:numel(interpolated_feeds)
        f_at_t(index) = interpolated_feeds{index}(mean(time_intv));
    end
    % 1 = reactor_volume, 2 = substrate, 3 = nitrogen, 4 = acid, 5 =
        % base, 6 = supplementary, 7 = total
    feed_term = - 1/mean(f_at_t(1))*...
                (mean(f_at_t(2))*P.glc_feed_conc ...
                 + mean(f_at_t(3))*P.nitrogen_feed_conc ...
                 + mean(f_at_t(4))*P.acid_feed_conc ...
                 + mean(f_at_t(5))*P.base_feed_conc ...
                 + mean(f_at_t(6))*P.supp_feed_conc);
    
    guess = (conc_diff/time_diff + f_at_t(7)/f_at_t(1)* median(conc_intv) + feed_term) / median(biomass_intv);
end

function [feed_interpolant] = create_feed_interpolant(feed)
    feed_names = fieldnames(feed);
    for index = 1:numel(feed_names)
        name = feed_names{index};
        feed_interpolant.(name) = griddedInterpolant(feed.(name).Time, feed.(name).Data);
    end
end

function [feed_interpolant] = create_feed_interpolant_2(feed)
    feed_names = fieldnames(feed);
    for index = 1:numel(feed_names)
        name = feed_names{index};
        % 1 = reactor_volume, 2 = substrate, 3 = nitrogen, 4 = acid, 5 =
        % base, 6 = supplementary, 7 = total
        feed_interpolant{index} = griddedInterpolant(feed.(name).Time, feed.(name).Data);
    end
end

function [biomass_interpolant] = create_biomass_interpolant(time_intv, biomass_intv, growth_rate_interpolant, interpolated_feeds)
    time_increment = 7e-5;
    [time, biomass_conc] = ode23(@(t2,x2)biomass_diff_equation(t2,x2,growth_rate_interpolant, interpolated_feeds{1}, interpolated_feeds{7}), ...
                                      [time_intv(1):time_increment:time_intv(end)], ...
                                      biomass_intv(1),...
                                      odeset('NonNegative', [],'MaxStep',0.1));
    biomass_interpolant = griddedInterpolant(time, biomass_conc);
end

function [feed_at_t] = interpolate_feeds(feed, t)
    feed_names = fieldnames(feed);
    for index = 1:numel(feed_names)
        name = feed_names{index};
        feed_at_t.(name) = interp1(feed.(name).Time, feed.(name).Data, t,'previous','extrap');
    end
end

function [residue] = optimize_specific_rate(specific_rate, time_intv, conc, interpolated_feeds, biomass_intv,P, biomass_interpolant,term_rates)
    
    [~,conc_calc] = ode23(@(t,x)conc_diff_equation(t,x, specific_rate, interpolated_feeds, P, biomass_intv, time_intv,biomass_interpolant,term_rates),...
                                                   time_intv,...
                                                   conc(1),...
                                                   odeset('NonNegative', [],'MaxStep',0.1));
    
%     residue = sum(abs(conc(2:end) - conc_calc(2:end)));
    if ~isfield(P,'phases') || length(time_intv) == 2
        residue = sum((conc(end) - conc_calc(end)).^2);
    else
        residue = sum((conc(2:end) - conc_calc(2:end)').^2);
    end
    %disp(residue)
end

function [dx] = conc_diff_equation(t, x, specific_rate, interpolated_feeds, P, biomass_intv, time_intv, biomass_interpolant,term_rates)
    
%     feed_names = fieldnames(interpolated_feeds);
%     for index = 1:numel(feed_names)
%         name = feed_names{index};
%         f_at_t.(name) = interpolated_feeds.(name)(t);
%     end
    
    for index = 1:numel(interpolated_feeds)
        f_at_t(index) = interpolated_feeds{index}(t);
    end
    
    if P.specific_growth_rate == true
        biomass_conc = x;
    else
        if time_intv(1)== t
            biomass_conc = biomass_intv(1);
        else
            biomass_conc = biomass_interpolant(t);
        end
    end
    
    uptake_term = specific_rate * biomass_conc;
%     dilution_term = - f_at_t.total * x / f_at_t.reactor_volume;
%     input_term = 1/f_at_t.reactor_volume*(f_at_t.substrate*P.glc_feed_conc ...
%                            + f_at_t.acid*P.acid_feed_conc ...
%                            + f_at_t.base*P.base_feed_conc ...
%                            + f_at_t.nitrogen*P.nitrogen_feed_conc ...
%                            + f_at_t.supplementary*P.supp_feed_conc);
    
    % 1 = reactor_volume, 2 = substrate, 3 = nitrogen, 4 = acid, 5 =
        % base, 6 = supplementary, 7 = total
        dilution_term = - f_at_t(7) * x / f_at_t(1);
    input_term = 1/f_at_t(1)*(f_at_t(2)*P.glc_feed_conc ...
                           + f_at_t(4)*P.acid_feed_conc ...
                           + f_at_t(5)*P.base_feed_conc ...
                           + f_at_t(3)*P.nitrogen_feed_conc ...
                           + f_at_t(6)*P.supp_feed_conc);
                       
    if isfield(P,'mechanism_term')                   
        mechanism_term = P.mechanism_term(t,x,biomass_conc,term_rates);
    else
        mechanism_term = 0;
    end
                       
    dx(1) = input_term + uptake_term + mechanism_term + dilution_term;
     
    dx=dx';
    
end

function [dx] = biomass_diff_equation(t, x, growth_rate_interpolant, reactor_volume, total_feed)
    
%     volume = interp1(reactor_volume.Time, reactor_volume.Data,t,'previous','extrap');
%     feed = interp1(total_feed.Time, total_feed.Data,t,'previous','extrap');
    
    volume = reactor_volume(t);
    feed = total_feed(t);
    % interp1 does not work with rates, as they are step functions. Step 
    % functions are correct from a mathematical point but can't be used.
    
    growth_rate = growth_rate_interpolant(t);
    
    dx(1) = 1/volume + growth_rate * x - feed * x / volume;
    
    dx=dx';
    
end

function [total_feed] = get_total_feed(varargin)
    feed_number = numel(varargin);
    x = struct();
    
    for index = 1:feed_number
        if ~isa(varargin{index},'BPTT.TimeVariable')
            error('getTotalFeed:NotTimeVariable',['Input ' num2str(index) ' is not a BPTT.TimeVariable.']);
        elseif ~(varargin{index}.Length>0)
            error('getTotalFeed:LengthZero',['Lenght of input ' num2str(index) ' is zero.']);
        elseif ~strcmp(varargin{index}.DataInfo.Units, varargin{1}.DataInfo.Units)
            error('getTotalFeed:InconsistentUnits',['Input ' num2str(index) ' has not the same units as previous inputs.']);
        else
            x.(['feed_' num2str(index)])= varargin{index};
        end
    end
    
    TS = BPTT.tools.interplowest(x);
    
    calc_total_feed = zeros(TS.feed_1.Length,1);
    
    for index = 1:feed_number
        calc_total_feed = calc_total_feed + TS.(['feed_' num2str(index)]).Data;
    end
    
    total_feed = BPTT.TimeVariable;
    total_feed.Data = calc_total_feed;
    total_feed.Time = TS.feed_1.Time;
    total_feed.Quality = TS.feed_1.Quality;
    
end

function [interval] = create_interval(data, time, index, parameter)
    % Sometimes there are some issues with wanting to have a specific
    % time point as the start/end of a phase due to rounding errors.
    % Rounding values to a sensible degree fixes that.
    significant_digit = 6;
    data = round(data,significant_digit);
    time = round(time,significant_digit);

    if ~isfield(parameter,'phases')
        start_value = data(index);
        end_value = data(index+1);
        interval = [start_value, end_value];
    elseif strcmp(parameter.phases, 'start2end')
        interval = data;
    else
        start_index = find(time <= round(parameter.phases{index}(1),significant_digit),1, 'last');
        end_index = find(time >= round(parameter.phases{index}(2),significant_digit),1, 'first');
        if isempty(end_index)
            end_index = length(time);
        end
        interval = data(start_index(1):end_index(end))';
    end
end

function [calc_conc] = calculate_theoretical_concentrations(rate, concentration, feeds, P, biomass_interpolant,biomass, term_rates)
    time_calc = [];
    conc_calc = [];
    
    max_index = numel(P.phases);    
    for index = 1:max_index
        stupid_time_intv = create_interval(concentration.Time, concentration.Time, index, P);
        conc_intv = create_interval(concentration.Data, concentration.Time, index, P);
        biomass_intv = create_interval(biomass.Data, biomass.Time, index, P);
        
        if (P.specific_growth_rate == false) && ~isnan(stupid_time_intv(1))
            if isnan(biomass_intv(1))
                biomass_intv(1) = biomass_interpolant{index}(stupid_time_intv(1));
            elseif isnan(biomass_intv(2))
                biomass_intv(2) = biomass_interpolant{index}(stupid_time_intv(2));
            end
        end
        
        deletion_indices = logical(isnan(biomass_intv) + isnan(conc_intv) + isnan(stupid_time_intv));

        biomass_intv(deletion_indices) = [];
        conc_intv(deletion_indices) = [];
        stupid_time_intv(deletion_indices) = [];

        time_intv = [stupid_time_intv(1):((stupid_time_intv(2)-stupid_time_intv(1))/20):stupid_time_intv(end)];

        specific_rate = interpolate_rate(rate, mean(time_intv));

        interpolated_feeds = create_feed_interpolant_2(feeds);
        [time_calc_intv, conc_calc_intv]=ode23(@(t,x)conc_diff_equation(t, x, specific_rate, interpolated_feeds, P, biomass_intv, time_intv, biomass_interpolant{index},term_rates),...
                                               time_intv,...
                                               conc_intv(1),...
                                               odeset('NonNegative', [],'MaxStep',0.1));

        time_calc = [time_calc; time_calc_intv];
        conc_calc = [conc_calc; conc_calc_intv];

    end

    calc_conc = BPTT.TimeVariable;
    calc_conc.Name = ['calc. ' concentration.Name];
    calc_conc.Data = conc_calc;
    calc_conc.DataInfo.Units = concentration.DataInfo.Units;
    calc_conc.Time = time_calc;
    calc_conc.TimeInfo.Units = concentration.TimeInfo.Units;
    calc_conc.Quality = concentration.Quality;
end

function [interpolated_rate] = interpolate_rate(rate, intp_time)
    [time_for_interp, unique_indices, ~] = unique(rate.Time,'last');
    rates_for_interp = rate.Data(unique_indices);
    interpolated_rate = interp1(time_for_interp, rates_for_interp,intp_time,'previous','extrap');
end

function [rate_interpolant] = create_rate_interpolant(rate)
    time = rate.Time(1:2:end);
%     if size(rate.Data,2) > 1
%         data = mean(rate.Data(1:2:end),2);
%     else
        data = rate.Data(1:2:end);
%     end
    rate_interpolant = griddedInterpolant(time, data, 'previous');
end

function [result] = iff(condition, trueResult, falseResult)
    if condition
        result = trueResult;
    else
        result = falseResult;
    end
end