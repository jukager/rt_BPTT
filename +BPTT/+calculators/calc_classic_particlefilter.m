function [y]  = calc_classic_particlefilter( x_in , obj)
    % ParticleFilter implementation in BPTT
    % (c) Aydin Golabgir
    % calc_particle filter is a calculator function and gets its inputs
    % from the calculator object.
    
    % modified: AGO 28.04.2015
    % modified: AGO 22.10.2015
    % modified: PEt 20.01.2016
    
    doPFdiagnostics = false; % local constant, enable diagnostic plot
    if isfield(obj.p, 'diagnostics')
        doPFdiagnostics = obj.p.diagnostics;
    end
    
    P = obj.p;                              % read the calculator parameters from the calculator object
    x_names = fieldnames(x_in);             % get the names of the input signals
    model = P.model;
    TS = BPTT.tools.interplowest(x_in);     % interpolate input signals
    
    % definition of a persistent variable for model parameters
    if ~isfield(obj.persist,'model_p')
        obj.persist.model_p = P.model.p;
    end
    
    td = [];
    L = TS.(x_names{1}).Length;
    
    if L > 0
        % define the time vector of input data
        td = (TS.(x_names{1}).Time  - TS.(x_names{1}).Time(1)) .* 24;
        oldtime = td; 
        if ~isempty(P.interval)
            t0 = min(td);
            tend = max(td);
            td = t0:P.interval:tend;
        end
        t_out = ((td/24) +  TS.(x_names{1}).Time(1));
    end

    % separate inputs and measurements from each other
    % relative time for inputs and conversion to hours
    for i=1:length(P.u)
            u.(TS.(P.u{i}).Name)=TS.(P.u{i});
        if L>0
            u.(TS.(P.u{i}).Name).Time = (u.(TS.(P.u{i}).Name).Time - u.(TS.(P.u{i}).Name).Time(1)).*24; 
        else
            u.(TS.(P.u{i}).Name).Time = [0; 1];
            u.(TS.(P.u{i}).Name).Data = [0; 1];
        end
    end

    % store the measurements in the m struct for later use
    for i=1:length(P.m)
        m.(P.m{i}) = TS.(P.m{i});
    end

    % initialize a matrix of zeros for actual outputs (measurements) for later use
    y_act=zeros(length(td),length(P.m));

    % get the output names from the model struct
    output_names = fieldnames(model.y);

    % get the state names from the model file
    state_names = fieldnames(model.x);

    % find out the index of the measured outputs in model outputs
    my_idx = zeros(length(output_names),1);
    for i=1:length(P.my)
        my_idx = my_idx + strcmp(output_names,P.my{i});
    end
    my_idx = logical(my_idx>0);    
        
    % check whether the time vector td has enough points for performing any kind of calculation
	if length(td) > 1

        %% Make the randomly generated particles from the initial prior gaussian distribution

        nxint = length(state_names);    % number of states
        ny = length(output_names);      % number of outputs

        x_P = zeros(nxint,P.N);         % Initialize a vector to hold the particles (states)
        y_init = zeros(ny,P.N);         % Initialize a vector to hold the calculated initial outputs of the particles
        
        % creating the initial particles and calculate their outputs
        for i = 1:P.N
            %x_P(:,i) =  P.xint' + initial_std' .*  P.xint' .* randn(nxint,1);  % adding relative random gaussian noise to the partcice
            x_P(:,i) =  P.xint' + P.initial_std' .*   randn(nxint,1);           % adding absolute random gaussian noise to the partcice
            
            x_P(x_P(:,i)<0,i)=1e-5;                                             % remove negative state variables
            % y_init(:,i)=feval(P.model_name,td(1),x_P(:,i),[],1,obj.persist.model_p,u)';   % calculate outputs for the initial population
            
            [t,y_init(:,i),~]=BPTT.tools.model_sim_conf(u,x_P(:,i),td(1),model,2);
            
        end


        %% Initializing matrices to hold estimated state and output for different time points
        % all matrices which store data accross time point are nt x nv where nt is
        % th number of time points and nv is the number of variables

        x_est = zeros(length(td),nxint);
        x_est_min = zeros(length(td),nxint);
        x_est_max = zeros(length(td),nxint);
        x_est_std = zeros(length(td),nxint);

        y_est = zeros(length(td),ny);
        y_est_std = zeros(length(td),ny);
        y_est_min = zeros(length(td),ny);
        y_est_max = zeros(length(td),ny);

        % statistics for the first step
        x_est(1,:) = median(x_P');
        x_est_min(1,:) = min(x_P');
        x_est_max(1,:) = max(x_P');
        x_est_std(1,:) = std(x_P');


        y_est(1,:) = median(y_init') ;         
        y_min(1,:) = min(y_init');
        y_max(1,:) = max(y_init');
        y_est_std(1,:) = std(y_init');

        % get the values of the measured outputs at the initial point td(1)
        for ii = 1:length(P.m)
            y_act(1,ii) = interp1(oldtime,m.(P.m{ii}).Data,td(1));
        end
        
        % define a matrix to hold mean values of parameters to be perturbed
        par_est = zeros(length(td),length(P.idx));
        
        % fill the first row with the initial parameter values
        for k=1:length(P.idx)
            par_est(1,k) = obj.persist.model_p.(P.idx{k}).Value;  
        end
        
        par_est_std = zeros(length(td),length(P.idx)); % initialize a matrix for parameter stds

        %% Particle filtering
        
        for i=1:length(td)-1    % loop through time points

            disp([num2str(i) ' of ' num2str(length(td)-1)])

            qi = zeros(P.N,1);                  % holds probabilities of individual particles
            yk_n = zeros(P.N,length(P.m));      % holds the predicted outputs + added measurement noise
            xk = zeros(nxint,P.N);              % holds updated states of particle simulations
            Y = zeros(ny,P.N);                  % holds outputs of particle simulations
            par_k = zeros(length(P.idx),P.N);   % holds parameters of individual particles

            par_est_slice = par_est(i,:);   % for parallel computing: mean parameters at time i

            % get the values of the measured outputs 
            for ii = 1:length(P.m)
                y_act(i+1,ii) = interp1(oldtime,m.(P.m{ii}).Data,td(i+1));
            end

            % Main loop through the particles
            tic
            disp(['Calculation step: ' num2str((td(i+1)-td(i))*3600) ' seconds']);
            
            %parfor j=1:P.N
            for j=1:P.N
                
                %% Generate random process noise to be applied to model parameters
                p_new = obj.persist.model_p; % create a new parameter structure

                % add noise to the chosen parameters
                par_ = par_est_slice + P.par_error' .* par_est_slice .* randn(1,length(P.idx));
                par_(par_<0)=0;

                % store in matrix holding parameters or all particles
                par_k(:,j) = par_';

                % Update the parameter structure which is required for simulations
                for k=1:length(P.idx)
                    p_new.(P.idx{k}).Value = par_(k); 
                end
                
                % create a new model structure so that new parameters can be simulated
                model_new = model;
                model_new.p = p_new; 
                
                deltaT = td(i+1)-td(i);
                
                % perform the main simulation for each particle
                x_in = x_P(:,j);
                x = [];
                try
                    [~,~,x] = BPTT.tools.model_sim_conf(u,x_in,[td(i) td(i+1)],model_new,0);
                catch
                   x = x_in';
                end
                
                % add process noise to states after the time propogation
                % amount of process noise depends on the time interval being simulated
                x_prop = x(end,:)';
                x_prop =  x_prop + ( P.process_noise' .* randn(nxint,1).* deltaT) ;
                x_prop(x_prop<0)=1e-5; % setting the state value equal to zero if process noise makes it negative
                
                % calculating the output 
                [~,ymod,~]=BPTT.tools.model_sim_conf(u,x_prop,td(i+1),model_new,2);
                
                xk(:,j) = x_prop;
                Y(:,j)=ymod;                  % outputs of all particles will be stored in Y
                yk = ymod(my_idx)';

                % add measurement noise 
                R = zeros(length(P.m),length(P.m));
                for k=1:length(P.m)
                    R(k,k)=P.meas_error(k)^2;
                end

                yk_n(j,:) = yk' + sqrt(diag(R)) .* yk' .* randn(length(P.m),1);
                
                % Compute the relative likelihood (qi) of each particle conditioned on
                % the measurement yk. That is the relative likelihood qi that the measurement
                % is equal to a specific measurement y* given the premise that xk
                % is equal to the particle xki
                %qi(j) = (1/(((2*pi)^(length(P.m)/2))*sqrt(norm(R)))) *  exp(-1*( (y_act(i+1,:)-yk_n(j,:)) * inv(R) * (y_act(i+1,:)-yk_n(j,:))' ) / 2 );
                e_yact = (y_act(i+1,:)-yk_n(j,:));
                qi(j) = (1/(((2*pi)^(length(P.m)/2))*sqrt(norm(R)))) *  exp(-1*( e_yact/R * e_yact' ) / 2 );
                
                %disp('----------')
                %disp([y_act(i+1,:) ;  yk_n(j,:)] )
               
            end
            
            toc

            %% Resampling
            
            % what this code specifically does is randomly, uniformally, sample from
            % the cummulative distribution of the probability distribution
            % generated by the weighted vector P_w.  If you sample randomly over
            % this distribution, you will select values based upon their statistical
            % probability, and thus, on average, pick values with the higher weights
            % (i.e. high probability of being correct given the observation z).
            % store this new value to the new estimate which will go back into the
            % next iteration

            if doPFdiagnostics
                % store the original qi for plotting purposes
                qi_orig = qi;
            end % if doPFdiagnostics
            
            % regularization
            qi = ((P.alpha - 1).* qi + mean(qi) ) ./ P.alpha;
            P_q = qi ./ sum(qi);
            
            ind = zeros(1,P.N);
            % resampling based on uniform sampling of the probability distribution           
            if P.resampling_method == 1
                
                for j = 1:P.N
                    rn = rand;      % select only from p>0.5

                    % use try catch to avoid error when outliers arise
                    try
                        ind(j) = find(rn <= cumsum(P_q),1);

                    catch err % in case of the singularity error, select randomly           
                        warning(err.message)
                        ind(j) = ceil(rand*P.N);
                    end
                end
                x_P = xk(:,ind);
                % P_q after resampling
                %P_q_res = P_q(ind);
               
                y_est(i+1,:) = median(Y(:,ind)');
                y_est_min(i+1,:) = min(Y(:,ind)'); 
                y_est_max(i+1,:) = max(Y(:,ind)'); 
                y_est_std(i+1,:)=std(Y(:,ind)');

                par_est(i+1,:) =  mean(par_k(:,ind)');
                par_est_std(i+1,:) =  std(par_k(:,ind)');
                
            % resampling based on best 10% of candiates
            elseif P.resampling_method == 2
                
                for j = 1:P.N
                    ind(j) = ceil(rand*P.N*0.1 + P.N-0.1*P.N );
                end

                M = [P_q xk'];
                M_sorted = sortrows(M);
                x_P = M_sorted(ind,2:end)';

                M = [P_q Y'];
                M_sorted = sortrows(M);
                Y = M_sorted(ind,2:end)';

                M = [P_q par_k'];
                M_sorted = sortrows(M);
                par_k = M_sorted(ind,2:end)';
                
            	y_est(i+1,:) = median(Y');
                y_est_min(i+1,:) = min(Y'); 
                y_est_max(i+1,:) = max(Y'); 
                y_est_std(i+1,:)=std(Y');

                par_est(i+1,:) =  mean(par_k');
                par_est_std(i+1,:) =  std(par_k');
                
                %P_q_res = P_q(ind);
                
            end
            
            % diagnostic plot
            if doPFdiagnostics
                % P_q after resampling
                P_q_res = P_q(ind);
                
                BPTT.diag.particle_filter_diagnistics( yk_n , y_act(end,:) , qi_orig , qi, P_q , P_q_res)
                drawnow
            end

            
            %% store mean/median/std values of all particles: states, outputs, and parameters

            x_est(i+1,:) = median(x_P');
            x_est_min(i+1,:) = min(x_P');
            x_est_max(i+1,:) = max(x_P');
            x_est_std(i+1,:) = std(x_P');
            
            %% replace the xint values with the estimated ones
            obj.p.xint = x_est(i+1,:);   
        end
        
    else % when the time vector is empty or has only one entry
        
       x_est = []; 
       y_est = [];
       y_est_std=[];
       par_est = [];
       par_est_std = [];
       t_out = [];
       
	end
    
    %% define calculator outputs
   
    % one output variable for states
    y.PF_states = BPTT.TimeVariable;
    y.PF_states.Name = 'PF_states';
    y.PF_states.Data = x_est;
    y.PF_states.Time = t_out;   
    y.PF_states.TimeInfo.Units = 'days';
    y.PF_states.DataInfo.Units = '';
    
    % output variable for outputs
    for i=1:length(output_names)
        on = ['PF_' output_names{i}];
        y.(on) = BPTT.TimeVariable;
        y.(on).Name = on;
        if ~isempty (y_est)
            y.(on).Data = y_est(:,i);
            y.(on).sdev = y_est_std(:,i); 
            y.(on).Time =  t_out;
            %y.(on).DataInfo.Unit = yi.(output_names{i}).Unit;
        end
        
        y.(on).TimeInfo.Unit = 'days';
        
    end
    
    % add estimated parameters to the output
    % also update the model parameter structure with new estimated values
    for i=1:length(P.idx)
        on = ['PF_' P.idx{i}];
        y.(on) = BPTT.TimeVariable;
        y.(on).Name = on;
        
        if ~isempty (par_est) && ~isempty (par_est_std)
            
            y.(on).Data = par_est(:,i);
            y.(on).sdev = par_est_std(:,i); 
            y.(on).Time =  t_out;
            obj.persist.model_p.(P.idx{i}).Value = par_est(end,i);
            %y.(on).DataInfo.Unit = obj.persist.model_p.(P.idx{i}).Unit;
            
        end
        
        y.(on).TimeInfo.Unit = 'days';
        
    end
    
end
