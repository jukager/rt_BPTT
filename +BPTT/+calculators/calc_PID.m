function y = calc_PID( x_in , obj)
    % Discrete PID controller implementation
    % first input = Setpoint/ second Process value
    % minpoints = min 2 -> set dedband for integration part
    % (c) Julian Kager May 2016
    
    P = obj.p; % get the parameter structure

    x_names = fieldnames(x_in);
    
    persistent error_prior integral_prior
    % needs a persistent variable for initial states
    if ~isfield(obj.persist,'outputs')
    obj.persist.outputs = [0 0 0];
    end
    if isempty(integral_prior)
    integral_prior = 0;
    end
    if isempty(error_prior)
    error_prior = 0;
    end
    if P.autoresetTi == 1 %autoreset Ti load
    integral_prior = 0;    
    end
    % interpolation lowest
    TS = BPTT.tools.interplowest(x_in);  

    % stores DATA and TIME in matrices with corresponding column i
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
    
    %%

    y.(P.output_name) = BPTT.TimeVariable; 
    y.(P.output_name).Name = P.output_name;
    
    if TS.(x_names{1}).Length > 2
    iteration_time = (TS.(x_names{i}).Time(end)- TS.(x_names{i}).Time(1))*24*60*60;
    
    error = (x{1}(end) - x{2}(end))/x{2}(end)*100;
    integral = integral_prior + (error*iteration_time);
    derivative = (error - error_prior)/iteration_time;
    
    output_raw = sum([P.KP*error P.KI*integral, P.KD*derivative , P.bias]);
    
    %normalization
    obj.persist.outputs(end+1,1)= P.KP*error;%/output_raw;
    obj.persist.outputs(end+1,2)= P.KI*integral;%/output_raw;
    obj.persist.outputs(end+1,3)= P.KD*derivative;%/output_raw;
    data = output_raw;%sum(obj.persist.outputs(end,:))+P.bias;%/output_raw; 
    
    error_prior = error ;
    integral_prior = integral;
    
        data(isinf(error))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time(end);
    end
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = P.output_units;
    
    
 end