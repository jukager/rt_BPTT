function [y]  = calc_MPC_C( x_in , obj)
    % Calculator for model predictive controller 
    % Aydin Golabgir 26.08.2014
    % x_in is a structure containing the data of the input signals for the
    % calculator which in this case consists of process values, setpoints,
    % and the state vector for the model
    % obj is the calculator object
    % y, the output of the calulator contains the controller output
    
    % get the previous inputs from the calculator object's buffer(?)
    persistent prev_inputs
   
    P = obj.p;% read the calculator parameters from the object
    model= P.Model;
    output_names = fieldnames(model.y);
    state_names = fieldnames(model.x);
    x_names = fieldnames(x_in);             % get the names of the calculator input signals
    %TS = BPTT.tools.interplowest(x_in);    % interpolate input signals
    
    % get the model states out of the input vector
    states = x_in.(P.model_states{1});
   
    
    % get the length of the input vectors
    N = states.Length;
    
    % prepare the output variables
    for i=1:length(P.u)
        name = ['MPC_' P.u{i}];
        y.(name) = BPTT.TimeVariable;
        y.(name).Name = name;
    end
    
    setpoint_min_length = 0;
    for i=1:length(P.setpoints)
        if x_in.(P.setpoints{i}).Length > setpoint_min_length
            setpoint_min_length = x_in.(P.setpoints{i}).Length;
        end
    end
    
    if N>1 && setpoint_min_length>1 % perform calculations only if there exists a state estimation value
        
        % get the latest time of the process value variables and set it as tnow
        tnow = states.Time(end);
        last_state = states.Data(end,:);
        
        t_mpc = [0;P.deltaT]; % time interval for mpc simulations
        
        % generate the random inputs for mpc simulations
        for i=1:P.N
            % generate the input structure for simulations
            for j=1:length(P.u)
                u.(P.u{j}) = BPTT.TimeVariable;
                u.(P.u{j}).Name = P.u{j};
                u.(P.u{j}).Time = t_mpc;
                interval = P.max_input(j) - P.min_input(j);
                rand_inputs(i,j) = (rand * interval + P.min_input(j));
                u.(P.u{j}).Data = ones(size(t_mpc,1),1) .* rand_inputs(i,j);
            end

            for j= length(P.u)+1 :length(P.u_c)
                u.(P.u_c{j}) = BPTT.TimeVariable;
                u.(P.u_c{j}).Name = P.u_c{j};
                u.(P.u_c{j}).Time = t_mpc;
                u.(P.u_c{j}).Data = ones(size(t_mpc,1),1) .* x_in.(P.u_c{j}).Data(end);
            end
            U{i}=u; % store the random input structures in a cell array
        end

        % get the index of the model outputs so that they can be used later for comparison of the setpoint and model outputs
        
        %yi = feval(P.model_name, 0, last_state, [] , 3, P.model_p , U{1});
        all_out_names = output_names;                             % get all of the model output names out of the structure
        for i=1:length(P.model_outputs)                             % find the index of the model outputs
            idx = find(strcmp(all_out_names,P.model_outputs{i}));   % find the index of the output
            if isempty(idx)                                         % perform some error checking
                error([P.model_outputs{i} ' not found in the model structure!'])
            end
            out_idx(i)=idx;                                         % assign the output index to a vector for later use
        end
        
        % get the values of setpoints at time (tnow + deltaT) and store in a vector
        for i = 1:length(P.setpoints)
            tint = tnow + (P.deltaT/24); % tnow + deltaT in units of days
            t = x_in.(P.setpoints{i}).Time;
            d = x_in.(P.setpoints{i}).Data;
            setpoint(i) = interp1(t,d,tint,'linear','extrap');
        end
        

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % performing the mpc simulations / parallel implementation
        %U_parfor = Struct
        tic
        parfor i=1:P.N
            
            % perform a simulation using the compiled simulator
            y_prop = []; % initialize the x vector to avoid parfor warnings
            U_parfor = U{i};
            [~,y_prop,~] = BPTT.tools.model_sim_conf(U_parfor,last_state,t_mpc',P.Model,1);
            %[~,x] = BPTT.tools.model_sim_C(P.model_p , U{i} , last_state ,t_mpc); % perform the actual simulation
            %x_prop = x(end,:)'; % propogated x values at (tnow + deltaT)
            
            % calculate the values of model output at (tnow + deltaT)
            %y_mod =  feval(P.model_name, t_mpc(2), x_prop, [] , 1, P.model_p , U{i});
            %pred_test(i,:) = y_prop(out_idx,end)
            pred(i,:) = y_prop(out_idx,end)
        end
        toc
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % calculating the value of the cost function

        
        for i=1:size(pred,2)
           %w(i) = mean(abs(pred(:,i)-setpoint(i)));
           w(i) = setpoint(i);
           if w(i)==0,w(i)=1e-6;end

           %di(:,i) = (pred(:,i) - setpoint(i)).^2 ./ w(i) + (uw(i) .* rand_inputs(:,i)./);
           di(:,i) = sqrt((pred(:,i) - setpoint(i)).^2) ;
           %di(:,i) = di(:,i) .* w1(i) ./ w(i) ;
           %di(:,i) = di(:,i)-min(di(:,i));
           di(:,i) = di(:,i) ./ w(i);
           di(:,i) = di(:,i) .* P.weight(i);
        end

        % penalty for input deviation

        
        if isempty(prev_inputs)
            prev_inputs = zeros(1,length(P.u));
        end

        
        % normalize the input by division by the allowed input range/interval
        input_interval = P.max_input - P.min_input;
        input_interval(input_interval==0)=1e-24; % I dont like division by zero!
      
        
       
        %% novel method for setting weights for the cost function / input penalty
        % if the input has a small effect on all outputs, then it gets more
        % expensive to change it. To evaluate the effect of t

        % get the initial values of the outputs
        %[t,y_init,~] = BPTT.tools.model_sim_conf(U{i},last_state,t_mpc(1),P.Model,2);
        %y_init = feval(P.model_name, t_mpc(1), last_state, [] , 1, P.model_p , U{i});
        %pred_init = y_init(out_idx);    
        
        if 0
            warning off
            uv = [];
            for j=1:size(di,2)
                X = zscore(rand_inputs,1);
                %Y = (pred(:,j)-pred_init(j))./setpoint(j);
                Y = (pred(:,j)-pred_init(j))./input_interval(j);
                Y = Y-mean(Y);
                b = regress(Y,X);
                uv(j,:) = b';
            end
            warning on

            uv = sum(abs(uv));
            uv(uv==0) = 1e-2;
            uv = 1./uv;
        else
            % no input penalty based on sensitivity
            uv = ones(1,size(rand_inputs,2));
        end
        
        
        
        %%
        
        % compute the difference between the current and past inputs and normalize it
        for i=1:length(P.u)
            diff_inputs(:,i) = abs(rand_inputs(:,i) - prev_inputs(i)) .* uv(i) ./ input_interval(i);
            %diff_inputs(:,i) = abs(rand_inputs(:,i) - 0 ) .* uv(i) ./ input_interval(i);
        end 
      
        
       
        s = sum(di');% + sum(diff_inputs' ); % JKA deactivation of penalty
        
        % find the index of the minimum of the cost function
        min_idx = find(s == min(s));


        if isfield(P,'plot')
            if P.plot == 1
        
                %% making the plots
                figure(2)
                subplot(4,2,1)
                hist(di(:,1),20)
                title('Predicted distance u1')
                
                subplot(4,2,2)
                hist(di(:,2),20)
                title('Predicted distance u2')
                
                subplot(4,2,3)
                hist(diff_inputs(:,1),20)
                title('Input penalty u1')
                
                subplot(4,2,4)
                hist(diff_inputs(:,2),20)
                title('Input penalty u2')
                
                subplot(4,2,5)
                plot(di(:,1),rand_inputs(:,1),'.')
                line([min(di(:,1)) max(di(:,1))],[rand_inputs(min_idx,1) rand_inputs(min_idx,1)],'Color','r')
                xlabel('predicted distance')
                ylabel('controller output')
                
                subplot(4,2,6)
                plot(di(:,2),rand_inputs(:,2),'.')
                line([min(di(:,2)) max(di(:,2))],[rand_inputs(min_idx,2) rand_inputs(min_idx,2)],'Color','r')
                xlabel('predicted distance')
                ylabel('controller output')
                
                subplot(4,2,7)
                hist(pred(:,1))
                title('Predictions 1')
        
                subplot(4,2,8)
                hist(pred(:,2))
                title('Predictions 2')
        
            end
        end
        
        %% remember the previous input values       
        %if isempty(rand_inputs(min_idx,:))
        %    rand_inputs(min_idx,:)= 1;
        %end
        prev_inputs = rand_inputs(min_idx,:);
        
        rand_inputs(min_idx,:)

        % return the output variables
        for i=1:length(P.u)
            name = ['MPC_' P.u{i}];
            y.(name).Data = rand_inputs(min_idx,i);
            if length(min_idx)> 1
            y.(name).Data = rand_inputs(end,i);
            end    
            y.(name).Time = tnow;
        end
        

    else % if the input vectors do not have any data points
        %error('Input vectors do not have enough data! Why??')
    end
  
end