function [ y ] = calc_convert_variables ( x,obj )
    % needs as parameter a string 'output_unit'; Additionally parameter
    % 'type' can either be 'Data' or 'Time', depending on what should be
    % converted.
    % for instance: P.output_unit = 'mL';
    
    x_names = fieldnames(x);
    parameters = obj.p;
    output_unit = parameters.output_unit;
    
    if ~isfield(parameters, 'type')
        parameters.type = 'Data';
    elseif ~any(strcmp(parameters.type,{'Data','Time'}))
        error('Only "Data" and "Time" are allowed for parameter "Type".');
    end
    
    actual_unit = x.(x_names{1}).DataInfo.Units;
    if isempty(actual_unit)
        error('No Units seem to be given for time variable, therefore convertion is not possible!');
    end
    actual_unit = BPTT.tools.remove_brackets_and_whitespace(actual_unit);
    converted_data = x.(x_names{1}).(parameters.type);
    converted_sdev = x.(x_names{1}).sdev;
    
    if ~strcmp(output_unit, actual_unit)
        
        [output_unit_sections, output_unit_operators] = separate_units(output_unit);
        [actual_unit_sections, actual_unit_operators] = separate_units(actual_unit);
        
        for index = 1:numel(output_unit_sections)
            
            if (index>1) && ~strcmp(output_unit_operators{index-1}, actual_unit_operators{index-1})
                error('calc_convert_variables:unequalOperators',['Input and output operators are not equal. Maybe check the order of units.']);
            end
            
            if is_time_unit(output_unit_sections{index})
                conversion_factor = convert_time(actual_unit_sections{index}, ...
                                                 output_unit_sections{index});
            elseif ~isnan(str2double(output_unit_sections{index}))
                conversion_factor = str2double(output_unit_sections{index});
            else
                unit_to_convert = ['1 ' actual_unit_sections{index}];

                [prefix_1,~] = BPTT.tools.sip2num(unit_to_convert);

                [prefix_2,~] = BPTT.tools.sip2num(['1' output_unit_sections{index}]);

                conversion_factor = prefix_1/prefix_2;
            end
            
            if index == 1 || strcmp(output_unit_operators{index-1},'*')
            elseif strcmp(output_unit_operators{index-1},'/') || strcmp(output_unit_operators{index-1},'\')
                conversion_factor = 1/conversion_factor; 
            end
            
            converted_data = converted_data*conversion_factor;
            switch parameters.type
                case 'Data'
                    converted_sdev = converted_sdev*conversion_factor;
            end
        end

    end
    
    y.([x_names{1} '_conv']) = x.(x_names{1});
    y.([x_names{1} '_conv']).(parameters.type) = converted_data;
    y.([x_names{1} '_conv']).sdev = converted_sdev;
    y.([x_names{1} '_conv']).DataInfo.Units = output_unit;
end

function [separated_string, operators] = separate_units(unit)
    delimiters = {'/','\','*'};
    [separated_string, operators] = strsplit(unit, delimiters);
end

function [answer] = is_time_unit(unit)
    time_units = {'days','d','hours','h','minutes','min''seconds','sec','s','milliseconds','nanoseconds'};
    result = strcmp(unit, time_units);
    if ~isempty(find(result,1))
        answer = true;
    else
        answer = false;
    end
end

function [conversion_factor] = convert_time(original_time_unit, time_unit)
    f = struct();
    f.d = 24;
    f.days = f.d;
    f.h = 1;
    f.hours = f.h;
    f.min = 1/60;
    f.minutes = f.min;
    f.s = f.min/60;
    f.sec = f.s;
    f.seconds = f.s;
    f.milliseconds = f.s/1000;
    f.nanoseconds = f.milliseconds/1000;
    
    conversion_factor_to_hours = f;
    
    conversion_factor = conversion_factor_to_hours.(original_time_unit) ...
                        / conversion_factor_to_hours.(time_unit);
end