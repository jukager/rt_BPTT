function y = calc_offgas_SAN( x , obj)
    % Offgas calculator for Sandoz dataset
    % Inputs: input signals must have the following names and units:

        % 'DRUCK'   [bar]
        % 'TEMP'    [C]  
        % 'LUFT'    [l/min]
        % 'AB_CO2'  [%]
        % 'AB_O2'   [%]
        % 'GEWICHT' [kg]
    
    % Required parameters:
    
        % P.MW_Luft= 28.954;            % Molmasse der Zuluft
        % P.L_dichte = 1.293;           % Zuluft Eichdichte 1.293
        % P.u_d = 1.01325;              % Umgebungsdruck
        % P.zu_h2o = 0.5;               % wird in LVA gemessen
  
    % Outputs:
        % CO2_mol       [mol/h]
        % O2_mol        [mol/h]
        % QCO2          [mol/kg/h]
        % QO2           [mol/kh/h]
        % RQ            [mol/mol]
        % Verdunstung   [kh H2O / h]
    
    P = obj.p; % get the parameter structure
    
    % interpolate the input signals
    x = BPTT.tools.interplowest(x);
   
    
    
    %% define the output timevariables
    y.CO2_mol = BPTT.TimeVariable; 
    y.CO2_mol.Name = 'CO2_mol';
    y.CO2_mol.DataInfo.Units = 'mol/h';
    
    y.O2_mol = BPTT.TimeVariable; 
    y.O2_mol.Name = 'O2_mol';
    y.O2_mol.DataInfo.Units = 'mol/h';
    
    y.QO2 = BPTT.TimeVariable; 
    y.QO2.Name = 'QO2';
    y.QO2.DataInfo.Units = 'g/kg/h';
    
    y.QCO2 = BPTT.TimeVariable; 
    y.QCO2.Name = 'QCO2';
    y.QCO2.DataInfo.Units = 'g/kg/h';
    
    y.RQ = BPTT.TimeVariable; 
    y.RQ.Name = 'RQ';
    y.RQ.DataInfo.Units = 'mol/mol';
    
    y.Verdunstung = BPTT.TimeVariable; 
    y.Verdunstung.Name = 'Verdunstung';
    y.Verdunstung.DataInfo.Units = 'kg/h';
    
    y.r_atmungsverlust = BPTT.TimeVariable; 
    y.r_atmungsverlust.Name = 'r_atmungsverlust';
    y.r_atmungsverlust.DataInfo.Units = 'kg/h';
    
    %% Input signal check
    %Check whether there are enough points in the interpolated input
    %signals for performing calculations
    fn = fieldnames(x);     % get the fieldnames of the input structure
    N = x.(fn{1}).Length;   % get the length of data points in the input structure - all of the data ponits have the same length due to interpolation

    if N>1
    
        %% Unit conversions on input signals
        x.LUFT.Data = x.LUFT.Data * 60 / 1000;        % l/min in m3/h
        
        % get the common time vector
        common_time = x.(fn{1}).Time;

        %% Calculations
        % absolute pressure inside the reactor [bar]
        PFerm= P.u_d + x.DRUCK.Data;

        % reactor temperature [K]
        Temp_ferm = 273.16 + x.TEMP.Data;

        % mol H2O/mol Zuluft
        zu_dampf_mm = P.zu_h2o / 100;     

        % Molekulargewicht Luft [g/mol]
        Zu_MolGew=(0.78084*28.014+0.00934*39.948+0.20946*31.998+0.00033*44.009)*(1-zu_dampf_mm)+(zu_dampf_mm*18.015);

        % g HO2 / kg inlet
        zu_dampf_gk=18.015*zu_dampf_mm/Zu_MolGew*1000;

        % g H2O / m3 inlet
        wa_dampf_zu_gm3 =zu_dampf_gk * P.L_dichte; 

        wa_zu_kgh = wa_dampf_zu_gm3 .* x.LUFT.Data / 1000;

        %  Wasser Partialdruk fermenter
        P_D = exp(-6655./Temp_ferm -4.54*log(Temp_ferm)+56.26) / 100000;

        AB_H2O = P_D ./ PFerm .* 100;

        % Messluft
        NL = 1 - P_D ./ PFerm;

        % Abgasbilanz ohne Korrektur der Harnstoffzersetzung

        % F30
        ab_o2_kor = x.AB_O2.Data .* NL;
        ab_co2_kor = x.AB_CO2.Data .* NL;

        zu_n2 = 79.019 * (1-zu_dampf_mm);

        L_kor= zu_n2 ./(100 -  ab_o2_kor - ab_co2_kor - AB_H2O);

        zu_o2=20.946*(1-zu_dampf_mm);       
        zu_co2=0.033.*(1-zu_dampf_mm);

        %F51 mol/h
        O2_mol=max(0,(x.LUFT.Data * 10 * P.L_dichte / P.MW_Luft ).*(zu_o2 - L_kor .* ab_o2_kor));    
        
        y.O2_mol.Time = common_time;
        y.O2_mol.Data = -1 .* O2_mol; % negative for soft-sensor purposes
        
        %F50
        CO2_mol=(x.LUFT.Data * 10 * P.L_dichte/P.MW_Luft ).*( L_kor .* ab_co2_kor - zu_co2);    

        y.CO2_mol.Time = common_time;
        y.CO2_mol.Data = CO2_mol;
        
        
        % RQ calculation
        RQ=CO2_mol./O2_mol;
        
        y.RQ.Time = common_time;
        y.RQ.Data = RQ;
        
        % g/kgh
        QCO2=44.009*CO2_mol./x.GEWICHT.Data;
        
        y.QCO2.Time = common_time;
        y.QCO2.Data = QCO2;
        
        QO2=31.998*O2_mol./x.GEWICHT.Data;
        
        y.QO2.Time = common_time;
        y.QO2.Data = QO2;

        %% Abgef�hrt, Abluft
        AB_Wasser_mol=P_D ./ PFerm .* L_kor;
        AB_Wasser_gkg=18.015*AB_Wasser_mol*1000/Zu_MolGew;
        AB_Wasser_gm3=AB_Wasser_gkg.*P.L_dichte;
        AB_Wasser_kgh=AB_Wasser_gm3.*x.LUFT.Data/1000;
        Verdunstung_kgh=(wa_zu_kgh-AB_Wasser_kgh) * -1;
        %s_verd=cumsum(Verdunstung_kgh.*dt);
        
        y.Verdunstung.Time = common_time;
        y.Verdunstung.Data = Verdunstung_kgh;
        
        % Gewichtsverlust durch Atmung
        r_av=QCO2-QO2;      % g/kg h
        r_atmungsverlust = r_av .* x.GEWICHT.Data ./1000;
        y.r_atmungsverlust.Time = common_time;
        y.r_atmungsverlust.Data = r_atmungsverlust;
    
    end
    
end