function y = calc_K2S1_v2(x_in, obj)
    % Estimating cx based on a linear correlation (OD or Perm signal)
    % (c) Julian Kager 
    
    P = obj.p;      % translate constant to function inputs
        M_s= P.M_s ;            % molecular weight of substrate	g/C-mol
        c_s= P.c_s;           % substrate concentration in feed 	g/l
        y_wet= P.y_wet;        % oxygen content of wet process air
        y_o2_in_air =P.y_o2_in_air;   % mole fraction of oxygen in inlet air
        y_co2_in_air =P.y_co2_in_air;% mole fraction of carbon dioxide in inlet air
        gamma_s =P.gamma_s;          % Degree of reduction of substrate
        gamma_o2 =P.gamma_o2;        % Degree of reduction of oxygen 
        gamma_x =P.gamma_x;      % Degree of reduction of biomass
        e_co2 =P.e_co2;         % relative error oxygen measurement
        e_o2 =P.e_o2 ;          % relative error carbon dioxide measurement
        e_s =P.e_s ;           % relative error substrate feed
        Vol=P.V;                 % reactor Volume = 1 to calculate absolute rates
        rx_ext = 1;
        e_x = 1;
    
    x_names = fieldnames(x_in); % assigning x_names to inputs
    TS = BPTT.tools.interplowest(x_in); % interpolation
 
    if isfield(P,'sim_Start') % gives the possibility to start simulation at an indicated timepoint p.sim_Start
            sim_Start= P.sim_Start; % the case where sim start is a date -> days since reftime
        for i=1:length(x_names)
            del_idx_min = find(TS.(x_names{i}).Time < (sim_Start)); % delete inputs before sim_Start
            TS.(x_names{i}) = TS.(x_names{i}).delsample('Index',del_idx_min);
        end
    end       
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
    
    %y.(P.output_name) = BPTT.TimeVariable;
    %y.(P.output_name).Name = P.output_name;
    
    for i=1:length(P.output_name)
    on = ['K2S1_' P.output_name{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    %y.(on).DataInfo.Unit = xi.(state_names{i}).Unit;
    y.(on).TimeInfo.Unit = 'days';
    end
    
if TS.(x_names{1}).Length > 0 
    % assigning names to K2S1 inputs
%         Fs = (x{1});
%         F_a_in=(x{2});
%         F_o2_in= (x{3});
%         y_o2_out= (x{4})/100;
%         y_co2_out= (x{5})/100;
data = zeros(length(x{1}),11);
for i= 1: length(x{1})         
        % assigning names to K2S1 inputs
        Fs = x{1}(i);
        F_a_in= x{2}(i)./60;
        F_o2_in= x{3}(i)./60;
        y_o2_out= x{4}(i)./100;
        y_co2_out= x{5}(i)./100;
    
    data(i,1:11)= BPTT.tools.K2S1_v2(Fs, c_s, M_s, F_a_in, F_o2_in, y_o2_out, y_co2_out, y_wet, y_o2_in_air, y_co2_in_air, gamma_s, gamma_o2, gamma_x, Vol, rx_ext, e_x, e_o2, e_co2, e_s);
end     
    % one output variable for states
    for i=1:length(P.output_name)
        on = ['K2S1_' P.output_name{i}];
        y.(on).Data = data(:,i);
        y.(on).Time =  t{1};
    end
    
%     y.(P.output_name) = BPTT.TimeVariable;
%     y.(P.output_name).Name = [P.output_name '_states'];
%     y.(P.output_name).Data = data;
%     y.(P.output_name).Time = TS.(x_names{1}).Time;   
%     y.(P.output_name).TimeInfo.Units = 'days';
%     y.(P.output_name).DataInfo.Units = '';
%         
%      y.(P.output_name).Quality = TS.(x_names{1}).Quality;
     %y.(P.output_name).TimeInfo.Unit = TS.(x_names{1}).TimeInfo.Units;
     %y.(P.output_name).DataInfo.Unit = P.output_units;
    end 
 end