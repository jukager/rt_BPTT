function y = calc_regression( x_in , obj)
% Calculator for obtaining the regression coefficients
% (c) Ines Stelzer & Julian Kager 23.02.2016

P = obj.p;                          % parameter structure

% definition of a persistent variable for model parameters

x_names = fieldnames(x_in);         % assign x-names to inputs
TS = BPTT.tools.interplowest(x_in); % interpolation of inputs

for i=1:length(x_names)
    x{i} = TS.(x_names{i}).Data;    % getting data vector from inputs
    t{i} = TS.(x_names{i}).Time;    % getting time vector from inputs
end

% defining persit fields if not available
if ~isfield(obj.persist,'slope') || ~isfield(obj.persist,'offset') ||~isfield(obj.persist,'timestamp') ||~isfield(obj.persist,'rsq')
    obj.persist.slope = P.regkoeff(1);
    obj.persist.offset = P.regkoeff(2);
    obj.persist.timestamp = now; %t{1}(1);
    obj.persist.rsq = 0;
end

%%
y.(P.output_name) = BPTT.TimeVariable;
y.(P.output_name).Name = P.output_name;

if TS.(x_names{2}).Length < 2
    FIT = P.regkoeff;
    obj.persist.slope(1,1) = FIT(1);
    obj.persist.offset(1,1) = FIT(2);
    obj.persist.timestamp(1,1) = now;
    
end

if strcmp(P.regmodel,'linear')
    if TS.(x_names{2}).Length >= 2 && TS.(x_names{1}).Length >= 2;      % assuming that
        
        FIT = polyfit(x{1}, x{2},1);
        
        if TS.(x_names{2}).Length > obj.p.forgetting     % assuming that
            FIT = polyfit(x{1}(end-obj.p.forgetting:end,1), x{2}(end-obj.p.forgetting:end,1),1);
        end
        obj.persist.slope(end+1,1) = FIT(1); % store values in persist field
        obj.persist.timestamp(end+1,1) = t{i}(end,1);
        obj.persist.offset(end+1,1) = FIT(2);
        % compute r squared
        yfit = polyval(FIT,x{1}); % evaluate function
        yresid = x{2} - yfit;         % calc residuals
        SSresid = sum(yresid.^2);   %Square the residuals and total them to obtain the residual sum of squares:
        SStotal = (length(x{2})-1) * var(x{2}); %Compute the total sum of squares of y by multiplying the variance of y by the number of observations minus 1
        disp(['linear model y = ' num2str(FIT(1)) ' * x ' num2str(FIT(2))])
        rsq = 1 - SSresid/SStotal % Compute R2
        obj.persist.rsq(end+1,1) = rsq;
    end
    y.(P.output_name).Data = FIT;
    
end
if strcmp(P.regmodel,'exp') && TS.(x_names{2}).Length >= 2 && TS.(x_names{1}).Length >= 2;
    
    [FIT, gof3] = fit(x{1}, x{2},'exp1')
    
    obj.persist.slope(end+1,1) = FIT.b;
    obj.persist.timestamp(end+1,1) = t{i}(end,1);
    obj.persist.offset(end+1,1) = FIT.a;
    obj.persist.rsq(end+1,1) = gof3.rsquare;
    y.(P.output_name).Data = [FIT.b FIT.a];
end

y.(P.output_name).Time = t{2}(end,1)+3/3600; % addition of 3 minutes to exclude datapoint for next input

end