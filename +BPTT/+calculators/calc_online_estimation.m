function y = calc_online_estimation(x_in, obj)
    % Estimating cx based on a linear correlation (OD or Perm signal)
    % (c) Julian Kager & Ines Stelzer 23.02.2016
    
    P = obj.p;      % parameter structure
    
    if P.recalibration == 1
    % definition of a persistent variable for model parameters
    obj.persist.slope = obj.o.Calculators.calc_reg.persist.slope(end,1);
    obj.persist.offset = obj.o.Calculators.calc_reg.persist.offset(end,1);
    else 
    obj.persist.slope = obj.o.Calculators.calc_reg.persist.slope(1,1);
    obj.persist.offset = obj.o.Calculators.calc_reg.persist.offset(1,1);
    end    
        
    x_names = fieldnames(x_in); % assigning x_names to inputs
    TS = BPTT.tools.interplowest(x_in); % interpolation
    
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end

    y.(P.output_name) = BPTT.TimeVariable;
    y.(P.output_name).Name = P.output_name;
    
    if TS.(x_names{1}).Length > 0 && strcmp(P.regmodel,'linear')
   data = obj.persist.offset+obj.persist.slope*x{1};
        
        data(isinf(data))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time; % only one variable existing
    end
    
    if TS.(x_names{1}).Length > 0 && strcmp(P.regmodel,'exp')
   data = obj.persist.offset*exp(obj.persist.slope*x{1});
        
        data(isinf(data))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time; % only one variable existing
    end
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{1}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = P.output_units;

 end