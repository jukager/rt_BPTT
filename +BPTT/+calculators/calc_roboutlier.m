function y = calc_roboutlier( x , obj )
    % Robust outlier detection for BPTT TimeVariables in BPTT objects. 
    % Aydin Golabgir 13.08.2014  

    % get the data from the ingoing struct
    P = obj.p; % get the parameter structure
    x_names = fieldnames(x);
    TS = x.(x_names{1}); 

    % remove NaNs from the timevariable
    idx = find(isnan(TS.Data));
    TS = delsample(TS,'Index',idx);

    TS_new = TS;
    TS_new = delsample(TS_new,'Index',1:TS_new.Length);
    
    w = P.window;
    if mod(w,2)==0
        w = w + 1;
    end
    
    if TS.Length > w
    
        [x_out,y_out,I] = BPTT.tools.roboutlier(TS.Time,TS.Data,P.cutoff,w);
        TS_new.Time = x_out;
        TS_new.Data = y_out;

        %% outlier detection 
        if ~isempty(P.sign) 

            if strcmp(P.sign,'positive') % using the sign criteria for positive conditions
               idx =  find(TS_new.Data < 0);
               TS_new = delsample(TS_new,'Index',idx);
            end

            if strcmp(P.sign,'negative') % using the sign criteria for negative conditions
               idx =  find(TS_new.Data > 0);
               TS_new = delsample(TS_new,'Index',idx);
            end
        end

        %% outlier detection using the maximum of the absolute value
        if ~isempty(P.maxabs)
           idx =  find(abs(TS_new.Data) > P.maxabs);
           TS_new = delsample(TS_new,'Index',idx);
        end

        %% apply a moving average filter
        if ~isempty(P.ma) && P.ma
            TS_new.Data = smooth(TS_new.Data,w);
        end

        % apply a rlowess filter
        if ~isempty(P.rlowess) && P.rlowess
            TS_new.Data = smooth(TS_new.Time,TS_new.Data,P.window,'rlowess');
        end

    end

    %% plotting if required
    if P.plot
        figure
        subplot(2,1,1)
        plot(TS.Time,TS.Data,TS_new.Time,TS_new.Data,'r')
        legend('Original signal','New signal')

        subplot(2,1,2)
        plot(TS_new.Time,TS_new.Data,'r')
        legend('New signal')

    end
    
    %%
    % Specify output (BPTT TimeVariable) 
    % first set the output equal to the original signal - also takes the
    % Quality values from the original signal
    TS_new.Name = [TS.Name '_outlier'];
    if length(TS.Quality)>0
        TS_new.Quality = interp1(TS.Time,TS.Quality,TS_new.Time,'nearest');
    end
    y.([TS.Name '_outlier']) = TS_new; 
    
    
    
end

