function [y]  = calc_particlefilter_IST_v2( x_in , obj)
% ParticleFilter implementation in BPTT
%% (c) Aydin Golabgir, Julian Kager, Ines Stelzer
% 21.07.2016 Beta Version JKA

% The implementation is based on Simon,D. 2006 Optimal State estimation (Chapter 15; page 461-481)
% calc_particle filter is a calculator function and gets its inputs
% from the calculator object which are the model inputs (P.u) and
% measurements (P.m). The estimated states and model parameters are
% returned as timevariables. Consider the Particle_configuration_example
% file to configure the Calculator
%
%  See also
%  Helpbrowser reference page
% <a href="matlab:doc BPTT.calculators.calc_particlefilter;">doc BPTT.calculators.calc_particlefilter</a>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ensuring compatability to old version (Aydin 22.10.2016)
if ~isfield(obj.p, 'errors');
    obj.p.errors = 'relative' ;         % standard = relative errors
end
if ~isfield(obj.p, 'outputname');
    obj.p.outputname = 'PF' ;           % PF if no name indicated
end
if ~isfield(obj.p, 'combination');
    obj.p.combination = '' ;            % 
end
if ~isfield(obj.p, 'reciever');
    obj.p.reciever = obj.Name;          % overwrites own results
end
if strcmp(obj.p.reciever,'')           % if empty to avoid errors
    obj.p.reciever = obj.Name;         % overwrites own results
end
doPFdiagnostics = false;            % local constant, enable diagnostic plot
if isfield(obj.p, 'diagnostics');
    doPFdiagnostics = obj.p.diagnostics;
end
if ~isfield(obj.p,'starttime')
    obj.p.starttime = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% retrieval of variables from function input (x_in, obj)
% separation of model inputs and measurements
P = obj.p;                              % reads the calculator parameters from the calculator object
x_names = fieldnames(x_in);             % gets the names of the input signals
model = P.model;                        % reads the model from the calculator
TS = BPTT.tools.interplowest(x_in);     % interpolates input signals to lowest !!! different for input and measurements!!!
output_names = fieldnames(model.y);     % gets the output names from the model struct
state_names = fieldnames(model.x);      % gets the state names from the model file
td = [];                                % simtime vector

% definition of a persistent variable for model parameters
if ~isfield(obj.persist,'model_p')
    obj.persist.model_p = P.model.p;
end

% deletion of data before starttime for real-time integration & testing
if P.starttime ~= 0 
   for i=1:length(x_names)
       del_idx_min = find(TS.(x_names{i}).Time < (P.starttime)); % delete inputs before sim_Start
       TS.(x_names{i}) = TS.(x_names{i}).delsample('Index',del_idx_min);
   end 
end

L = TS.(x_names{1}).Length;             % check length of 1st input time variable

% define the time vector of input data
if L > 0
    td = (TS.(x_names{1}).Time  - TS.(x_names{1}).Time(1)) .* 24;
    oldtime = td; % store timestamp as oldtime
    if ~isempty(P.interval)
        t0 = min(td);
        tend = max(td);
        td = t0:P.interval:tend;
    end
    t_out = ((td/24) +  TS.(x_names{1}).Time(1));
end
% separates inputs and measurements from each other
% retrieves model inputs and converts time into relative hours taking more
% timepoints
for i=1:length(P.u)
    u_raw.(x_in.(P.u{i}).Name) = x_in.(P.u{i});
end
u = BPTT.tools.interplowest(u_raw);
if L > 0
    % stores the model inputs in the u struct (relative time in hours)
    td_reference = u.(u.(P.u{1}).Name).Time(1); %to align other interpolated data if necessary
    for i=1:length(P.u)
        %Time = u.(TS.(P.u{i}).Name).Time
        %Data = u.(TS.(P.u{i}).Name).Data
        %u.(P.u{i}) = (u.(P.u{i}).addsample('Time',Time,'Data',Data)); %add Time/Data to model input       
        u.(u.(P.u{i}).Name).Time = (u.(u.(P.u{i}).Name).Time - u.(u.(P.u{i}).Name).Time(1)).*24;
    end
%else
         %u.(u.(P.u{i}).Name).Time = [0; 1];
         %u.(u.(P.u{i}).Name).Data = [0; 1];
end    
% for i=1:length(P.u)
%     u.(TS.(P.u{i}).Name)=TS.(P.u{i});
%     if L > 0
%         u.(TS.(P.u{i}).Name).Time = (u.(TS.(P.u{i}).Name).Time - u.(TS.(P.u{i}).Name).Time(1)).*24;
%     else
%         u.(TS.(P.u{i}).Name).Time = [0; 1];
%         u.(TS.(P.u{i}).Name).Data = [0; 1];
%     end
% end

% retrives mesurements and orders them to match model outputs
% sort P.m and P.my according to model.y
my_order = length(P.my);
for i=1:length(P.my)
    my_order(i) = find(strcmp(output_names,P.my{i})); % find index
end
[~,~,rnk] = unique(my_order); % rank from 1 to length(P.my)
rnk = rnk';
m_orig = P.m;
my_orig = P.my;
meas_error_y_orig = P.meas_error_y;

for i=1:length(P.my) %rewrites P.m/ P.my/ P.meas_error
    P.m(i)= m_orig(rnk(i));
    P.my(i)= my_orig(rnk(i));
    P.meas_error_y(i)= meas_error_y_orig(rnk(i));    
end
% stores the measurements in the m struct for later use
for i=1:length(P.m)
    m.(P.m{i}) = TS.(P.m{i});
end
% initializes a matrix of zeros for actual outputs (measurements) for later use
y_act=zeros(length(td),length(P.m));

% finds out the existence of the measured outputs in model outputs
my_idx = zeros(length(output_names),1);
for i=1:length(P.my)
    my_idx = my_idx + strcmp(output_names,P.my{i});
end
my_idx = logical(my_idx>0);

%% checks whether the time vector td has enough points for performing any kind of calculation
if length(td) > 1
    %% Simon 2006; p.468 Step 1: The system is described by (1) the process model (states) including process noise, parameters and inputs u (e.g. feeding rates) and
    %(2) the output equations y being dependent on the states x, parameters and inputs u and measurement noise
     
    %% Simon 2006; p.468 Step 2: Generation of P.N particels due to the randomly generated particles from the initial prior gaussian distribution;
    %  IST: see comments beginning with IST
    
    nxint = length(state_names);     % number of states x
    ny = length(output_names);       % number of outputs y %% IST: Brauche am Anfang y-Berechnung nicht; Annahme y_0=0
    
    x_P = zeros(nxint,P.N);          % Initialize a vector to hold the N particles for the states
    y_init = zeros(ny,P.N);          % Initialize a vector to hold the calculated initial outputs of the particles; assumption y_0=0 (mathematical assumption)
 
    if strcmp(P.errors,'absolute')  %JKA absolute option assigns zeros to xint
        xint= ones(length(P.xint)) ; 
    else
        xint = P.xint;
    end    
    for i = 1:P.N %% creating the initial particles and calculate their outputs
            x_P(:,i) = P.xint' + (P.initial_std' .* xint' .* randn(nxint,1));    % adding random noise based on prededfined relative standard deviation of initial state
            x_P(x_P(:,i)<0,i)=1e-5;                                                % remove negative state variables
            [t,y_init(:,i),~]=BPTT.tools.model_sim_conf(u,x_P(:,i),td(1),model,2); % calculate outputs for the initial population
    end
    
    %% Initializing matrices to hold estimated states (x) and outputs (y) for different time points
    %  Matrices which store data for all time points
    
    x_est = zeros(length(td),nxint);                      % Estimation of state vector x for several time points
    x_est_min = zeros(length(td),nxint);
    x_est_max = zeros(length(td),nxint);
    x_est_std = zeros(length(td),nxint);
    
    y_est = zeros(length(td),ny);                         % Estimation of output vector y for several time points
    y_est_std = zeros(length(td),ny);
    y_est_min = zeros(length(td),ny);
    y_est_max = zeros(length(td),ny);
    
    % Statistics for the first time step
    
    x_est(1,:) = median(x_P');
    x_est_min(1,:) = min(x_P');
    x_est_max(1,:) = max(x_P');
    x_est_std(1,:) = std(x_P');
    
    
    y_est(1,:) = median(y_init');                         % IST: assumption y_0=0 due to mathematical theory
    y_min(1,:) = min(y_init');
    y_max(1,:) = max(y_init');
    y_est_std(1,:) = std(y_init');
    
    % Define a matrix to hold mean values of parameters to be perturbed
    % IST: parameter estimation not conatined in Simon; can be tricky
    
    par_est = zeros(length(td),length(P.idx));
    par_est_ref = zeros(length(td),length(P.idx));
    % Fill the first row with the initial parameter values and store
    % within the calculator object
    
    
    for k = 1:length(P.idx)
        par_est_ref(1,k) = model.p.(P.idx{k}).Value; % reference parameter set to hold parameter adaption constant
        par_est(1,k) = obj.persist.model_p.(P.idx{k}).Value;
    end

    par_est_std = zeros(length(td),length(P.idx)); % Initialize a matrix for parameter stds
    
    %% Particle filtering
    
    for i=1:length(td)-1    % Loop through time points
        
        disp([num2str(i) ' of ' num2str(length(td)-1)])
        
        qi = zeros(P.N,1);                  % Relative likelihood that estimated measurement basing upon P.N particles (states) for time step i is equal to real measured value
        yk_n = zeros(P.N,length(P.m));      % Holds the predicted outputs y due to P.N particles and model equations
        xk = zeros(nxint,P.N);              % Holds updated particles (states) due to process model
        Y = zeros(ny,P.N);                  % Holds outputs of particle simulations
        par_k = zeros(length(P.idx),P.N);   % Holds parameters of individual particles
        % Predefine Variables for Parallel loop
        par_est_ref = par_est_ref(1,:);     % for parallel computing startparameter to hold deviation constant
        par_est_slice = par_est(i,:);       % For parallel computing: mean parameters at time i 
        p_new_parfor = obj.persist.model_p; % Create a new parameter structure
        
        % Get the values of the (real) measured outputs and calculate errors
        for ii = 1:length(P.m) 
            y_act(i+1,ii) = interp1(oldtime,m.(P.m{ii}).Data,td(i+1));
        end
        % Creating Covariance matrix of
        meas_error = zeros(length(P.m));
        for ii= 1: length(P.m)
            if y_act(i+1,ii) == 0 % if 0 take absolute value of relative error
                meas_error(ii) = P.meas_error_y(ii);
            else
                meas_error(ii) = P.meas_error_y(ii) * y_act(i+1,ii)^2;
            end
        end
        
        if strcmp(P.errors,'absolute') %JKA absolute option
            meas_error = P.meas_error_y;
        end
        % IST: Generation of R
        R = zeros(length(P.m),length(P.m));
        for ii = 1:length(P.m)
            R(ii,ii) = meas_error(ii); %% IST: ge�ndert
        end
       
        tic
        disp(['Calculation step: ' num2str((td(i+1)-td(i))) ' hours']);
        % Main loop through the particles
        parfor j=1:P.N % use parfor for parallelization -> requires parallel calculation toolbox and multi core processor or cluster access
            % Store in matrix holding parameters or all particles
            p_new= p_new_parfor;
            
            % Add noise to the chosen parameters
            par_ = par_est_slice + P.par_error' .* par_est_ref .* randn(1,length(P.idx));
            par_(par_<0)=0;              % Set negative parameters to zero
            par_k(:,j) = par_';
            % Update the parameter structure which is required for simulations
            for k=1:length(P.idx)
                p_new.(P.idx{k}).Value = par_(k);
            end
            
            % Create a new model structure so that new parameters can be simulated
            model_new = model;
            model_new.p = p_new;
            
            % IST: Simulation for each particle - solving differential equation to
            % obtain particles for the next time step
            
            x_in = x_P(:,j);
            x=[];
            try
                [~,~,x] = BPTT.tools.model_sim_conf(u,x_in,[td(i) td(i+1)],model_new,0);
            catch
                x = x_in';
            end
            
            % Process noise (noise on process model): add process noise to states after the time propogation
            % IST: Assumption: process noise is a random variable distributed according
            % to multivariate normal distribution and process noise is not "too small"
            
            x_prop = x(end,:)';  % IST: for time td(i+1) calculate x_{td(i+1),j}^{-} for j=1,...,P.N
           
            %for l = 1 : nxint
            %if abs(x_prop(l))< 0.001 
            %end
            %end
            if strcmp(P.errors,'relative') %JKA absolute option
            x_prop =  x_prop + ( P.process_error_mean'.* x_prop .* randn(nxint,1)); %addition of  relative process noise to model states, without taking time into account JKA
            elseif strcmp(P.errors,'absolute')
            x_prop =  x_prop + ( P.process_error_mean' .* randn(nxint,1)); %addition of  relative process noise to model states, without taking time into account JKA
            end
            x_prop(x_prop<0)=1e-5;                                         % State variables are not allowed to become negative
            
            % Calculating the output due to the particles
            
            [~,ymod,~]=BPTT.tools.model_sim_conf(u,x_prop,td(i+1),model_new,2);
            
            xk(:,j) = x_prop;
            Y(:,j)=ymod;                  % Outputs of all particles will be stored in Y
            yk = ymod(my_idx)';
            
            % Simon 2006 p.466
            % IST: Noise on measurements v; assumption that noise is linear in measurement equation and that it is distributed according to multivariate normal distribution
            
            mu_v = zeros(ny,1); % IST: mean of v
            
            % IST: Covariance matrix - assumption errors do not influence each other,
            % i.e. covariance matrix R is a diagonal matrix - form mathematical point of view entries are not allowed to be 0
            
            % IST: Loop guaranteeing that diagonal entries of R are not eual to zero
            
            yk_n(j,:) = yk'; % IST: no noise here
            
            % Compute the relative likelihood (qi) for each particle
            
            e_yact = (y_act(i+1,:)-yk_n(j,:)); % IST: Difference of real measurement and estimated measurement basing upon particles
            
            % IST: Estimation of qi very simplified -> solving high dimensional integrals
            
            qi(j) = (1/(((2*pi)^(length(P.m)/2))*sqrt(det(R)))) * exp(-1/2*( e_yact * inv(R) * e_yact' ) );
            
        end
        
        toc
        
        if doPFdiagnostics
            % Store the original qi for plotting purposes
            qi_orig = qi;
        end                 % If doPFdiagnostics
        
        %% Resampling (see Simon p. 467)
        % Sampling of important particles - using standard methods (Method 1 is called "weighted bootstrap", Smith and Gelfand 1992)
        
        % Normalization of qi
        
        P_q = qi ./ sum(qi);
        P_q_kummuliert = cumsum(P_q);  % IST: Vector whose components are qi that are summed up
        rn = rand(1,P.N);              % IST: rand generates uniformly distributed random numbers in [0,1]
        ind = zeros(1,P.N);
        
        if P.resampling_method == 1
            try
            for j = 1:P.N
                ind(j) = find(P_q_kummuliert>=rn(j),1); %% IST: Variablen nur umgestellt - cum ist Vektor mit kummulativen Eintr�gen der qi.
            end
            catch
            ind = ones(length(P.N));     
             warning('Resampling failed, propagation of all particles')
            end    
            x_P = xk(:,ind);                    % IST: Resampled P.N particles
            
            y_est(i+1,:) = median(Y(:,ind)');   % IST: y as function of x - y = h(x)
            y_est_min(i+1,:) = min(Y(:,ind)');
            y_est_max(i+1,:) = max(Y(:,ind)');
            y_est_std(i+1,:)=std(Y(:,ind)');
            
            par_est(i+1,:) =  mean(par_k(:,ind)');
            par_est_std(i+1,:) =  std(par_k(:,ind)');
            
            % Resampling based on best 10% of candiates %% Methode 2
            % not supported anymore
        elseif P.resampling_method == 2
            
            for j = 1:P.N
                ind(j) = ceil(rand*P.N*0.1 + P.N-0.1*P.N );
            end
            
            M = [P_q xk'];
            M_sorted = sortrows(M);
            x_P = M_sorted(ind,2:end)';
            
            M = [P_q Y'];
            M_sorted = sortrows(M);
            Y = M_sorted(ind,2:end)';
            
            M = [P_q par_k'];
            M_sorted = sortrows(M);
            par_k = M_sorted(ind,2:end)';
            
            y_est(i+1,:) = median(Y');
            y_est_min(i+1,:) = min(Y');
            y_est_max(i+1,:) = max(Y');
            y_est_std(i+1,:)=std(Y');
            
            par_est(i+1,:) =  mean(par_k');
            %                 <<<<<<< HEAD                          % Merging conflict
            %                 par_est_std(i+1,:) =  std(par_k',0,1);
            %                 =======
            par_est_std(i+1,:) =  std(par_k');
            
        end  % End of resampling
        
        % Diagnostic plot
        if doPFdiagnostics
            P_q_res = P_q(ind);
            BPTT.diag.particle_filter_diagnistics( yk_n , y_act(end,:) , qi_orig , qi, P_q , P_q_res)
            drawnow
        end
        
        %% Store mean/median/std values of all particles
        
        x_est(i+1,:) = median(x_P');
        x_est_min(i+1,:) = min(x_P');
        x_est_max(i+1,:) = max(x_P');
        x_est_std(i+1,:) = std(x_P',0,1);
        
        %% Replace the xint values with the estimated ones
        
        obj.p.xint = x_est(i+1,:);
        for k=1:length(P.idx)
            obj.persist.model_p.(P.idx{k}).Value = par_est(i+1,k);
        end
        
        %% Parallel combination; the enabled calculator will transfer information to the selected object
        
        if strcmp(P.combination,'parameters') % parameters from persist will be transfered
            obj.o.Calculators.(P.reciever).persist.model_p = obj.persist.model_p;
        elseif strcmp(P.combination,'parameters_and_states')% additional estimated states are transfered
            obj.o.Calculators.(P.reciever).persist.model_p = obj.persist.model_p;
            obj.o.Calculators.(P.reciever).p.xint = obj.p.xint;
        end
        
    end % End loop time
    
else % when the time vector is empty or has only one entry
    
    x_est = [];
    y_est = [];
    y_est_std=[];
    par_est = [];
    par_est_std = [];
    t_out = [];
    
end

%% Define calculator outputs, that will be stored within the Bioprocess object

y.(P.outputname) = BPTT.TimeVariable;
y.(P.outputname).Name = [P.outputname '_states'];
y.(P.outputname).Data = x_est;
y.(P.outputname).Time = t_out;
y.(P.outputname).TimeInfo.Units = 'days';
y.(P.outputname).DataInfo.Units = '';

% output variable for outputs
for i=1:length(output_names)
    on = [P.outputname '_' output_names{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    if ~isempty (y_est)
        y.(on).Data = y_est(:,i);
        y.(on).sdev = y_est_std(:,i);
        y.(on).Time =  t_out;
        %y.(on).DataInfo.Unit = yi.(output_names{i}).Unit;
    end
    y.(on).TimeInfo.Unit = 'days';
end

% Add estimated parameters to the output
% also update the model parameter structure with new estimated values
for i=1:length(P.idx)
    on = [P.outputname '_' P.idx{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    
    if ~isempty (par_est) && ~isempty (par_est_std)
        
        y.(on).Data = par_est(:,i);
        y.(on).sdev = par_est_std(:,i);
        y.(on).Time =  t_out;
        obj.persist.model_p.(P.idx{i}).Value = par_est(end,i);
        %y.(on).DataInfo.Unit = obj.persist.model_p.(P.idx{i}).Unit;
        
    end
    y.(on).TimeInfo.Unit = 'days';
end
end