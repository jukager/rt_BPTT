function [ y ] = calc_specific_growth_rate( x,obj)
    % Variable x input (in that order):
    %   - biomass conc
    %   - reactor_volume
    %   - substrate_feed
    %   - nitrogen_feed
    %   - acid_feed
    %   - base_feed
    %   - supplementary_feed
    
    x_adjusted = adjust_x_for_growth_rate(x);
    obj.p.specific_growth_rate = true;
    
    y = BPTT.calculators.calc_basic_rate(x_adjusted,obj);
    
end

function [x_adjusted] = adjust_x_for_growth_rate(x)
    x_names = fieldnames(x);
    x2.filler1 = x.(x_names{1});
    x3.filler2 = x.(x_names{1});
    x_adjusted = BPTT.tools.catstruct(x2,x,x3);
end

