function y = calc_export( x_in , obj)
    % A  calculator format to insert specific calculations
    % Julian Kager Dez 2016
    
    P = obj.p; % get the parameter structure
   
    x_names = fieldnames(x_in);
    
    if isfield(P, 'interp1')
        if strcmp(P.interp1,'highest')
        TS = BPTT.tools.interphighest(x_in);
        elseif strcmp(P.interp1,'lowest')
        TS = BPTT.tools.interplowest(x_in);      
        else % estendable to more options
        TS = BPTT.tools.interplowest(x_in);   
        end
    else
    TS = BPTT.tools.interplowest(x_in);  
    end
    
    % stores DATA and TIME in matrices with corresponding column i
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
    
    %%
    % read out specified output names
    y.(P.output_name) = BPTT.TimeVariable; 
    y.(P.output_name).Name = P.output_name;
    
    if TS.(x_names{1}).Length > 0 % does it only when datapoints are avaliable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
data = zeros(length(x{1}),length(x));
% write timeaxis into 1st column (hardcoded)
data(:,1) = t{1};
Header{1} = 'Timestamp days';
Units{1} = 'days';
% loop for reasigning matrix and construct Header (same order as selected
% inputs)
for i = 1 : length(x)
data(:,i+1) = x{i};
Header{i+1} = strjoin([x_names(i),x_in.(x_names{i}).DataInfo.Units]);  
Units{i+1} = x_in.(x_names{i}).DataInfo.Units;
end

% xls write 
 xlswrite(P.output_name,  Header, 'Sheet1')
 xlswrite(P.output_name, data, 'Sheet1', 'A2')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
        data(isinf(data))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time;
   
    end
    
    disp(['congrats your file is saved as ' cd '\' P.output_name]) 
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = Units;
    
    
 end