function y = calc_offgas_PKR( x_in , obj)
    % Function for calculating OUR and CER in aerobic cultures where offgas
    % signals and aeration rates are measured.
    % Julian Kager February 2016
    
    P = obj.p; % get the parameter structure

    % get the time series and interpolate at the lowest frequency
    x_names = fieldnames(x_in);
    
    % perform calculation only if data is available
    if x_in.(x_names{1}).Length > 0 && x_in.(x_names{2}).Length > 0 && x_in.(x_names{3}).Length > 0 && x_in.(x_names{4}).Length > 0
    
        TS = BPTT.tools.interplowest(x_in);
    % stores DATA and TIME in matrices with corresponding column i
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
%     
% -parameters-
% pressure in the reactor [pa]
rawdata.pR=P.Pressure;

% ideal gas constant [J/(mol k)]
rawdata.R=P.R;

% norm temp
rawdata.Tn=P.Tn; %[k]

% norm pressure
rawdata.pn=P.Pn;

% -Time and initial values-
% initial Working Volume [mL]
rawdata.Vinit = P.Vinit;

% total Volume of the vessel [mL]
rawdata.Vges  = P.Vges;

% time variables
caldata.H2Ooff = zeros(size(x{1}))*P.H2Ooff; % [%] Wasseranteil im Offgas
caldata.Time = t{1}*24;
caldata.yO2off = x{1};
caldata.yCO2off = x{2};
caldata.Airin = x{3}*1000;
caldata.O2in = x{4}*1000;
caldata.TR = x{5}+273.15;
caldata.pO2 = x{6};
caldata.pH = x{7};
caldata.VR = x{8};
caldata.CO2in = zeros(size(x{1}));
caldata.N2in = zeros(size(x{1}));

%%  Calculations, normierungen, einheitenkram
% Headspace [mL]
caldata.Vhead =rawdata.Vges-caldata.VR;

% O2 total in [mLn/h]
caldata.O2int  = (caldata.O2in+0.20942*caldata.Airin);

% CO2 total in [mLn/h]
caldata.CO2int = (caldata.CO2in +0.00039*caldata.Airin);

% N2 total in [mLn/h]
caldata.N2int  = (caldata.Airin*0.7808+caldata.N2in);

% MFC total Flow in [mLn/h]
caldata.Vint   = (caldata.O2in+caldata.CO2in+caldata.Airin...
                +caldata.N2in);

% MFC inert Flow in [mLn/h]
caldata.inert=caldata.Vint-caldata.O2int-caldata.CO2int;

% CO2 in % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -Offgas Values-

% O2 offgas [VolO2/Vol]
caldata.O2off = caldata.yO2off/100;

% CO2 offgas [VolCO2/Vol]
caldata.CO2off= caldata.yCO2off/100;

% H2O offgas [VolH2O/Vol]
caldata.H2Ooff= caldata.H2Ooff/100;

%% Calculation of mol flow in the reactor

% O2 in [mol/h]
Calc.Inlet.O2total=(caldata.O2int*10^-6*rawdata.pn)./(rawdata.R*rawdata.Tn);

% CO2 in [mol/h]
Calc.Inlet.CO2total=(caldata.CO2int*10^-6*rawdata.pn)./(rawdata.R*rawdata.Tn);

% Inert in[mol/h]
Calc.Inlet.Inerttotal=((caldata.Vint-caldata.CO2int-caldata.O2int)*10^-6*rawdata.pn)./(rawdata.R*rawdata.Tn);
%%  Gesamtbilanz Reaktor

Calc.Reactor.nGast=(caldata.Vhead*10^-6*rawdata.pn)./(rawdata.R*rawdata.Tn);
Calc.Reactor.dnGastdt=[0; diff(Calc.Reactor.nGast)./diff(caldata.Time)];

Calc.Reactor.nGasO2=Calc.Reactor.nGast.*caldata.O2off;
Calc.Reactor.dnGasO2dt=[0; diff(Calc.Reactor.nGasO2)./diff(caldata.Time)];

Calc.Reactor.nGasCO2=Calc.Reactor.nGast.*caldata.CO2off;
Calc.Reactor.dnGasCO2dt=[0; diff(Calc.Reactor.nGasCO2)./diff(caldata.Time)];

Calc.Reactor.nGasIne=Calc.Reactor.nGast.*(caldata.CO2off+caldata.O2off+caldata.H2Ooff);
Calc.Reactor.dnGasInedt=[0; diff(Calc.Reactor.nGasIne)./diff(caldata.Time)];

%% Calculation of Vout

% Annahme: TRinnert = 0

%   Inertmassenstrom Abgas
Calc.Outlet.Inerttotal = Calc.Inlet.Inerttotal-Calc.Reactor.dnGasInedt;

%   Gesamtmassenstrom Abgas
Calc.Outlet.Gastotal   = Calc.Outlet.Inerttotal./...
    (1-(caldata.CO2off+caldata.O2off+caldata.H2Ooff));

%   Volumenstrom Abgas []
Calc.Outlet.Vout       = Calc.Outlet.Inerttotal./...
    (1-(caldata.CO2off+caldata.O2off+caldata.H2Ooff))...
    *(rawdata.R*rawdata.Tn)/(10^-6*rawdata.pn);

%   O2-massenstrom Abgas
Calc.Outlet.O2total = Calc.Outlet.Gastotal.*caldata.O2off;

%   CO2-massenstrom Abgas
Calc.Outlet.CO2total = Calc.Outlet.Gastotal.*caldata.CO2off;

%% Transportraten

%   [mol/h]
Calc.TR.O2total=Calc.Outlet.O2total-Calc.Inlet.O2total+Calc.Reactor.dnGasO2dt;
Calc.TR.CO2total=Calc.Outlet.CO2total-Calc.Inlet.CO2total+Calc.Reactor.dnGasCO2dt;

%   [mol/h/L]
Calc.TR.O2totalvol=Calc.TR.O2total./(caldata.VR/1000);
Calc.TR.CO2totalvol=Calc.TR.CO2total./(caldata.VR/1000);


%% OU CE
Calc.TR.OU=cumtrapz(caldata.Time(1:end-2),Calc.TR.O2totalvol(1:end-2));
Calc.TR.CE=cumtrapz(caldata.Time(1:end-2),Calc.TR.CO2totalvol(1:end-2));

%%
deltaT=0.5;
intertime=[0:deltaT:caldata.Time(end-2)]';
Inter.OU=interp1(caldata.Time(1:end-2),Calc.TR.OU,intertime);
Inter.CE=interp1(caldata.Time(1:end-2),Calc.TR.CE,intertime);
Inter.OTR=diff(Inter.OU)/deltaT;
Inter.CER=diff(Inter.CE)/deltaT;

Inter.OTRtr=interp1(intertime(1:end-1),Inter.OTR,caldata.Time(1:end-2),'previous');
Inter.CTRtr=interp1(intertime(1:end-1),Inter.CER,caldata.Time(1:end-2),'previous');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Transportraten fertig!!
    end
    
%% 
    y.OTR = BPTT.TimeVariable; 
    y.OTR.Name = 'OTR';
    y.OTR.Data = Inter.OTR;
    y.OTR.Time = intertime(1:end-1)/24;
    y.OTR.Quality = TS.(x_names{1}).Quality;
    y.OTR.TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.OTR.DataInfo.Unit = 'mol/h/l';
    
    y.CTR = BPTT.TimeVariable; 
    y.CTR.Name = 'CTR';
    y.CTR.Data = Inter.CER;
    y.CTR.Time = intertime(1:end-1)/24;
    y.CTR.Quality = TS.(x_names{1}).Quality;
    y.CTR.TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.CTR.DataInfo.Unit = 'mol/h/l';

end
% %% Offgas calculator configuration
% 
% clear P
% clear c
% 
% P.Pressure = 1.0133*10^5 ;   % pressure in the reactor [pa]
% P.R = 8.3144621; % ideal gas constant [J/(mol k)]
% P.Tn = 273.15; % norm temp
% P.Pn = 1.0133*10^5; % norm pressure
% P.Vinit = 1500; % initial Working Volume [mL]
% P.Vges = 3000; % total Volume of the vessel [mL]
% P.H2Ooff = 1; % [%] Wasseranteil im Offgas
% 
% c = BPTT.Calculator;                            % define a new Calculator object
% c.Name = 'calc_offgas';                         % specify a name for the calculator
% c.f = @BPTT.calculators.calc_offgas_PKR;            % specify which function file to use for calculating outputs
% c.p = P;                                        % assign parameter values
% %c.x= O2_offgas CO2_offgas Air_in O2_in temperature DO2 pH 
% c.x = {'XOout','XCOout','FAir_PV','FO','T_PV', 'DO_PV', 'pH_PV','V_PV'};            % specify the names of the input signals
% 
% c.Mode = 'online';                          % calculation mode: online vs. offline
% c.minpoints = 1;                            % minimum points to perform a partial calc.
% c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations
% 
% B.AddCalculator(c);