function y = calc_despike( x_in , obj)
    % A  calculator format to insert specific calculations
    % Julian Kager April 2017
    
    P = obj.p; % get the parameter structure
    if ~isfield(obj.p, 'bandwidth')
    P.bandwidth = 0.01 ;            %
    end
    
    x_names = fieldnames(x_in);
    
    % interpolation lowest 
    TS = BPTT.tools.interplowest(x_in);  
    
    % stores DATA and TIME in matrices with corresponding column i
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
    
    %%
    % read out specified output names
    y.(P.output_name) = BPTT.TimeVariable; 
    y.(P.output_name).Name = P.output_name;
    
    if TS.(x_names{1}).Length > 0 % does it only when datapoints are avaliable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% but here your fancy calculations and write them on data vector
  
[data, Id] = BPTT.tools.despike(x{1}, P.bandwidth, P.bandwidth);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
        data(isinf(data))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time;
   
    end
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = P.output_units;
    
    
 end