function y = calc_empty( x_in , obj)
    % A  calculator format to insert specific calculations
    % Julian Kager Dez 2016
    
    P = obj.p; % get the parameter structure
    if P.why
        why;
    end
    
    x_names = fieldnames(x_in);
    
    % interpolation lowest 
    TS = BPTT.tools.interplowest(x_in);  
    
    % stores DATA and TIME in matrices with corresponding column i
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
    
    %%
    % read out specified output names
    y.(P.output_name) = BPTT.TimeVariable; 
    y.(P.output_name).Name = P.output_name;
    
    if TS.(x_names{1}).Length > 0 % does it only when datapoints are avaliable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% but here your fancy calculations and write them on data vector
% a funny MLR    
data = zeros(length(x{1}),length(x));

for i = 1 : length(x)
data(:,i) = x{i};
end

% xls write 
% xlswrite('MyExport',  c.x, 'Sheet1')
% xlswrite('MyExport', bp.Variables.Export.Data, 'Sheet1', 'A2')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
        data(isinf(data))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time;
   
    end
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = P.output_units;
    
    
 end