function y = calc_math_cut( x_in , obj)
    % A generic calculator for performing mathematical operations on TimeVariables
    % (c) Aydin Golabgir 17.08.2014
    % Julian Kager May 2016
    
    P = obj.p; % get the parameter structure

    x_names = fieldnames(x_in);
    
    % interpolation lowest (default) and compatability to old configurations
    % new selectable interpolation to highest timeframe
    if isfield(P, 'interp1')
        if strcmp(P.interp1,'highest')
        TS = BPTT.tools.interphighest(x_in);
        else % estendable to more options
        TS = BPTT.tools.interplowest(x_in);   
        end
    else
    TS = BPTT.tools.interplowest(x_in);  
    end
    
    % stores DATA and TIME in matrices with corresponding column i
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data;
        t{i} = TS.(x_names{i}).Time;
    end
    
    %%

    y.(P.output_name) = BPTT.TimeVariable; 
    y.(P.output_name).Name = P.output_name;
    
    if TS.(x_names{1}).Length > 0
        data = eval(P.exp);
        
        if isfield(P,'max')
           data(data>P.max)=nan; 
        end
        
        if isfield(P,'min')
           data(data<P.min)=nan; 
        end
        
        data(isinf(data))=nan;
        y.(P.output_name).Data = data;
        y.(P.output_name).Time = TS.(x_names{1}).Time;
    end
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = P.output_units;
    
    
 end