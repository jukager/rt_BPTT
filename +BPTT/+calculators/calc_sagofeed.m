function y = calc_sagofeed( x, obj)
    % Calculator for calculating the feedrate in [g/h] from a balance
    % signal in [g] using the sago savitzky golay implementation
    % Aydin Golabgir 13.08.2014
    P = obj.p; % get the parameter structure
    x_names = fieldnames(x);
    balance = x.(x_names{1});
    
    % remove any non-monotically increasing points from input signal
    idx = find(diff(balance.Time)==0);
    balance = delsample(balance,'Index',idx);
    
    window = P.SG_window;
    if mod(window,2)==0
        window = window + 1;
    end
    
    t_out = [];
    y_out = [];
    
    if balance.Length>=window % perform calculations only if there are enough data points available
    
        %% taking the derivative
        DN = 1; % differential order should be 1 for taking the first derivative

        % check whether window size is larger than SG polynomial order
        if window > P.SG_pol_order
            weight = ones(balance.Length, 1);
           [t_out,y_out] = BPTT.tools.sago(balance.Time, balance.Data,P.SG_pol_order,DN, window,weight, 0);
        end
    
    end
    
    %% Defining the output structure
    
    output = BPTT.TimeVariable;
    output.Data = -1 .* y_out ./ 24; % converting units from [g/days] to [g/hours] 
    output.Time = t_out;
    output.Name = [ x_names{1} '_feedrate'];
    output.DataInfo.Unit = 'g/h';
    
    output.TimeInfo.Unit = balance.TimeInfo.Units;
    if length(balance.Quality)>0
        output.Quality = interp1(balance.Time,balance.Quality,output.Time,'nearest');
    end
    
    y.(output.Name) = output;
    
end
 
