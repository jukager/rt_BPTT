function [y]  = calc_model_sim_conf( x_in , obj)
% Simulation of a model: returns the model outputs (y) and states (x) given the
% inputs (u). initial states (P.xinit) and model inputs u (available within bioprocess named as model input) must be given.
% P.Prediction can be indicated to look into future by using last input
% as constant or predefined optimal feed profile. It is capable of working in online and offline modes.
% model outputs are avalable as timevariabels within the bioprocess
% model with structure format are supported
% (c) Julian Kager, April 2016

P = obj.p;                          % read the calculator parameters from the object
%model = P.Model;                    % read the model structure passed as parameter
if isfield(P, 'Modelname')
    %modelname = fieldnames(obj.o.Models);
    model = obj.o.Models.(P.Modelname);
else
model = P.Model;                    % read the model structure passed as parameter
end
% needs a persistent variable for initial states
if ~isfield(obj.persist,'x_init')
    obj.persist.x_init = P.xinit;
end

u_names = fieldnames(x_in);          % get the names of the input signals
u = BPTT.tools.interplowest(x_in);
%u = x_in;

% define the reference datetime which is equivalent to sim time zero
% here the reftimee must be defined in MetaData (could be changed to
% process start time)
reftime = P.reftime;

% convert units of inputs from days to hours since reftime
for i=1:length(u_names)
    u.(u_names{i}).Time = (u.(u_names{i}).Time - reftime) .* 24;
end
% defining timerinterval td
td = u.(u_names{1}).Time;
% get the output names and state names from the model structure
output_names = fieldnames(model.y);
state_names = fieldnames(model.x);

% important for calculator initialization
% initialize output variable for model outputs (y)
    y.sim_states = BPTT.TimeVariable;
    y.sim_states .Name = ['sim_states'];
    y.sim_states .TimeInfo.Units = 'days';
    y.sim_states .DataInfo.Units = '';
    
for i=1:length(output_names)
    on = ['sim_y_' output_names{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    %y.(on).DataInfo.Unit = yi.(output_names{i}).Unit;
    y.(on).TimeInfo.Unit = 'days';
end

% initialize output variable for model states (x)
for i=1:length(state_names)
    on = ['sim_x_' state_names{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    %y.(on).DataInfo.Unit = xi.(state_names{i}).Unit;
    y.(on).TimeInfo.Unit = 'days';
end
% initialize output variable for predicted inputs
for i=1:length(u_names)
    on = ['predicted_' u_names{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    y.(on).DataInfo.Unit = 'l/h';
end
if length(td) > 0 % perform calculation only if data is available
    
    if P.Prediction > 0 % for prediction tasks
        for i=1:length(u_names)% extend able to multiple inputs
            data=[];    % resets data vector
            td = u.(u_names{i}).Time;   % define the time vector of input data
            td_old = td;    % hold original timevector
            
            % create indicated timeframe for prediction using 0.2h steps
            % (could be parametrized -> 0.2h enought)
            prediction_period = (u.(u_names{i}).Time(end)+ 0.2: 0.2:(u.(u_names{i}).Time(end)+P.Prediction))';
            td = [u.(u_names{i}).Time; prediction_period]; % merge timevector with prediction vector
            m=length(prediction_period);    % determine length of prediction matrix
           
            input = (u_names{i}); % stores string to check existancy
            % If optimized feed profile is avalable 'p.u_opt', model predicts using the optimized feed profile
            if isfield(P,'u_opt') && isfield(P.u_opt,input);
                td_opt= P.u_opt.(u_names{i}).Time; % timeframe of optimized profile
                data_opt = P.u_opt.(u_names{i}).Data; % datavector of predefined profile
                % search realtime datapoint and align process values with
                % predefined profile
                data_opt = interp1(td_opt,data_opt,td,'linear','extrap'); %interpolate profile to td
                diff= abs(td - td_old(end));    
                [~,startindex] = min(diff);     % serch index of last realtime input
                data = data_opt(startindex+1:end,1); % defined predicted data
                u.(u_names{i}) = (u.(u_names{i}).addsample('Time',prediction_period,'Data',data)); %add Time/Data to model input
                disp(['Prediction using optimized ' u_names{i}])
                
            else % takes the last output as constant value
                data(1:m) = u.(u_names{i}).Data(end);
                data= data';
                u.(u_names{i}) = (u.(u_names{i}).addsample('Time',prediction_period,'Data',data)); %add Time/Data to model input
                disp(['Prediction using constant ' u_names{i}])
            end
        end
    end
    
    % calculate the states and outputs
    
    if isfield(P,'sim_Start') || P.sim_start == 0  % gives the possibility to start simulation at an indicated timepoint p.sim_Start
        if P.sim_Start < 1000        
            sim_Start = P.sim_Start; % in case sim start is indicated as days since reftime -> days since reftime
        else
            sim_Start= (P.sim_Start- P.reftime)*24; % the case where sim start is a date -> days since reftime
        end
        for i=1:length(u_names)
            del_idx_min = find(u.(u_names{i}).Time < (sim_Start)); % delete inputs before sim_Start
            u.(u_names{i}) = u.(u_names{i}).delsample('Index',del_idx_min);
        end
        td = u.(u_names{1}).Time; % redefine td for cut input
        tic
        [t_out,yhat,xhat] = BPTT.tools.model_sim_conf(u ,obj.persist.x_init,td,model,1);
        disp(['Model: ' model.Name])
        toc
    else
        sim_Start = 0;
        tic
        [t_out,yhat,xhat] = BPTT.tools.model_sim_conf(u ,obj.persist.x_init,td,model,1);
        disp(['Model: ' model.Name])
        toc
    end
    % write the calculated outputs in the output time variables
    for i=1:length(output_names)
        on = ['sim_y_' output_names{i}];
        
        % adjust frequency of output data if specified
        yfreq = size(yhat,2) / t_out(end); % points per hour
        if ~isempty(P.output_freq)
            downsample = round(yfreq/P.output_freq(i));
        else
            downsample=1;
        end
        
        if downsample==0,downsample=1;end
        new_time = t_out(1:downsample:end);
        new_data = yhat(i,1:downsample:end)';
        y.(on).Data = new_data + new_data .* randn(length(new_data),1) .* P.output_error(i);
        y.(on).Time =  new_time./24+P.reftime;
        
    end
    
    % write the calculated states in the output time variables

    y.sim_states .Data = xhat(:,:);
    y.sim_states .Time = t_out./24+P.reftime;
   
    for i=1:length(state_names)
        on = ['sim_x_' state_names{i}];
        y.(on).Data = xhat(:,i);
        y.(on).Time =  t_out./24+P.reftime;
    end
    % update the value of the initial state for the next round
    % in cas of prediction store the last realtime value
    if exist('prediction_period','var')
        %try
            val = min(abs(td(:)-td_old(end)));
            ind = find(val==abs(td(:)-td_old(end)));
            try
            obj.persist.x_init  = xhat(ind,:);
            catch
            obj.persist.x_init  = xhat(end,:);
            end
            %obj.persist.x_init  = xhat(end-length(prediction_period),:);
        %s1= xhat(end-length(prediction_period),:)
       % catch
        %warning('calc model sim config (line 151) storage of xinit - prediciton failed')     
       % obj.persist.x_init  = xhat(1,:); % take first estimate for propagation (okay for frequent calculation periods)
        %s2= xhat(1,:)
        %end
    else
    obj.persist.x_init  = xhat(end,:);
    %s3= xhat(end,:)
    end
    
    % Aditionally predicted feed profiles as output
    for i=1:length(u_names)
        on = ['predicted_' u_names{i}];
        y.(on) = BPTT.TimeVariable;
        y.(on).Data = u.(u_names{i}).Data;
        y.(on).Time = u.(u_names{i}).Time  ./ 24 + P.reftime; % no adition of sim_start becaus evriable are cut
        y.(on).Name = u_names{i};
        y.(on).DataInfo.Unit = 'l/h';
        %y.(output.Name) = output;
    end
end
end