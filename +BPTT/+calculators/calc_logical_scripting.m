function y = calc_logical_scripting( x_in , obj)
    % A generic calculator for performing mathematical operations on TimeVariables
    % (c9 Julian Kager April 2015
    
    P = obj.p; % get the parameter structure

    x_names = fieldnames(x_in);
    TS = BPTT.tools.interplowest(x_in);
    
    for i=1:length(x_names)
        x{i} = TS.(x_names{i}).Data(end);
        t{i} = TS.(x_names{i}).Time(end);
    end
    
    %%

    y.(P.output_name) = BPTT.TimeVariable;
    y.(P.output_name).Name = P.output_name;

    if ~isfield(obj.o.Variables, P.output_name)
    y.(P.output_name).Data = P.Startpoint ;
    y.(P.output_name).Time = now;   
    obj.o.AddVariable( y.(P.output_name));
    end    
    
    if TS.(x_names{1}).Length > 0
%% place here logical script 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    if x{1} <= 30
logic_result = obj.o.Variables.(P.output_name).Data(end) + 0.05; 
    elseif x{1} > 60 && obj.o.Variables.(P.output_name).Data(end) >= 0
logic_result = obj.o.Variables.(P.output_name).Data(end) - 0.05;
    elseif obj.o.Variables.(P.output_name).Data(end) < 0;
logic_result = 0;
    else        
logic_result = obj.o.Variables.(P.output_name).Data(end);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
        y.(P.output_name).Data = logic_result;
        y.(P.output_name).Time = TS.(x_names{1}).Time(end);
    end
    
    y.(P.output_name).Quality = TS.(x_names{1}).Quality;
    y.(P.output_name).TimeInfo.Unit = TS.(x_names{i}).TimeInfo.Units;
    y.(P.output_name).DataInfo.Unit = P.output_units;
    
    
 end