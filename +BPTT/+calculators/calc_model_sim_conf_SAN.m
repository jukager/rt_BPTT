function [y]  = calc_model_sim_conf( x_in , obj)
% Simulation of a model: returns the model outputs (y) and states (x) given the
% inputs (u). initial states (P.xinit) and model inputs u (available within bioprocess named as model input) must be given.
% P.Prediction can be indicated to look into future by using last input
% as constant or predefined optimal feed profile. It is capable of working in online and offline modes.
% model outputs are avalable as timevariabels within the bioprocess
% model with structure format are supported
% (c) Julian Kager, April 2016

P = obj.p;                          % read the calculator parameters from the object
model = P.Model;                    % read the model structure passed as parameter

% needs a persistent variable for initial states
if ~isfield(obj.persist,'x_init')
    obj.persist.x_init = P.xinit;
end

u_names = fieldnames(x_in);          % get the names of the input signals
u = x_in;

% define the reference datetime which is equivalent to sim time zero
% here the reftimee must be defined in MetaData (could be changed to
% process start time)
reftime = P.reftime;

% convert units of inputs from days to hours
for i=1:length(u_names)
    u.(u_names{i}).Time = (u.(u_names{i}).Time - reftime) .* 24;
end
td = u.(u_names{1}).Time;
% get the output names from the model structure
output_names = fieldnames(model.y);

% get the state names from the model structure
state_names = fieldnames(model.x);

% initialize output variable for model outputs (y)
for i=1:length(output_names)
    on = ['sim_y_' output_names{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    %y.(on).DataInfo.Unit = yi.(output_names{i}).Unit;
    y.(on).TimeInfo.Unit = 'days';
end

% initialize output variable for model states (x)
for i=1:length(state_names)
    on = ['sim_x_' state_names{i}];
    y.(on) = BPTT.TimeVariable;
    y.(on).Name = on;
    %y.(on).DataInfo.Unit = xi.(state_names{i}).Unit;
    y.(on).TimeInfo.Unit = 'days';
end

    % initialize output variable for predicted inputs
    for i=1:length(u_names)
        on = ['predicted_' u_names{i}];
        y.(on) = BPTT.TimeVariable;
        y.(on).Name = on;
        y.(on).DataInfo.Unit = 'l/h';
    end
    % for prediction tasks
if td > 0 % perform calculation only if data is available    
    
    if P.Prediction > 0
        %u_pred = struct;
        for i=1:length(u_names)% extend able to multiple inputs
            data=[]; %resets data vector
            % define the time vector of input data
            td = u.(u_names{i}).Time;
            td_old = td;
            % create indicated timeframe for prediction using 0.2h steps
            prediction_period = (u.(u_names{i}).Time(end)+ 0.2: 0.2:(u.(u_names{i}).Time(end)+P.Prediction))';
            % add timframe to avalaible data
            td = [u.(u_names{i}).Time; prediction_period];
            % determine length of prediction matrix
            m=length(prediction_period);
            %for i=1:length(u_names) enables loop trought multiple profiles
            % If optimized feed profile is avalable 'p.u_opt', model predicts using the optimized feed profile
            if isfield(P,'u_opt')
                disp(['Prediction using optimized ' u_names{i}])
                % find startpoint of prediction from predefined feed profile
                %startpoint = obj.o.Variables.(u_names{1}).Time(1,1)
                % timeframe of optimized profile
                td_opt= P.u_opt.(u_names{i}).Time; % startpoint;
                % dataframe of optimized
                data_opt = P.u_opt.(u_names{i}).Data;
                data_opt = interp1(td_opt,data_opt,td,'linear','extrap');
                diff= abs(td - td_old(end));
                [~,startindex] = min(diff);
                data = data_opt(startindex+1:end,1);
                u.(u_names{i}) = (u.(u_names{i}).addsample('Time',prediction_period,'Data',data));
                
            else % takes the last output as constant value
                
                %for i=1:length(u_names)
                data(1:m) = u.(u_names{i}).Data(end);
                data= data';
                u.(u_names{i}) = (u.(u_names{i}).addsample('Time',prediction_period,'Data',data));
                %end
            end
        end
    end
    
    % calculate the states and outputs
    
    if isfield(P,'sim_Start') % gives the possibility to start simulation at an indicated timepoint p.sim_Start
        for i=1:length(u_names)
            P.sim_Start= P.sim_Start- reftime*24;
            del_idx_min = find(u.(u_names{i}).Time < P.sim_Start);
            u.(u_names{i}) = u.(u_names{i}).delsample('Index',del_idx_min);
        end
        tic
        [t_out,yhat,xhat] = BPTT.tools.model_sim_conf(u ,obj.persist.x_init,td,model,1);
        disp(['Model: ' model.Name])
        toc
    else
        tic
        [t_out,yhat,xhat] = BPTT.tools.model_sim_conf(u ,obj.persist.x_init,td,model,1);
        disp(['Model: ' model.Name])
        toc
    end
    % write the calculated outputs in the output time variables
    for i=1:length(output_names)
        on = ['sim_y_' output_names{i}];
        
        % adjust frequency of output data if specified
        yfreq = size(yhat,2) / t_out(end); % points per hour
        if ~isempty(P.output_freq)
            downsample = round(yfreq/P.output_freq(i));
        else
            downsample=1;
        end
        
        if downsample==0,downsample=1;end
        new_time = t_out(1:downsample:end);
        new_data = yhat(i,1:downsample:end)';
        y.(on).Data = new_data + new_data .* randn(length(new_data),1) .* P.output_error(i);
        y.(on).Time =  new_time./24+P.reftime+P.sim_Start/24;
        
    end
    
    % write the calculated states in the output time variables
    for i=1:length(state_names)
        on = ['sim_x_' state_names{i}];
        y.(on).Data = xhat(:,i);
        y.(on).Time =  t_out./24+P.reftime+P.sim_Start/24;
    end
    % update the value of the initial state for the next round
    obj.persist.x_init  = xhat(end,:);
    
    % Aditionally predicted feed profiles as output
    for i=1:length(u_names)
        on = ['predicted_' u_names{i}];
        y.(on) = BPTT.TimeVariable;
        y.(on).Data = u.(u_names{i}).Data;
        y.(on).Time = u.(u_names{i}).Time  ./ 24 + reftime;
        y.(on).Name = u_names{i};
        y.(on).DataInfo.Unit = 'l/h';
        %y.(output.Name) = output;
    end
end
end