function y = calc_sago_h( x, obj)
    % Calculator for applying the SAGO filter result in [unit/h]
    % based on Calculator BPTT.calculators.calc_sago
    % Peter Ettnger 02.09.2015
    
    P = obj.p; % get the parameter structure
    
    x_names = fieldnames(x);
    balance = x.(x_names{1});
    
    % remove any non-monotically increasing points from input signal
    idx = find(diff(balance.Time)==0);
    balance = delsample(balance,'Index',idx);
    
    window = P.SG_window;
    if mod(window,2)==0
        window = window + 1;
    end
    
    t_out = [];
    y_out = [];
    
    if balance.Length>=window % perform calculations only if there are enough data points available
    
        %% taking the derivative
        
        % check whether window size is larger than SG polynomial order
        if window > P.SG_pol_order
            weight = ones(balance.Length, 1);
           [t_out,y_out] = BPTT.tools.sago(balance.Time, balance.Data,P.SG_pol_order,P.DN, window,weight,0);
        end
    
    end
    
    %% Defining the output structure
    
    output = BPTT.TimeVariable;
    output.Name = P.output_name;
    
    output.Data = y_out ./24; % converting from [unit/days] to [unit/hours] 
    output.Time = t_out;

    if sum(strcmp(fieldnames(P),'output_units'))==1
        output.DataInfo.Units = P.output_units;
    else
        output.DataInfo.Units = '/h';
    end
    output.TimeInfo.Unit = balance.TimeInfo.Units;
    
    if ~isempty(balance.Quality)
        output.Quality = interp1(balance.Time,balance.Quality,output.Time,'nearest');
    end
    y.(P.output_name) = output;
    
 end

