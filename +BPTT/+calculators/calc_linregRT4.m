function y = calc_linregRT4( x_in , obj)
    % Calculator for obtaining the regression coefficients
    % (c) Ines Stelzer & Julian Kager 23.02.2016
    
    P = obj.p;                          % parameter structure

    % definition of a persistent variable for model parameters
    
    
    
    x_names = fieldnames(x_in);         % assign x-names to inputs 
    TS = BPTT.tools.interplowest(x_in); % interpolation of inputs
    
    for i=1:length(x_names)            
        x{i} = TS.(x_names{i}).Data;    % getting data vector from inputs
        t{i} = TS.(x_names{i}).Time;    % getting time vector from inputs
    end
%%  
    y.(P.output_name) = BPTT.TimeVariable;
    y.(P.output_name).Name = P.output_name;
    
   if TS.(x_names{1}).Length < 2;
        FIT = P.regkoeff;
         obj.persist.slope(1,1) = FIT(1);
         obj.persist.offset(1,1) = FIT(2);
         obj.persist.timestamp(1,1) = 0;
         
   end
   if TS.(x_names{1}).Length >= 2;      % assuming that 
        FIT = polyfit(x{1}, x{2},1);
    
    if TS.(x_names{2}).Length > obj.p.forgetting       % assuming that 
        FIT = polyfit(x{1}(end-obj.p.forgetting:end,1), x{2}(end-obj.p.forgetting:end,1),1);
    end
    obj.persist.slope(end+1,1) = FIT(1);
    obj.persist.timestamp(end+1,1) = t{i}(end,1);
    obj.persist.offset(end+1,1) = FIT(2);
   end
end