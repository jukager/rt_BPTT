classdef Incyght_DB < BPTT.database.REST_DB
    %INCYGHT_DB Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        update_interval = 60; 
        update_calculators = false;      % property if calculators should be updated
    end
    
    properties (Constant)
        DATETIME_DB_US = 'yyyy-MM-dd''T''HH:mm:ss.SSSSSSXXX'  % matlab json timestamp format
        DATETIME_DB = 'yyyy-MM-dd''T''HH:mm:ssXXX'  % matlab json timestamp format
    end
    
    properties (Access = private)
        LocalTIMEZONE = datenum(datetime('now','TimeZone','utc','Format','Z'))-datenum(datetime('now','TimeZone','local','Format','Z')) %set timezone shift in hours
        realtime_batches = struct       % Holds all Bioprocesses selected by user
        timer_tag = 'update_realtime_data'  % tag to identify the realtime-timer
        DATETIME_EXACT = 'yyyy-MM-dd HH:mm:ss.SSS' % matlab datetime format str up to microseconds
    end
    
    methods
        function obj = Incyght_DB(varargin)
            obj@BPTT.database.REST_DB(varargin);
            
%             if isempty(strfind(obj.api_url, 'api/'))
%                 obj.api_url = [obj.api_url 'api/'];
%             end
               
            % try to log in with JWT
            if ~obj.check_authentication();
                parts = strsplit(obj.api_url, '/');
                errordlg(['Username or password was wrong, please try again or register at ' parts{2}], 'Authentication error');
                throw(MException('InCyght_DB:auth', ...
                    ['Username or password was wrong, please try again or register at ' parts{2}]));
            end
        end
        
        % ========== Functions for Session =============
                
        function response = save_experiment_collection(obj, ec_struct)

            response = obj.post_to_db(ec_struct, 'api/sessions/');
            
        end
        
        function response = load_sessions_data(obj)
            response = obj.get_from_db('api/sessions/', 'paginate', true);
        end
        
        function response = load_session(obj, session_id)
            response = obj.get_from_db(['api/sessions/' num2str(session_id) '/']);
        end
        
        
        % ========== Functions for Batch ====================
        
        function response = save_bioprocess(obj, bp_struct)
            response = obj.post_to_db(bp_struct, 'api/batches/');
        end
        
        function response = save_bioprocess_data(obj, data)
            response = obj.post_to_db(data, 'api/variables/bulk_update/');
        end
        
        function response = load_bioprocess_data(obj, batch_id)
            response = obj.get_from_db(['api/batches/' batch_id '/variable_data/']);
        end
        
        
        % ======= Function for variables ================
        
        function response = save_variable(obj, tv_struct)
            response = obj.post_to_db(tv_struct, 'api/variables/');
        end
        
        function [values, timestamps, quality] = load_variable(obj, variable, bioprocess)
            values = [];
            timestamps = [];
            quality = [];
            
            response = obj.get_from_db(['api/variables/' num2str(variable.db_id) '/']);
            
            values = cell2mat(response.data{1});
            timestamps = cell2mat(response.time);
            quality = cell2mat(response.quality);            
        end
        
        
        % ========= Functions for processes =================
       
        function response = list_processes(obj)
            response = obj.get_from_db('api/process/');
        end
        
        function response = load_process(obj, id)
            % returns 
            response = obj.get_from_db(['api/process/' num2str(id) '/']);
        end
        
        function response = save_process(obj, process)
            response = obj.post_to_db(process, 'api/process/');
        end
        
        function response = save_processes(obj, processes)
            response = obj.post_to_db(processes, 'api/process/bulk_update/');
        end
        
        function response = delete_process(obj, id)
            response = obj.post_to_db([], ['api/process/' num2str(id) '/'], 'method', 'DELETE');
        end
        
        
        % ========== FUnctions for realtime-data ================
        
        function obj = clear_bioprocesses(obj)
            obj.stop();
            obj.realtime_batches = [];
        end
        
        function response = get_active_batches(obj, varargin)
            url = 'active=1';
            if ~isempty(varargin)
                url = [url '&' varargin{1}];
            end
            response = obj.get_all_batches(url);
        end
        
        function response = get_all_batches(obj, varargin)
            url = 'api/realtime_batches/';
            if ~isempty(varargin)
                url = [url '?' varargin{1}];
            end
            response = obj.get_from_db(url);
            if response.isGood
                response = response.content;
            else
                response = [];
            end
        end
        
        function bp = create_bioprocess(obj, varargin)
            bp = [];
            if isa(varargin{1}, 'char')
                if strcmp(varargin{1}, 'reactor') || strcmp(varargin{1}, 'reactor_name')
                    condition = ['reactor_name=' varargin{2}];
                elseif strcmp(varargin{1}, 'batch') || strcmp(varargin{1}, 'batch_name')
                    condition = ['batch_name=' varargin{2}];
                else
                    condition = [];
                end
                ret = obj.get_active_batches(condition);
                if ~isempty(ret)
                    bp = obj.create_bioprocess_from_rtb(ret(1), varargin{:});
                end
            else
                bp = obj.create_bioprocess_from_rtb(varargin{1}, varargin{:});
            end
            
        end
        
        % Fetches all variables for given realtime-batch from REST-Server 
        % and saves references in private variable bioprocesses for later
        % updates
        function bp = create_bioprocess_from_rtb(obj, rtb, varargin)
            param_idx = find(strcmp(varargin, 'parameter'));
            if ~isempty(param_idx)
                if isa(varargin{param_idx + 1}, 'char')
                    parameter_names = strtrim(strsplit(varargin{param_idx + 1},','));
                else
                    parameter_names = varargin{param_idx + 1};
                end
            else
                parameter_names = [];
            end
            
            start_datenum = datenum(datetime(rtb.creation_time, 'InputFormat', obj.DATETIME_DB_US, 'TimeZone', 'local'));
            bp = BPTT.Bioprocess;
            bp.Process_Start_Time = start_datenum;
            bp.Name = BPTT.tools.rm_special_characters(rtb.reactor_name);
            bp.Comment = { rtb.name; ['Start: ' datestr(start_datenum)]};
            bp.db_id = rtb.id;
            
            obj.realtime_batches.(bp.Name).object = bp;
            obj.realtime_batches.(bp.Name).last_update = 0;
            obj.realtime_batches.(bp.Name).parameter_names = parameter_names;
    
            if obj.debug
                disp(['Getting initial data for ' bp.Name ' (this might take some time...)']);
            end
            obj.update_rest_data(bp);

        end
        
        function data_received = update_rest_data(obj, bp, varargin)
            param_idx = find(strcmp(varargin, 'parameter'));
            if ~isempty(param_idx)
                if isa(varargin{param_idx + 1}, 'char')
                    parameter_names = strtrim(strsplit(varargin{param_idx + 1},','));
                else
                    parameter_names = varargin{param_idx + 1};
                end
            else
                parameter_names = [];
            end
            
            if ~isempty(parameter_names)
                obj.realtime_batches.(bp.Name).parameter_names = parameter_names;
            else
                parameter_names = obj.realtime_batches.(bp.Name).parameter_names;
            end
            
            data_received = false;
            
            % create url to fetch 
            url = ['api/realtime_variables/?query_mode=db&batch_id=' int2str(bp.db_id)];
            
            % add last update timestamp to only fetch new data from server
            if obj.realtime_batches.(bp.Name).last_update > 0
                % get newest timestamp and add 1e-8 to not always get the
                % lastest series again
                iso_datestr = datestr(obj.realtime_batches.(bp.Name).last_update + obj.LocalTIMEZONE + 1e-8, 'yyyymmddTHHMMSS.FFF');
                url = [ url '&created__gt=' iso_datestr];
            end
                
            % add pre-selected parameter names
            if ~isempty(parameter_names)
                url = [ url '&parameter_names=' strjoin(parameter_names,',') ];
            end
            
            if obj.debug
                disp(['API Request: ' url]);
            end
            
            response = obj.get_from_db(url);
            
            if ~response.isGood
                warning(['Could not load data: ' response.content]);
                return
            end
            
            if isempty(response.content)
                if obj.debug
                    disp(['Got no new data for ' bp.Name]);
                end
                return
            end
            
            dataIn = response.content;
            if obj.debug
                disp([ num2str(numel(dataIn)) ' new datapoints received for ' bp.Name]);
            end
            data_received = true;
            
            
            for i=1:numel(dataIn)
                tmp_name = BPTT.tools.rm_special_characters(dataIn(i).name);
                
                ts_data = dataIn(i).data;
                
                % If first time, create time series. Add new data to
                % existing timeseries.
                if ~isfield(bp.Variables, tmp_name)
                    % If timeseries does not exist, create new one
                    TS = BPTT.TimeVariable;
                    % Set timeseries name and Id
                    TS.Name = tmp_name;
                    if ~isnan(dataIn(i).unit)
                        TS.DataInfo.Units = dataIn(i).unit;
                    end
                    TS.TimeInfo.Units = 'days';
                else
                    TS = bp.Variables.(tmp_name);
                end
                
                try
%                     new_time = datenum(datetime({ts_data.created}, ...
%                             'InputFormat', obj.DATETIME_DB, ...
%                             'TimeZone', 'local'))';
                    new_time = datenum(datetime([dataIn(i).timestamps{:}]',...
                            'ConvertFrom', 'posixtime', ...
                            'TimeZone', 'local'));
                    new_data = cellfun(@double, ts_data);
                    % in case of update
                    if obj.realtime_batches.(bp.Name).last_update > 0 && ...
                            ~isempty(TS.Time)
                        % fix for not adding duplicate timeseries
                        new_data = [TS.Data; new_data(new_time > TS.Time(end))]; 
                        new_time = [TS.Time; new_time(new_time > TS.Time(end))];
                    end
                    
                    if isempty(TS.Quality)
                        TS.set('Time', new_time, 'Data', new_data);
                    else
                        % find last quality which is not -1 and apply it to
                        % new data as quality
                        last_idx = find(TS.Quality ~= -1);
                        last_idx = last_idx(end);
                        new_quality = numel(new_time) - numel(TS.Quality);
                        new_quality = [TS.Quality; ones(new_quality,1)*TS.Quality(last_idx)];
                        TS.set('Time', new_time, 'Data', new_data, 'Quality', new_quality);
                    end
                    
                    bp.AddVariable(TS);
                catch err
                    BPTT.tools.catch_error(err);
                end 
            end
            
            % update last_update for bioprocess
            % check if this timeseries contains newest timestamp
            % and save this timestamp as last_timestamp
            if numel(new_time) > 1 && ...
                    new_time(end) > obj.realtime_batches.(bp.Name).last_update
                obj.realtime_batches.(bp.Name).last_update = new_time(end);
            end
        end
        
        function status = write_setpoints(obj, bp, setpoints)
            status = obj.write_data(bp, setpoints, 'api/realtime_variables/write_setpoint/');
        end
        
        function status = write_data(obj, bp, values, varargin)
            if ~isempty(varargin)
                url = varargin{1};
            else
                url = 'api/realtime_variables/';
            end
            
            if isa(bp, 'BPTT.Bioprocess')
                data.batch = bp.db_id;
            else
                data.batch = bp;
            end
            
            status = [];
            if isa(values, 'BPTT.TimeVariable')
                for i=1:numel(values)
                    data.name = values(i).Name;
                    data.value = values(i).Data(end);
                    response = obj.post_to_db(data, url);
                    status = [status response];
                end     
            else
                for i=1:numel(values)
                    data.name = values(i).name;
                    data.value = values(i).value;
                    response = obj.post_to_db(data, url);
                    status = [status response];
                end
            end
        end
        
        
        % ================= TIMER functions ====================
        
        function obj = start(obj)
            % Start timer
            delete(timerfind('Tag', obj.timer_tag));
            
            % start update once
%             obj.update_all_data();
            
            t = timer;
            t.TimerFcn = @(~, ~)obj.update_all_data();
            if obj.update_interval > 0
                if obj.update_interval < 0.1
                    obj.update_interval = 0.1; % minimum update interval
                end
                t.StartDelay = obj.update_interval;
                t.Period = obj.update_interval;
                t.ExecutionMode = 'fixedSpacing';
                t.Tag = obj.timer_tag;
                start(t);
            end
        end
        
        function any_new_data = update_all_data(obj)
            any_new_data = false;
            % Request the data
            bp_names = fieldnames(obj.realtime_batches);
            
%             tic
            for i=1:numel(bp_names)
                % check if handle still valid, otherwise remove bioprocess
                if ~isvalid(obj.realtime_batches.(bp_names{i}).object)
                    obj.realtime_batches = rmfield(obj.realtime_batches, bp_names{i});
                    continue;
                end
                bioprocess = obj.realtime_batches.(bp_names{i}).object;
                
                % fetch new data
                new_data = false;
                try
                    new_data = obj.update_data(bioprocess);
                catch err
                    obj.useful_error(err);
                end
                
                % run calculators, if new data received
                if new_data
                    any_new_data = true;
                    
                    if obj.update_calculators
                        try
                            obj.run_calculators(bioprocess);
                        catch err
                            obj.useful_error(err);
                        end
                    end
                end
            end
%             toc

            % check if no valid realtime_batches are left, then stop timer
            if isempty(fieldnames(obj.realtime_batches))
                obj.stop();
            end
        end

        function new_data = update_data(obj, bp)
            new_data = false;
            
            if ~isempty(obj.api_url) && bp.db_id ~= 0
                new_data = obj.update_rest_data(bp);
            end
            
            if new_data && isfield(bp.Variables, 'ProcessTime')
                bp.AddVariable(bp.Variables.ProcessTime);
            end
        end
      
        
        function obj = run_calculators(obj, bp)
            % Check if a experiment exists
            if isempty(bp) || ~isvalid(bp)
                warning('No Bioprocess given');
                return;
            end
            
            % rerun all calculators, set mode to online (true) (EXPUTEC)
            % bp.rerunAllCalculators(true);
            % calculateAll executes all calculators (VTU)
            bp.calculateAll
        end

        
        function stop(obj)
            t = timerfind('Tag', obj.timer_tag );
            if ~isempty(t)
                stop(t);
                delete(t);
            end
            if obj.debug
                disp('Data request loop stopped. Finishing already running calculations if any.');
            end
        end
        
        function useful_error(~, err)
            warn_settings = warning;
            if strcmp(warn_settings(1).state, 'off')
                warning('on');
                warn_was_off = 1;
            else
                warn_was_off = 0;
            end

            namestack = err.stack.name;
            linestack = err.stack.line;
            warning on;
            warning([namestack ' in line ' num2str(linestack) ': ' err.message]);

            if warn_was_off
                waring('off');
            end
        end
    end
    
    % ==== general static class functions ============
    methods(Static)
      function status = status_field_all(handles, field, value)
         status = true;
         
         realtime_objects = {'realtime', 'realtime_opc', 'realtime_edb'};
         for i=1:numel(realtime_objects)
            if isfield(handles, realtime_objects{i})
                status = handles.(realtime_objects{i}).(field);
                
                if exist('value', 'var')
                    handles.(realtime_objects{i}).(field) = value;
                    status = value;
                end
            end
         end
      end
        
    end
    
end

