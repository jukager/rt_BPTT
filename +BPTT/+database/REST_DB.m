classdef REST_DB < handle
    %REST_DB Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        api_url = ''       % URL to access DB
        header = []         % header to perform requests to webserver
        token = []
        debug = false;      % show debug information
        last_refresh = 0;   % last request on server (needed to refresh token)
        token_refresh = 600;  % refresh each 5 minutes
        
        username; 
        password;
    end
    
    methods
        
        function obj = REST_DB(varargin)
            % Initialisation function
            global global_cfg;
            
            if isfield(global_cfg, 'debug_mode') && global_cfg.debug_mode
                obj.debug = true;
            end
            
            input = varargin{1};
            if numel(input) > 0 && ~isempty(input{1})
                obj.api_url = input{1};
            elseif isfield(global_cfg, 'database_url')
                obj.api_url = global_cfg.database_url;
            else
                throw(MException('REST_DB:url', ...
                    ['You need to specify an URL for the database']));
            end
            
            if numel(input) > 2
                obj.username = input{2};
                obj.password = input{3};
            end
            
            if ~strcmp(obj.api_url(end),'/')
                obj.api_url = [obj.api_url '/'];
            end

            obj.header = obj.default_header();
        end
        
        function header = default_header(obj)
            header = http_createHeader('Content-Type','incyght-win/zlib_data');
            header(end+1) = http_createHeader('Accept','incyght-win/zlib_data');
            if ~isempty(obj.token)
                header(end+1) = http_createHeader('Authorization',['JWT ' obj.token]);
            end
        end
            
        function obj = set_basic_authentication(obj, username, password)
            encoder  = sun.misc.BASE64Encoder();
            str      = java.lang.String([username ':' password]); 

            obj.header = obj.default_header();
            obj.header(end+1) = http_createHeader('Authorization',['Basic ' char(encoder.encode(str.getBytes()))]);
        end

        function ret = is_authenticated(obj)
            ret = ~isempty(obj.token);
        end
        
        function status = refresh_token(obj)
            status = false;
            data = struct('token', obj.token);
            response = obj.post_to_db(data, 'api/token_refresh/', 'skip_token_check');
            if response.isGood
                obj.token = response.content.token;
                
                try
                    handles=guidata(findall(0,'Tag','main_gui'));
                    handles.session_data.jwt = obj.token;
                    guidata(handles.main_gui, handles);
                catch
                end
                
                obj.header = obj.default_header();
                status = true;
            end 
            obj.last_refresh = now;
        end
        
        function status = verify_token(obj)
            status = false;
            data = struct('token', obj.token);
            response = obj.post_to_db(data, 'api/token_verify/', 'skip_token_check');
            if response.isGood
                obj.header = obj.default_header();
                status = true;
            end 
            obj.last_refresh = now;
        end
        
        function auth = check_authentication(obj)
            global global_cfg;
            try
                handles=guidata(findall(0,'Tag','main_gui'));
            catch
                handles = {};
            end
            
            auth = false;
            
            % check if saved token is still valid
            if isfield(handles, 'session_data') && isfield(handles.session_data, 'jwt')
                obj.token = handles.session_data.jwt;
                auth = obj.verify_token();
            end
            
            % if token is not valid anymore, try to login again
            if ~auth
                % get user from config or query user
                if isfield(global_cfg, 'database_user') && isempty(obj.username)
                    obj.username = global_cfg.database_user;
                    obj.password = global_cfg.database_password;
                elseif isempty(obj.username)
                    % displays popup for configuring database username and password
                    [obj.password, obj.username] = passwordEntryDialog('enterUserName',true,...
                        'CheckPasswordLength',false, 'WindowName', 'REST-Login');
                    if obj.password == -1
                        return;
                    end
                end
                
                % get token from server
                data = struct('username', obj.username, 'password', obj.password);
                response = obj.post_to_db(data, 'api/token_auth/');
                if response.isGood
                    obj.token = response.content.token;
                    % save token to session
                    try
                        handles.session_data.jwt = obj.token;
                        guidata(handles.main_gui, handles);
                    catch
                    end
                    % authentication successfull
                    auth = true;
                    obj.header = obj.default_header();
                end
                
                obj.password = '';
            end
        end
       
        
        % ------- HTTP Methods for REST-API -------------
        
        function response = get_from_db(obj, url, varargin)
            if (now-obj.last_refresh)*24*3600 > obj.token_refresh && ...
                    isempty(find(strcmp(varargin,'skip_token_check'), 1))
                obj.refresh_token();
            end
            % get method parameter if it exists
            try
                paginate = varargin{find(strcmpi('paginate', varargin))+1};
            catch
                paginate = false;
            end
                
            if obj.debug
                disp([obj.api_url url]);
            end
            [content, extras] = urlread2([obj.api_url url], 'GET', '', obj.header);
            
            if extras.isGood && extras.status.value == 200 && ~isempty(content)
                content = obj.decompress(uint8(content));
                if isfield(content, 'results') && ~paginate
                    content = content.results;
                end
                
                if numel(content) > 0 && isa(content, 'cell')
                    response.content = cell2mat(content);
                else
                    response.content = content;
                end
            else
                response.content = [];
            end
            
            response.status = extras.status;
            response.isGood = extras.isGood;
        end
        
        function response = post_to_db(obj, data, url, varargin)
            if (now-obj.last_refresh)*24*3600 > obj.token_refresh && ...
                    isempty(find(strcmp(varargin,'skip_token_check'), 1))
                obj.refresh_token();
            end
            
            method = 'POST';
            if numel(data) == 1 && isfield(data, 'id')
                if isa(data.id, 'char')
                    data.id = str2double(data.id);
                end
                if data.id > 0
                    url = [url num2str(data.id) '/'];
                    method = 'PUT';
                end
            end
            
            % get method parameter if it exists
            try
                method = varargin{find(strcmpi('method', varargin))+1};
            catch
            end
            
            if strcmp(method, 'DELETE')
                post_data = [];
            else
                post_data = obj.compress(data);
            end
            
            [content, status] = urlread2([obj.api_url url], method, post_data, obj.header);
            
            if status.isGood
                if ~isempty(content)
                    content = obj.decompress(uint8(content));
                end
            end
            
            response.content = content;
            response.isGood = status.isGood;
            response.status = status.status;
        end
        
        function data = compress(~, data)
            data = BPTT.tools.m2json(data);
            data = zlibencode(data);
        end
        
        function data = decompress(~, data)
            data = zlibdecode(data);
            data = char(data);
            data = BPTT.tools.json2m(data);
        end

        
        
    end
    
end

