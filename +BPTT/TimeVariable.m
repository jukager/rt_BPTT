% TimeVariable is a custom made time series object
% 
% See also timeseries
% 
% Reference page in Help browser 
% <a href="matlab:doc BPTT.TimeVariable;">doc BPTT.TimeVariable</a>
%  
classdef TimeVariable < timeseries
    
    properties
        
        Type='raw'          % {raw} / calculated
        Calculator          % Specify the name of calculator if variable is calculated
        interplowest=1      % Should the TimeVariable count for determination of the common time axis in the BPTT.interplowest function? 1=yes 2=no
        sdev=[]             % Standard deviation (can also be used for error)
        MetaData;           % MetaData for each time variable
        
    end
    
    properties 
        
        ModificationTime = now;
        
    end
    
    properties (SetAccess = private)
    
    CreationTime = now;    
        
        
    end
    
    methods
        
        function obj = TimeVariable()
        % Function for creating a new TimeVariable  
            obj.TimeInfo.Units = 'days';
            obj.CreationTime = now;
            
        end
        
    end
    
end