function [plot_handle] = multiple_rates(BPTT_objects, legend_names, variable_name, varargin)
    if any(~ismember(varargin,'LineStyle'))
        varargin = [varargin, 'LineStyle', '-'];
    end
    plot_handle = BPTT.plots.time_variables(BPTT_objects, legend_names, variable_name, varargin{:});
    hold on
    BPTT.plots.tools.horizontal_line(0,'--');
    hold off
end