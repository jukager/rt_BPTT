function [line_handle, text_handle] = annotated_line(type, value, annotation_text, line_style)
    switch type
        case 'horizontal'
            line_handle = BPTT.plots.tools.horizontal_line(value,line_style);
            x_value = line_handle.XData(2)*0.95;
            y_value = value;
        case 'vertical'
            line_handle = BPTT.plots.tools.vertical_line(value,line_style);
            x_value = value;
            y_value = line_handle.YData(2)*0.95;
    end
    text_handle = text(x_value, y_value, annotation_text, 'HorizontalAlignment','right','VerticalAlignment', 'bottom');
    set(text_handle, 'rotation', 90);
end