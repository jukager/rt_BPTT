function rate_and_concentration(rate, orig_concentration, calc_concentration, varargin)
    % Plots rate and concentration, both measured and calculated. Takes
    % additional name-value-pairs:
    % Name: 'unit', Value: String with output unit, for instance 'g/g/min';
    %   standard output unit is 'g/g/h';
    
    figure;
    yyaxis right
    hold on
    
    rate = convert_rate(rate,varargin);
    
    handle_rate = rate.plot(' -');
    handle_zero_rate = horizontal_line(0);
    hold off
    
    ax = gca;
    xlabel_string = ax.XLabel.String;
    
    yyaxis left;
    handle_orig_conc = orig_concentration.plot('o ');
    hold on
    if size(orig_concentration.Data,2) > 1
        ax = gca;
        ylabel_string = ax.YLabel.String;
        mean_conc = calculate_mean(orig_concentration);
        handle_mean_conc = mean_conc.plot('o ');
        ylabel(ylabel_string);
        set(handle_mean_conc, 'MarkerFaceColor',[0 113/255 188/255]);
        mean_conc_label = 'mean of concentrations';
    else
        mean_conc_label = '';
        handle_mean_conc = [];
    end 
    handle_calc_conc = plot(calc_concentration.Time, calc_concentration.Data,' -');
    hold off
    xlabel(xlabel_string);
    
    legend_label = {'measured concentration', mean_conc_label ,'calc. concentration','calc. specific rate q', 'q=0'};
    legend_label(strcmp(legend_label,'')) = [];
    
    legend( [handle_orig_conc(1), handle_mean_conc, handle_calc_conc, handle_rate, handle_zero_rate], legend_label);
end

function [converted_rate] = convert_rate(rate, value_pairs)
    
    if find(ismember(value_pairs,'unit'))
        unit_index = find(ismember(value_pairs,'unit'));
        rate_index = unit_index +1;
        output_unit = value_pairs{rate_index};
    else
        output_unit = 'g/g/h';
    end

    if ~strcmp(rate.DataInfo.Units, output_unit)
        obj.p = struct;
        obj.p.output_unit = output_unit;
        x.rate = rate;
        y = BPTT.calculators.calc_convert_variables(x,obj);
        converted_rate = y.rate_conv;
    end
    
end

function [handle] = horizontal_line(y_value)
    ax = gca;
    xlim = ax.XLim;
    handle = line(xlim, [y_value, y_value],'LineStyle','--');
end

function [handle] = vertical_line(x_value)
    ax = gca;
    ylim = ax.YLim;
    handle = line(ylim, [x_value, x_value],'LineStyle','--');
end

function [varargout] = calculate_mean(varargin)
    input_number = numel(varargin);
    varargout = cell(input_number,1);
    for index = 1:input_number
        varargout{index} = varargin{index};
        if size(varargin{index}.Data,2)>1
            varargout{index}.Data = mean(varargout{index}.Data,2);
        end
    end
end