function [x_out y_out] = sago(x,y,n,d,f,w,flag)
    % Generalized Savitzky Golay filter for even unevenly-spaced data
    % It still cannot handle the endpoints, but needs to be added
    
    % x     time vector
    % y     data points
    % d     differential order
    % n     polynomial order
    % f     frame size - must be odd
    % w     weights
    % flag  0/1 specify whether start and endpoints are to be estimated
    
    % (c) Aydin Golabgir 17.08.2014
    
    if mod(f,2)==0
        error('Sago frame length must be odd.')
    end
    
    N = length(x); % total length of the input data
    
    if N<f
        error('Number of data points must be greater than or equal the frame size')
    end

    if n<d
        error('Polynomial order must be greater than or equal to the differential order.')
    end
    
    % if w is not set, assign equal weights to the data
    if isempty(w)
        w = ones(N,1);
    end
    
    y_out = zeros(N-f+1,1);
    x_out = zeros(N-f+1,1);
    
    % loop through data points and calculate the midpoint estimation
    for k=1:N-f+1
    
        idx = k:k+f-1;
        
        x_s = x(idx);
        y_s = y(idx);
        
        x_mid = x_s - x_s((f+1)/2);
        
        A = zeros(length(idx),n+1);
        for i=1:n+1
            A(:,i) = x_mid .^ (i-1);
        end
        

        w_s = eye(f,f);
        for i=1:f
            w_s(i,i) = w(idx(i));
        end

        
        B = (inv(A'*w_s*A)*A'*w_s);
        
        y_out(k) = B(d+1,:) * y_s;
        x_out(k) = x(idx((f+1)/2));
    end
        
%     c = B*x;
%     xhat = A*c;
    
    
end