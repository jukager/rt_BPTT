function [correct_unit] = remove_brackets_and_whitespace ( unit_with_brackets)
    brackets = {'(', ')', '[', ']','{','}',' '};
    correct_unit = unit_with_brackets;
    for index = 1:numel(brackets)
        correct_unit = strrep(correct_unit, ...
                              brackets{index},...
                              '');
    end
end