% Vienna University of Technology, Bioprocess Engineering
% Aydin Golabgir last modified: 01.09.2014

function [min_time, max_time] = getminmaxtime(x_in)
    %getminmaxtime returns the minimum and maximum time (datenum) of a
    %TimeVariable collection (struct containing TimeVariables)
    
    % get the names of the time variables
    fn = fieldnames(x_in);
    
    min_time = x_in.(fn{1}).Time(1);
    max_time = x_in.(fn{1}).Time(end);
    
    for i=1:length(fn)
            if x_in.(fn{i}).Time(1) < min_time
                min_time = x_in.(fn{i}).Time(1);
            end
            if x_in.(fn{i}).Time(end) > max_time
                max_time = x_in.(fn{i}).Time(end);
            end
    end

end