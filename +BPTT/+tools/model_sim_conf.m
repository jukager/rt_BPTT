%% Simulation file for arbitary models in configuration format
% (c) Aydin Golabgir 
% Last modified: 21.04.2015

% flag=0, only x will be calculated over a time range
% flag=1, both x and y will be calculated over a time range
% flag=2, return the value of y(t,x) - for one time point

function [t, y, x, h] = model_sim_conf(u,xint,t_in,model,flag)

    y=[];
    x=[];
    h=[];
    
    %% simulation using the symbolic model definitions
    if strcmp(model.simmode,'symbolic')
        [t,y,x] = BPTT.tools.symbolsim(model, t_in, xint, u, flag);
        return
    end
    
    %% simulation using the in-memory non-symbolic method
    if strcmp(model.simmode,'memory')
        
        % performing the ODE simulation
        if flag == 0 || flag == 1
        
            % specify the options
            nonnegative = [1:length(xint)];
            options=odeset('NonNegative',nonnegative);
            % check solver type and perform simulations
            if strcmp(model.solvertype,'15s')
                [t,x]=ode15s(@BPTT.tools.model_sim_memory,t_in,xint,options,0,model,u);
            elseif strcmp(model.solvertype,'23')
                [t,x]=ode23(@BPTT.tools.model_sim_memory,t_in,xint,options,0,model,u);
            else
                error('Invalid solver type.')
            end
        end
        
        if flag==1
            for i=1:length(t)
                y(:,i)=BPTT.tools.model_sim_memory(t(i),x(i,:)',1,model,u);
            end
        end

        % returns the value of y(t,x) only at the end time point
        if flag ==2
            x = xint;
            t = t_in(end);
            y=BPTT.tools.model_sim_memory(t,x,1,model,u);
        end
        
        return
    end
    
    %% simulation using the temp file method ...
    
    % look for or generate the matlab function
    mod_file = which([model.Name '_mod']);
    if isempty(mod_file) || model.recompile
        % compile the model if necessary
        h = BPTT.tools.model_compiler(model);
        h       % need to have these two lines for avoiding an error later on
        which h % need to have these two lines for avoiding an error later on
    else
        h = str2func([model.Name '_mod'] );
    end

    % performing the ODE simulation - calculating x
    if flag == 0 || flag == 1
        % simulation using the matlab function file
        if strcmp(model.simmode,'Matlab')
            %% simulate state variables
            nonnegative = [1:length(xint)];
            options=odeset('NonNegative',nonnegative);
            h = str2func([model.Name '_mod'] );
            if strcmp(model.solvertype,'15s')
                [t,x]=ode15s(char(h),t_in,xint,options,0,model,u);
            elseif strcmp(model.solvertype,'23')
                [t,x]=ode23(char(h),t_in,xint,options,0,model,u);
            else
                error('Invalid solver type.')
            end
        end
    end
    
    % calculate outputs : y values if needed, i.e. specified by flag    
    if flag==1
        for i=1:length(t)
            y(:,i) = h(t(i),x(i,:)',[],1,model,u);
        end
    end
    
    % returns the value of y(t,x) only at the end time point
    if flag ==2
        x = xint;
        t = t_in(end);
        y = h(t,x,[],1,model,u);
        
    end
end