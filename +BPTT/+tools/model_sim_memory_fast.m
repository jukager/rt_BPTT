% function for simulation models in configuration format
% generation and without symbolic expressions
%   call by handle from ODE-Solver or similar
%
%   out = BPTT.tools.model_sim_memory_fast(t, x, yp0, flag, model, u)
%   with t     timepoint
%        x     Statevector for time t
%        yp0   empty exept for ODE15i: initial values for dx
%        flag  0: out is dx | 1: out is y
%        model struct with model in configuration format
%        u     struct of TimeVariabels as model-Inputs
%
% optimized for faster evaluation with single Eval-call
% See also BPTT.tools.model_sim_memory
%
% (c) Peter Ettinger 2017 04
%     Aydin Golabgir, Antje Dittmer

function out = model_sim_memory_fast(t, x, yp0, flag, model, u) %#ok
%model_sim_memory_fast evaluate models in configuration format
% node: yp0 for signatue compatibility with ode15i only

numUs = length(model.u_model);
singleEval = model.fastEval;

% declare and preallocate elements wich are referenced within models
% set of equalsions
p = model.p; %#ok<NASGU> % for calls to eval (p is referenced in .EQ, .x and .y -strings)
EQ = cell2struct(num2cell(nan(singleEval.numEQs+numUs,1,'double')),[singleEval.nameEQs;model.u_model(:)]);
%EQ = cell2struct(num2cell(nan(singleEval.numEQs,1,'double')),singleEval.nameEQs);
dx = nan(singleEval.numStates,1,'double');

%insert values for inputs u
if isfield(model, 'interp') && strcmp(model.interp,'interpfast')
    for i = 1:numUs
        EQ.(model.u_model{i}) = BPTT.tools.interpfast(u.(model.u_data{i}).Time,u.(model.u_data{i}).Data,t);
    end
else %
    for i = 1:numUs
        EQ.(model.u_model{i}) = interp1(u.(model.u_data{i}).Time,u.(model.u_data{i}).Data,t,'linear','extrap');
    end
end
% evaluate Results for EQ and dx
try
    eval(singleEval.EQsAndStates);
catch simErr
    warning('model_sim:Model','Failure evaluating Model: %s',simErr.message)
    % get Failure souce
    for i = 1:singleEval.numEQs  % search throu Model-Equalsions EQ
        try
            EQ.(singleEval.nameEQs{i}) = eval(model.EQ.(singleEval.nameEQs{i}));
        catch simErrEQ
            error('model_sim:EQs','Failure in EQ.%s: %s',singleEval.nameEQs{i},simErrEQ.message)
        end
    end
    for i = 1:singleEval.numStates % search throu State-Equalsions x
        try
            dx(i,1) = eval(model.x.(singleEval.nameStates{i}).dxdt);
        catch simErrState
            error('model_sim:States','Failure in x.%s.dxdt: %s',singleEval.nameStates{i},simErrState.message)
        end
    end
end % evaluate Results for EQ and dx

if flag == 0
    out = dx;
    
elseif flag == 1
    out = nan(singleEval.numOuts,1,'double');
    % evaluate Results for y
    try
        eval(singleEval.Outputs);
    catch simErr
        warning('model_sim:Model','Failure evaluating Model-Outputs: %s',simErr.message)
        % get Failure souce
        for i = 1:singleEval.numOuts
            try
                out(i,1) = eval(model.y.(singleEval.nameOuts{i}).eq);
            catch simErr
                error('model_sim:Outputs','Failure in y.%s.eq: %s',singleEval.nameOuts{i},simErr.message)
            end
        end
    end
else
    % unknoewen option for argument 'flag'
end

end
