function [significant_value, significant_error] = reduce_to_significant_digits(value, error)
    
    number_of_significant_digits = 1;
    while 1 > abs(error) * 10^(number_of_significant_digits)
        number_of_significant_digits = number_of_significant_digits + 1;
    end
    
    significant_value = round(value,number_of_significant_digits);
    significant_error = round(error, number_of_significant_digits);
end