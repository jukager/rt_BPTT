function [x_out,y_out,I] = roboutlier(x,y,t,f)
    % Function for robust outlier detection
    % Aydin Golabgir - 17.08.2014
    % x: time vector
    % y: data vector
    % t: treshold
    % f: frame length

    
    N = length(x); % total length of the input data
    
    if N<f
        error('Number of data points must be greater than or equal the frame size')
    end
    
    if mod(f,2)==0
        error('Frame length must be odd.')
    end
    
    y_out = zeros(N-f+1,1);
    x_out = zeros(N-f+1,1);
    I = zeros(N-f+1,1);
    
    % loop through data points in a framewise fashion
    for k=1:N-f+1
        
        idx = k:k+f-1;
        
        x_s = x(idx);
        y_s = y(idx);
        
        
        % implementing mad
        % http://en.wikipedia.org/wiki/Median_absolute_deviation
        scale = 1.4826 * median(abs(y_s - median(y_s)));
        
         %scale = std(y_s);
         location = median(y_s);
        
         d = abs((y_s((f+1)/2) - location) / scale);
        
        if d>t
            y_out(k) = location;
            I(k) = 1;
        else
            y_out(k)=y(idx((f+1)/2));
        end
        
        x_out(k) = x(idx((f+1)/2));
        
        
    end

end