% function for simulation models in configuration format
% generation and without symbolic expressions
% (c) Aydin Golabgir
% Last modified: 22.04.2015

function out = model_sim_memory(t,x,flag,model,u)
%model_sim_memory evaluate models in configuration format
    
    p = model.p; % for calls to eval (p is referenced in .EQ, .x and .y -strings)

    for i=1:length(model.u_model)
        EQ.(model.u_model{i}) = interp1(u.(model.u_data{i}).Time,u.(model.u_data{i}).Data,t,'linear','extrap'); 
    end

    EQs = fieldnames(model.EQ);
    for i=1:length(EQs)
        try
            EQ.(EQs{i}) = eval(model.EQ.(EQs{i}));
        catch simErr
            error('model_sim:EQs','Failure in EQ.%s: %s',EQs{i},simErr.message)	
        end
        %if EQ.(EQs{i}) < 0
        %EQ.(EQs{i}) = 0
        %end
    end

    states = fieldnames(model.x);
    for i=1:length(states)
        try
            dx(i,1) = eval(model.x.(states{i}).dxdt);
        catch simErr
            error('model_sim:States','Failure in x.%s.dxdt: %s',states{i},simErr.message)	
        end   
    end

   if flag == 0
       out = dx;
   
   elseif flag ==1
       outputs = fieldnames(model.y);
        for i=1:length(outputs)
            try
                out(i,1) = eval(model.y.(outputs{i}).eq);
            catch simErr
                error('model_sim:Outputs','Failure in y.%s.eq: %s',outputs{i},simErr.message)	
            end                
        end
   end
   
end
