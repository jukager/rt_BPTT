% custom functiona for adding combining TimeVariable objects
% Vienna University of Technology, Bioprocess Engineering
% Aydin Golabgir
% Last modified: 22.09.2014

% TS1 and TS2 are Timevariable objects - TS2 will be added to the end of TS1

function TS_out = addsample_AGO(TS1,TS2)
    
    % first set TS_out equal to the input TS
    TS_out = TS1; 
    % delete everything in TS_out
    TS_out = delsample(TS_out,'Index',1:TS_out.Length);
    
    %TS_out = BPTT.TimeVariable;
    %TS_out.Name = TS1.Name;
    
    %if ~isempty(TS1.DataInfo.Units)
    %    TS_out.DataInfo.Units = TS1.DataInfo.Units;
    %end
    
    %if ~isempty(TS1.TimeInfo.Units)
    %    TS_out.TimeInfo.Units = TS1.TimeInfo.Units;
    %end
    
    % add the combined values to TS_out

    TS_out.Time = [TS1.Time ; TS2.Time];
    TS_out.Data = [TS1.Data ; TS2.Data];

        
end
    
