% function for peforming model simulation using symbolic expressions
% does not require temp file generation

function [t,y,X] = symbolsim(model, Timeframe, xint, u, flag)

    L = model.sym_f;
    Y = model.sym_y;


    p = model.p;
    fn = fieldnames(model.EQ);
    for j=1:length(fn)
       sym(fn{j},'real');
       L = subs(L,fn{j},eval(model.EQ.(fn{j})));
       Y = subs(Y,fn{j},eval(model.EQ.(fn{j})));
    end

    % substitute the values of parameters in the symbolic expressions
    fn = fieldnames(model.p);
    for j=1:length(fn)
       sym(fn{j},'real');
       L = subs(L,fn{j},p.(fn{j}).Value);
       Y = subs(Y,fn{j},p.(fn{j}).Value);
    end

    X = sym('X',[length(fieldnames(model.x)),2]); % define symbolic state variables (mol/l)                      
    X = X(:,1);
    X = sym(X, 'real');
    
    if length(model.sym_u)>0
        MF = matlabFunction(L,'vars',[X' , model.sym_u]);
        MY = matlabFunction(Y,'vars',[X' , model.sym_u]);
    else
        MF = matlabFunction(L,'vars',[X']);
        MY = matlabFunction(Y,'vars',[X']);
    end
    
    [t,X] = integrate(Timeframe, xint, model, u);
    y=[];
    
    if flag~=0
        y = output(Timeframe, X, model, u);
    end

    %ode23(a,[0 1],[MA.xint 0 0 0])

        function uv = feed(tv,u)
        % returns the value of inputs at each time point
            fn = fieldnames(u);
            for i=1:length(fn)
                uv(i,1) = interp1(u.(fn{i}).Time, u.(fn{i}).Data , tv, 'linear','extrap');
            end
        end

        function fv = f(tv, xv, model, u)
            % compatibality for models with no input variables
            if length(model.sym_u)>0
                xu = [model.sym_x ; reshape(model.sym_u,4,1)];
                uv = feed(tv, u);
                xuv = [xv ; uv];
            else
                xuv = xv;
            end
            fv = zeros(length(L),length(tv));
            %for j=1:length(tv)
                %fv(:,j) = double(subs(L,xu,xuv(:,j)));
                xu_cell = num2cell(xuv,2);
                fv = MF(xu_cell{:});
            %end
        end

        % calculating the output function
        function hv = output(tv, xv, model, u)
            xu = [model.sym_x ; reshape(model.sym_u,length(model.sym_u),1)];
            hv = zeros(length(Y),length(tv));
            for j = 1:length(tv)
                
                % compatibality for models without input variables
                if length(model.sym_u)>0
                    uv = feed(tv(j), u);
                    xuv = [xv(j,:)' ; uv];
                else
                    xuv = xv(j,:)';
                end
                
                xu_cell = num2cell(xuv,2);
                hv(:,j) = MY(xu_cell{:});
                %hv(:,j) = double(subs(Y,xu,xuv));
            end
        end         

        function [t, X] = integrate(Tinput, X0, model, u)
        % integrating the state space equations
            options = odeset('NonNegative',[1:length(X0)]);
            % add other solver types here!
            [t, X] = ode15s(@f, Tinput, X0 , options, model, u);
        end

end