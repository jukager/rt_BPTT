% function for downsampling a structure containing TimeVariable objects
% dt is the  time interval between time points in units days

function TS_out = downsample(TS_in, dt)

    fn = fieldnames(TS_in);
    TS_out = TS_in;
    
    for i = 1:length(fn)
        
        TS_out.(fn{i}) = delsample(TS_out.(fn{i}),'Index',[1:TS_out.(fn{i}).Length]);
        t0 = TS_in.(fn{i}).Time(1);
        tend = TS_in.(fn{i}).Time(end);
        t = t0:dt:tend;
        
        TS_out.(fn{i}).Time = t';
        TS_out.(fn{i}).Data = interp1(TS_in.(fn{i}).Time  ,TS_in.(fn{i}).Data,t','linear','extrap');
        if ~isempty(TS_out.(fn{i}).Quality)
            TS_out.(fn{i}).Quality = interp1(TS_in.(fn{i}).Time,TS_in.(fn{i}).Quality,t,'nearest');
        end
        
    end
    
end