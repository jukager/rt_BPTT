function varargout = rm_special_characters(string, varargin)
    %%
    %   This function removes all special characters from a given string
    %   and returns strings consisting only of simple letters, numbers
    %   and underscores.
    %
    %   For default it shows the user a warning dialog when characters were 
    %   replaced. To avoid this, append the two args: 'warning', 0
    %   Output: [newName,changed_status]
    %               newName is the changed name; changed_status is 1 if the
    %               string was changed and 0 when the string remained the
    %               same.
    %%
    
    if ~isempty(varargin)
        warning = varargin{find(strcmpi('warning', varargin))+1};
    else
        warning = 1;
    end
    
    if isa(string, 'char')
        [sec_str, ori_str] = replace_string(string);
        
        corr = ~strcmp(sec_str, ori_str); % String corrected?
        if strcmp(strrep(ori_str, ' ', '_'), sec_str)
            corr = 0;
        end
        if corr && warning
            warndlg('The name containted unallowed special characters or whitespaces. They have been removed. Please use only characters, numbers and underscores and start your name with a character.', 'Special Characters removed');
        end
        
        sec_str = remove_redundant_underscores(sec_str);
        varargout{1} = sec_str;
        if nargout > 1
            varargout{2} = corr;
        end
    elseif isa(string, 'cell')
               
        % Make sure only one dimensional cell arrays are accepted
        if size(string, 1) > 1 && size(string, 2) > 1
            error('Only cell arrays with one row or colum are supported at the moment');
        end
        
        str_num = numel(string);
        sec_str = cell(str_num, 1);
        ori_str = cell(str_num, 1);
        corr = zeros(str_num, 1);
        
        for i = 1:numel(string)
            [sec_str{i}, ori_str{i}] = replace_string(string{i});
            sec_str{i} = remove_redundant_underscores(sec_str{i});
            corr(i) = ~strcmp(sec_str{i}, ori_str{i});
        end
        
        % Correct cell array dimensions?
        if size(string, 1) ~= size(sec_str, 1)
            sec_str = sec_str';
            corr = corr';
        end
        
        sec_str = remove_redundant_underscores(sec_str);
        varargout{1} = sec_str;
        if nargout > 1
            varargout{2} = corr;
        end
    else
        error('The input variable type is not supported. Please enter char or cell');
    end
        
    
    function [sec_str, ori_str] = replace_string(string)

        % Make valid string name
        ori_str = string;
        string = strtrim(string);
        string = string';
        idx = (double(string) < 48) | (double(string) > 57 & double(string) < 65) | (double(string) > 90 & double(string) < 97) | (double(string) > 122);
        string(idx) = 95; % make underline instead
        sec_str = string';
        sec_str = matlab.lang.makeValidName(sec_str);

    end

    
end

function [corrected_string] = remove_redundant_underscores(uncorrected_string)
    corrected_string = uncorrected_string;
    while strcmp(corrected_string(end),'_')
        corrected_string(end) = [];
    end
    
    underscore_idx = strfind(corrected_string,'_');
    delete_idx = find(diff(underscore_idx)==1);
    while ~isempty(delete_idx)
        corrected_string(underscore_idx(delete_idx(1))) = [];
        underscore_idx = strfind(corrected_string,'_');
        delete_idx = find(diff(underscore_idx)==1);
    end
end

