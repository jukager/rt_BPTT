function [ ind ] = particlefilter_sir_resampling( P , qi  )
% SIR resampling of particle filter
% P is the Population size
% qi is the punctual Probability of every Particle

P_q = qi ./ sum(qi);
P_q_kummuliert = cumsum(P_q);  % IST: Vector whose components are qi that are summed up
rn = rand(1,P);              % IST: rand generates uniformly distributed random numbers in [0,1]
ind = zeros(1,P);

for j = 1:P
    try
        ind(j) = find(P_q_kummuliert>=rn(j),1); %% IST: Variablen nur umgestellt - cum ist Vektor mit kummulativen Eintr�gen der qi.
    catch err % in case of the singularity error, select randomly
        warning(err.message)
        ind(j) = j; % ceil(rand*P.N); % random selection
        warning('Resampling failed, propagation of all particles')
    end
end

end

