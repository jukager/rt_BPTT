function [valid_name] = create_valid_name(name)
    name = regexprep(name,'[.]','_');
    name = regexprep(name,'[1 2 3 4 5 6 7 8 9 0 + -]','');
    name = regexprep(name,'[? �]','mu');
    valid_name = name;
end