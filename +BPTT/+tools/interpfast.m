function yu = interpfast(x,y,u)
%% fast linear interpolation
% Performs 1D linear interpolation of 'u' points using 'x' and 'y',
% resulting in 'yi', following the formula yu = y1 + (y2-y1)/(x2-x1)*(u-x1).
% Returns copies of nearest values for values of 'u' out of range.
%
% 'x'  is the monotonically increasing column vector [n x 1].
% 'y'  is the data matrix [n x m], corresponding to 'x'.
% 'u'  is aimed column vector [ui x 1]
% 'yu' is matrix [ui x n], corresponding to 'u'.
%
% Approach of Loren Shure; http://blogs.mathworks.com/loren/2008/08/25/piecewise-linear-interpolation/
% Implemented by Julian Kager, 06/2017

%% Test requirements
%search for duplicate time points
%x_unique = unique(x); !!!very slow!!!
% if length(x_unique) < length(x)
%    error('duplicate times')
% end
%check for monotonically increasing times
xIsNotMonoton = ~all(diff(x) > 0); % strictly increasing increasing
if xIsNotMonoton
    error('time is not monotonically increasing or contains duplicates')
end
%%
n = size(x,1); % length of column vector x
m = size(y,2); % Matrix extension of y (usally 1)
%get the index of the 'x' which is the interpolation start value (left)
[~,k] = histc(u,x); % matlab recommends [~,~,k] = histcounts(u,x); but is sligthly slower

%k(k == n) = n - 1; % truncate k if k > n --> original code
k = max(k,1);     % truncate if index=0 (u < x(1)) !!!no extrapolation!!!
k = min(k,n-1);   % truncate if index=n+1 (u > x(end)) !!!no estrapolation!!!

%du = u-x(k); % u - x differences to reference index
%u = x(k+1)-x(k); % x(+1) - x differences = 1 for equidistant data
%t = du./u;
%yu = (1-t(:,ones(1,m))).*y(k,:) + t(:,ones(1,m)).*(y(k+1,:)); % actual linear
% interpolation according to formula

% ~30% faster if code is putted together
t  = (u - x(k)) ./ (x(k+1) - x(k));
yu = y(k,:) + t(:,ones(1,m)) .* (y(k+1,:) - y(k,:));

%yu(u > x(end),:) = y(end,:) .* ones(sum(u > x(end)),m);%
% as we now the point x(end) as x(n,1) and y(end,:) as y(n,:) because by
% convention x and y must have same number of rows n (what is not checked!!)
% furthermore y(end,:) .* ones(sum(u > x(end)),m) WILL issue an error if
% m >1 and sum(u > x(end)) >1 due to missmatch of vector/matrix size -->
% insufficent testcases 
expolTailIdx =  u > x(n,1); %logical Index for SampleHold Points at end of u
yu(expolTailIdx,:) = ones(sum(expolTailIdx),1) * y(n,:);

%yu(u < x(1),:) = y(1,:) .* ones(sum(u < x(1)),m);
% analog to above
expolHeadIdx =  u < x(1,1); %logical Index for SampleHold Points at start of u
yu(expolHeadIdx,:) = ones(sum(expolHeadIdx),1) * y(1,:);

end % interpfast %