function [out] = Coryn_opt(x,xinit,t,model)
%Simulates model with changed dynamic feed given as x vector
% x1 = feed and dilution
% x2 = feed concentration

%% define new feed regimes
u.glucose_feed = BPTT.TimeVariable;
u.glucose_feed.Name = 'glucose_feed';
u.glucose_feed.Time = (1:0.1:170);
u.glucose_feed.Data = ones(length(u.glucose_feed.Time),1).* x(1); % L/h
%u.glucose_feed.Data(1:50)= 0; % batch phase = no feed

%% Change model parameters
model.p.qs.Value = x(1); % conc. of glycerol in Feed [g/L]
%ind = x(1)*10;

%% Simulate model with adapted feeds and parameters

[t,y1,x1]=BPTT.tools.model_sim_conf(u,xinit,t,model,1);

%% define your minimization objective
%scaled with maximal values

%objective1 = (y1(13,:)+y1(1,:)+y1(11,:));
objective1 = max((y1(13,:))+(y1(12,:))+(y1(11,:)));
if objective1 > 3
    objective1 = 100;
end
objective2 = max(y1(10,:));

out = objective1+(objective2)*(-1);

disp(x)
disp(out)
    
end

