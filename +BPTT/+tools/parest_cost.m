function [ out ] = parest_cost(p0,obj,plotflag)
% cost function for parameter estimation
% (c) Aydin Golabgir
% Inputs:
%   p0: the current parameter values (vector)
%   td: time span for comparing simulation and measurements
%   ParEst: parameter names which are being estimated
%   model: structure containing model information, equations, etc.
%   Experiments: structure containing experiment(s) (e.g. bioprocess objects)
%   comp_data: name of variables in experiments used for comparison
%   comp_model: name of variables in model used for comparison
%   output_format: 0/1 one value or a vector
%   Quality: quality/phase code for experimental data
%   xint: initial values for model state variables / can be a vector or matrix if more than one experiments are fitted
%   meas_error: absolute expected measurement error

    tic
    % filter out disabled experiments
    enabled = obj.AllEnabled;
    enabled = genvarname(enabled); % generate fieldnames of enabled experiments
    selected_experiments = struct;

    for i=1:length(enabled) % copy enabled experiments to exp. struct
        selected_experiments.(enabled{i}) = obj.Experiments.(enabled{i});
    end

    fn = fieldnames(selected_experiments);   % get the names of all input experiments
    u_names = obj.Model.u_data;    % the name of input variables in experiments

    % some error checking
    if length(obj.comp_model) ~= length(obj.comp_data)
        error('The length of data and model comparison variables are not equal.')
    end

    % extract required variables from the MA object
    Experiments = selected_experiments;
    comp_data = obj.comp_data;
    model = obj.Model;
    ParEst = obj.ParEst;
    td = obj.Timeframe;
    xint = obj.xint;
    obj.exp_data ={};
    ny = length(fieldnames(model.y)); % number of model outputs
    nx = length(fieldnames(model.x)); % number of model states
    %obj.xsim = {};
    %obj.ysim = {};
    % Update model specific parameter set
    if numel(selected_experiments) >= 2 || isempty(xint) % in case of multiple Experiments or empty xint
        xint=zeros(length(fn),nx);
        %td = {}; preparation for Experiement specific different timeframes
        for i=1:length(fn) % build up matrix holding initial conditions from experiment model storage
            xint(i,:) = Experiments.(fn{i}).Models.xint;
            %td{i} = obj.Experiments.(exp{i}).Models.Timeframe;
        end
    end
    if isempty(xint)
        error('xint not found withnin MA.xint or MA.Experiments.BatchName.Models.xint')
    end

    % loop through the bioprocess objects (experiments) - if more than one
    exp_data = cell(length(fn));
    reftime = cell(length(fn));

    for j=1:length(fn)

        selected_experiments = fieldnames(obj.Experiments);
        % extract input variables from the bioprocess object
        u = Experiments.(fn{j}).getvars(u_names,'Quality',obj.Quality);

        % downsample if specified
        if ~isempty(obj.downsample)
            u = BPTT.tools.downsample(u,obj.downsample);
        end

        % define the reference datetime which is equivalent to sim time zero
        % here the reftimee is taken to be the first time point of the first input variable
        %reftime = u.(u_names{1}).Time(1);
        % extract experimenta data from the experiment/bioprocess
        exp_data{j} = Experiments.(fn{j}).getvars(comp_data,'Quality',obj.Quality);
        try
            reftime{j} = obj.Experiments.(fn{j}).MetaData.StartTime;
        catch
            reftime{j} = obj.Experiments.(fn{j}).MinTime;
        end
        % convert units of inputs from days to hours
        for i=1:length(u_names)
            u.(u_names{i}).Time = (u.(u_names{i}).Time - reftime{j}) .* 24;
        end

        for i=1:length(comp_data)
            % convert units of experimental data from days to hours based on the reftime
            %exp_data.(comp_data{i}).Time = (exp_data.(comp_data{i}).Time - reftime) .* 24;
            exp_data{j}.(comp_data{i}).Time = (exp_data{j}.(comp_data{i}).Time - obj.Experiments.(selected_experiments{j}).MinTime) .* 24;

            % cut out the experimental data which is out of the range of interest
            %exp_data.(comp_data{i}) = getsampleusingtime(exp_data.(comp_data{i}),td(1),td(end));
        end
        obj.exp_data{1,j} = exp_data{j}; % store exp data

        % find the index of model outputs to be compared
        model_idx = obj.getOutputIdx;

        % replace model parameters with p0
        for i=1:length(ParEst)
            model.p.(ParEst{i}).Value = p0(i);
        end

        % assign experimentn specific model constants
        if isfield(Experiments.(fn{j}).Models, 'p')
            constants = fieldnames((Experiments.(fn{j}).Models.p));
            model_exp= model;
            for i= 1:length(constants)
                model_exp.p.(constants{i}).Value = Experiments.(fn{j}).Models.p.(constants{i}).Value;
            end
        else
            model_exp = model;
        end
        % perform a simulation with model and experimental inputs
        [mT,ysim,xsim]=BPTT.tools.model_sim_conf(u,xint(j,:),td,model_exp,1);

        % check length and restructure in case of solver failure
        if length(ysim)<length(td)
            disp(['bad simulation :-( refilled with zeros; stopped @', num2str(max(mT))])
            mT= [];
            mT= td';
            ysim = [ysim, zeros(ny, length(td) - length(ysim))]; %fill y_sim to length of td
            xsim = vertcat(xsim, zeros(length(td) - length(xsim),nx));  %fill x_sim to length of td
        end

        % add simulation results to the MA object
        if plotflag
            if ~isfield(obj.ysim, (fn{j}))
                obj.xsim.(fn{j}) = xsim ;
                obj.ysim.(fn{j}) = ysim;
                obj.mT = mT;
            else
                obj.mT = mT;
                %obj.xsim(j) = xsim;
                %obj.ysim(j) = ysim;
                %xsim_var = obj.xsim{j};
                obj.xsim.(fn{j}) = cat(3 ,obj.xsim.(fn{j}),xsim);
                %ysim_var = obj.ysim{j};
                obj.ysim.(fn{j}) = cat(3 ,obj.ysim.(fn{j}) ,ysim);
            end
        end
        % calculate a fitness/likelihood for each measurement
        L = zeros(length(comp_data),1);
        L_exp.(fn{j}) = zeros(length(comp_data),1);
        for i=1:length(L)
            exp_data_var = exp_data{j};
            if exp_data_var.(comp_data{i}).Length>0

                % interpolate the simulated outputs onto the measured time
                ysim_interp = interp1(mT,ysim(model_idx(i),:),exp_data_var.(comp_data{i}).Time);

                mu = exp_data_var.(comp_data{i}).Data;
                sigma = obj.meas_error(i)*abs(nanmean(mu)); %relative error
                %disp(sigma)
                %if sigma < obj.min_meas_error(i);
                %sigma = obj.min_meas_error(i);
                %disp(['min abs error of ', comp_data{i}])
                %end
                %negative log likelihood function
                l = (-1/2)*log(2*pi) - (1/2) .* log(sigma.^2) - 1./(2*sigma.^2) .* (ysim_interp-mu).^2;
                L(i) = -1 * nanmean(l);
            else
                L(i)=0;
                disp(L)
            end
            L_exp.(fn{j})(i) = L(i);
        end
    end

    % displaying the likelihood values
    %disp([num2str(L') ' ' num2str(sum(L))])

    if size(p0,1)>size(p0,2)
        p0 = p0';
    end

    % returning the output depends on the specified output format
    if obj.output_format == 0   % fminsearchbnd
        %out = sum(L);
        out = sum(structfun(@sum,L_exp)); %summing up all likelihoods for fmin search
        if plotflag
            obj.estimated_p = [obj.estimated_p ; p0];   % store the estimated parameter values
        end
    elseif obj.output_format ==1 % patternsearch
        out = sum(L);
        if plotflag
            obj.estimated_p = [obj.estimated_p ; p0];   % store the estimated parameter values
        end
    end

    if plotflag
        for j=1:length(fn)
            if ~isfield(obj.L, (fn{j}))
                obj.L.(fn{j}) = L_exp.(fn{j})' ;
            else
                obj.L.(fn{j}) = [obj.L.(fn{j}) ; L_exp.(fn{j})']; % store the likelihood values in the object
            end
        end
        obj.EstimationTime = now;                   % update the estimation time
        obj.Cost = [obj.Cost ; out];
    end
    % function to generate plots every 20 simulations
    if ((obj.output_format == 0 || obj.output_format == 1) && mod(size(obj.estimated_p,1)+1,5)==0) || (obj.output_format == 2 && plotflag)
        figure(1)
        obj.plot_estimated
        drawnow

        figure(2)
        obj.plot_variables
        drawnow
    end

    elapsed = toc;
    if obj.verbose == 1
        disp(['Time for cost-function evaluation:' num2str(elapsed) ' sec'])
    end

end

