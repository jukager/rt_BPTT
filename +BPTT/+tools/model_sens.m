%  Local sensitivity analysis
%  The output sensitivity matrix are given for central perturbations. 
%  (c) Aydin Golabgir
%   03/2018 Julian Kager changed to incremental Sensitivity

function [t, y_ref, dydpc] = model_sens(obj)

    % initialize the required variables for sensitivity analysis
    td = obj.Timeframe;
    idx = obj.ParEst;
    model = obj.Model;
    experiment = obj.Experiments;
    u_names = obj.Model.u_data;
    try
    y_error = obj.y_error
    catch
    y_error = 0.05
    end
    
    % number of model outputs
    ny = length(fieldnames(model.y)); 

    n=length(td);			% number of data points
    m=length(idx);			% number of parameters to be perturbed
    dydpc=zeros(n,ny,m);    % initialize 3D parameter value normalized sensitivity matrix
    dydpc_abs= zeros(n,ny,m); % % initialize 3D absolute sensitivity matrix
    
    p_ref = model.p;     % reference parameter, original values of the parameters
    
    % extract the input variables from the first experiment. Currently
    % sensitivity analysis supports only one experiment in the struct.
    fn = fieldnames(experiment); 
    u = experiment.(fn{1}).getvars(u_names,'Quality',obj.Quality);
    
    % downsample if specified
    if ~isempty(obj.downsample)
        u = BPTT.tools.downsample(u,obj.downsample); 
    end
    
    % define the reference datetime which is equivalent to sim time zero
    % here the reftimee is taken to be the first time point of the first input variable
    reftime = u.(u_names{1}).Time(1);
    
    % convert units of inputs from days to hours
    for i=1:length(u_names)
    	u.(u_names{i}).Time = (u.(u_names{i}).Time - reftime) .* 24;
    end
    
    % show available parameters and check if selcted exist
    disp(['available_parameters ' strjoin(fieldnames(p_ref)',' , ')]);
    for i=1:m  %check if parameter exists
        param_incl(i)= p_ref.(idx{i}).Value;   
    end 
    disp(['all ' num2str(length(idx)) ' selected parameters available'])
    
    %% perform stepwise reference simulations
    disp('Reference simulation ...')
    tic
    % get time intervals from obj.Timeframe 
    deltatd = median(diff(td));
   
    % initialize Matrices
    xint = obj.xint; % startvalues
    y_ref= zeros(ny, length(td));
    x_ref = zeros(length(td), length(xint));
    x_ref(1,:)= xint;% set startvalues
    
    for ii = 1:length(td)
    [t,y,x]=BPTT.tools.model_sim_conf(u,xint,[td(ii),(td(ii)+deltatd)],model,1); % is used to define new startvalue
    y = y(:,end);
    xint = x(end,:); % set new startvalue
    x_ref(ii+1,:)= xint; % stores reference model states used as startpoints
    y_ref(:,ii)= y; % stores reference model outputs to calculate deviation
    end
    toc
    %% stepwise simulation for each parameter with deflection
    for i=1:m  % loop throught parameters (could be parallelized)
        p = p_ref;       % set current parameter value 
        dp=zeros(m,1);   % initialize dydp to Zero
        y_per= zeros(size(y_ref)); % initialize deflected output matrix
        
         disp(['Parameter ' num2str(i) ' of ' num2str(m) ]) % display simulation number
        param_value = p_ref.(idx{i}).Value; % get value of the param
        dp(i) = obj.pert * abs(param_value); % parameter perturbation

        % positive pertubation
        p.(idx{i}).Value = p_ref.(idx{i}).Value + dp(i); % perturb parameter p(i)
        new_model = model;
        new_model.p = p;

        % stepwise simulation
        for j = 1:length(td)
        [t,ystep,~]=BPTT.tools.model_sim_conf(u,x_ref(j,:),[td(j),(td(j)+deltatd)],new_model,1);
        ystep = ystep(:,end);
        y_per(:,j)= ystep;     
        end
        
        % derivative of output with respect to parameter value
        % further scaling will occur in ModelAnalysis
        dydpc_abs(:,:,i) = (y_per'-y_ref')./(deltatd)./(dp(i)); % absolute sensitivity
        dydpc(:,:,i) = (y_per'-y_ref')./(deltatd) .* abs(param_value) ./(dp(i)) ; % absolute parameter normalized sensitivity
  
    end
    
    %dydpc_sum = reshape(sum(dydpc_abs,1),ny,m);
    %W = 1./((eye(ny).*y_error'.*(mean(y_ref,2)))^2);
    %W(isinf(W)|isnan(W)) = 0;
    %FIM = dydpc_sum'*W*dydpc_sum;
    %rank(FIM)
    %cond(FIM)
    %corr(FIM)
    %eig(FIM)
end
