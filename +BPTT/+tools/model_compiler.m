% function for compiling the models from struct format to function handle
%
%   h = BPTT.tools.model_compiler(model)
% where
%   model is a struct (model in configuration format)
%
%   creates or replaces the model-function file named [model.Name]_mod.m
%   and returnd a function-handle to it.
%
function h = model_compiler(model)

    % open/create the file
    fid = fopen([model.Name '_mod.m'],'w');
    
    % add function header
    %fstr = ['function [out] = ' model.Name '_mod(t,x,~,flag,model,u)\n'];
    fstr = ['function [out] = ' model.Name '_mod(t,x,yp0,flag,model,u) %%#ok\n'];
    fstr = strcat(fstr,'%% node: yp0 for signatue compatibility with ode15i only\n');
    fprintf(fid,fstr);
    
    % get model parameters
    fstr = strcat('\n','p = model.p;\n');
    fprintf(fid,fstr);
    
    % interpolate the inputs
    if isfield(model, 'interp') && strcmp(model.interp,'interpfast')
        for i=1:length(model.u_model)
        fstr = ['EQ.' model.u_model{i} '= BPTT.tools.interpfast(u.' model.u_data{i} '.Time,u.' model.u_data{i} '.Data, t);\n'];
        fprintf(fid,fstr);
        end
    else
        for i=1:length(model.u_model)
        fstr = ['EQ.' model.u_model{i} '= interp1(u.' model.u_data{i} '.Time,u.' model.u_data{i} '.Data, t, ''linear'',''extrap'' );\n'];
        fprintf(fid,fstr);
        end    
    end

    % evaluate defined equations, EQs
    EQs = fieldnames(model.EQ);
    fprintf(fid,'\n');
    for i=1:length(EQs)
        %fstr = strcat(fstr,['EQ.' EQs{i} ' = ' model.EQ.(EQs{i}) ';\n ' ]);
        fstr = ['EQ.' EQs{i} ' = ' model.EQ.(EQs{i}) ';\n' ];
        fprintf(fid,fstr);
    end
    
    % evaluate differential equations
    states = fieldnames(model.x);
    fprintf(fid,'\n');
    for i=1:length(states)
       fstr = ['dx(' num2str(i) ',1) = ' model.x.(states{i}).dxdt ';\n'];
       fprintf(fid,fstr);
    end
    
    fstr = ['\n','if flag == 0\n\t' 'out = dx;\n' 'elseif flag == 1\n'];
    fprintf(fid,fstr);
    
    % evaluate model outputs
    outputs = fieldnames(model.y);
    for i=1:length(outputs)
        fstr = ['\t','y(' num2str(i) ',1) = ' model.y.(outputs{i}).eq ';\n'];
        fprintf(fid,fstr);
    end
    fprintf(fid,['\n\t','out = y;\n']);
    fprintf(fid,'end\n');
    
    fprintf(fid,'end\n');
    fclose(fid);
    
    h = str2func([model.Name '_mod'] );
    
end