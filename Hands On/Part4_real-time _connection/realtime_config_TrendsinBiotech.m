%% +BPTT config file
% Matlab Setup for DJANGo
%(c) Julian Kager; Fabian Pollesböck Dez 2016
% Configuration file for Batch retrieval; Calculators and sending Data/ setpoints to Server
%% clear your realtime Workspace
clc
clear all
delete(timerfindall) %deletes all timerobjects (retrieving and sending) from RAM storage !!!deletion of object doesn't help

%% 1) Start Data Import from server
clc
% Show running reactors            Serveradress                Name   Passphrase
r = BPTT.database.Incyght_DB('http://django.vt.tuwien.ac.at/','realtime','realtime'); 
r.debug = true; % debug mode for extended information in console
batch_list = r.get_all_batches(); 
batch_list.reactor_name

%% Select reactor and show avaliable variables
used_reactor = 'fermenter10' % define reactor name
reactor_index = find(strcmp({batch_list.reactor_name}, (used_reactor))==1);
parameter_list = [batch_list(reactor_index).data_names];
parameter_list = strjoin(parameter_list,' , ')

%% define requested parameters as a comma separeted single string
parameter_request = parameter_list

%% prepare for data import
bp = r.create_bioprocess('reactor', (used_reactor), 'parameter', (parameter_request));
r.update_data(bp)
r.update_interval = 30;         % update interval in sec
r.update_calculators = true;    % want to run realtime calculators
%% start data import timer
r.start();                      % start real time object timer (after errors it can be restarted)
%r.stop;                         % stop data import

%% qs Setpoint

clear c 
clear P

P.exp = '0.03 .* sin(x{1}.*(2*3.14/24)-3*3.14/4)+0.035';                             % mathematical expression
%P.exp = '0.03 .* (x{1}./x{1})';
%P.exp = '0.17 - 0.025.* ((x{1})./24)'
P.output_name = 'qs_spt';                       % specify the name of the output signal
P.output_units = 'g/g/h';                        % specify the units of the output signal
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_qs_Spt';               % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
                                   %
c.x = {'ProcTime'};                                   % specify the names of the input signals
c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 3600;                            % calculation interval in [s] higest hierachy
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations
bp.AddCalculator(c);                % add calculator

%% Feed rate calculation for glucose

clear c
clear P

P.SG_window = 10;                      % SG window for taking derivative in number of points - must be odd
P.SG_pol_order = 1;                             % SG polynomial order
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_Glucose_feedrate';    	% specify a name for the calculator
c.f = @BPTT.calculators.calc_sagofeed;          % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = { 'WeightFeed2'};           % specify the names of the input signals / for this calc. input must have units of g

c.Mode = 'online';                              % calculation mode online vs offline
bp.AddCalculator(c);

%% Convert units of glucose feed from g/h to l/h

clear c
clear p

P.exp = 'x{1} ./ 1150';                         % mathematical expression
P.output_name = 'glucose_feed';                 % specify the name of the output signal
P.output_units = 'l/h';                         % specify the units of the output signal
P.min = 0;
P.max = 0.02
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_glucose_feed';              % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'WeightFeed2_feedrate'};                    % specify the names of the input signals

c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations

bp.AddCalculator(c);

%% model simulation calculator to get model outputs as BPTT timevariables
clear c P
% Define calculator function specific paramaters 
%load models/TrendsinBiotech
P.Prediction = 100;                          % prediction horizon in hours
P.Model = model;                            % set model
P.xinit = [1.5 2 8]  ;         % initial conditions at process start
P.reftime = 0;                              % reference time of process start [days] -> needed if times are in days since year O
P.sim_Start = 0;                            % simulation startpoint according to xinit [hours]
%P.u_opt = u_opt;                           % attach predefined feed inputs for prediction (if not active last value is used for prediction)     
P.output_error = ones(1,length(fieldnames(P.Model.y))) .* 0.0; % simulate noise for model outputs (insilico data generation)
P.output_freq=[];                           % not enabled fixed value to 0.2 h
% Define calculator properties
c = BPTT.Calculator;
c.Name = 'sim_calc';                        % calculator name
c.calctimer = [0 0];                        % calctimer reset
c.x = {'glucose_feed'};                       % define inputs -> derive from model struct -> rename if necceessary
c.p = P;                                    % stores parameters in calc obj
c.f = @BPTT.calculators.calc_model_sim_conf; % function handle to sim function

c.Mode = 'online';                          % calculation mode: online vs. offline
c.deadband= 30;                            % calculation interval in [s] higest hierachy
c.minpoints = 3;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations

bp.AddCalculator(c);

%% Write Desired calculated Timevariables to server

% SET Points
upload_sp = timer;                                  %place variable path
upload_sp.TimerFcn = @(~, ~)r.write_setpoints(bp, [bp.Variables.FeedA_Spt]);
upload_sp.Period = 30;  % loop-zeit von timer-object
upload_sp.ExecutionMode = 'fixedSpacing';
upload_sp.Tag = 'setpoints_timer';
response_setpoints=upload_sp.TimerFcn;
start(upload_sp)