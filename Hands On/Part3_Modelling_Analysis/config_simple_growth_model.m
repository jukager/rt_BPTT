% Configuration file for a model definition

clc
clear EQ model p u P
%% Define model parameters
p.qsmax.Value = 1 % max substrate uptake rate [gS/gX/h]
p.qsmax.Unit = 'gS/gX/h' %not manadatory

p.KS.Value = 0.05 % Ks value for glucose [g/L]
p.KS.Unit = 'g/L'

p.Yxs.Value = 0.45 % Yield coefficent for Glucose [gX/gS]
p.Yxs.Unit = 'gX/gS'

% Add here also model constants (concentration, composition...)
p.c_glu.Value = 828.8 % conc. of glucose in Feed [g/L]
p.c_glu.Unit = 'g/L'

%% Model helper equations
% Feed
%EQ.F_in = 'EQ.F_in' % only for information variable model inputs are add automatically!!!
% Substrate uptake rate qs
EQ.qs = 'p.qsmax.Value * ( x(3) / (x(3) + p.KS.Value) )'
% Biomass growth rate
EQ.qx = 'EQ.qs * p.Yxs.Value'

%% Differential equations --> mass flows
% dV/dt
x.V_L.dxdt = 'EQ.F_in' 
% dX/dt
x.X_g.dxdt = 'x(2) * ( EQ.qx - EQ.F_in / x(1) )'
% dS/dt 
x.S_g.dxdt = 'EQ.F_in / x(1) * ( p.c_glu.Value - x(3) ) - EQ.qs * x(2)'

%% Model outputs

y.qs.eq = 'EQ.qs'
y.qs.Unit = 'g/[g/L]'

y.qx.eq = 'EQ.qx'
y.qx.Unit = '1/h'

y.V.eq = 'x(1)'
y.V.Unit = 'L'

y.X.eq = 'x(2)'
y.X.Unit = 'g/L'

y.S.eq = 'x(3)'
y.S.Unit = 'g/L'


%%

model.Name = 'Expgrowth_model';          % name of the model
model.Type = 'ode';                     % only ode is supported
model.p = p;                            % model parameters
model.EQ = EQ;                          % equations of the model
model.x = x;                            % states & differential equations & information
model.y = y;                            % output equations & information

% define the inputs required by the model
model.u_model = {'F_in'}; % --> can automatically used as EQ.F_in... 

% names of the input data timevariables
model.u_data = { 'glucose_feed'};           


model.recompile = 1;                   % 0 or 1 - recompile model every time?
model.solvertype = '15s';              % solver type for matlab solving or failover of C, supports: 23, 15s
model.C_filename = '';                 % currently not used
model.simmode = 'memory';              % can be Matlab or memory
