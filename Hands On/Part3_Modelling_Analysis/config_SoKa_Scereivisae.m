% Configuration file for Sonnleitner Kapelli (sonka) Model
% Reference:
% Sonnleitner, B., & K�ppeli, O. (1986). 
% Growth of Saccharomyces cerevisiae is controlled by its limited respiratory capacity: formulation and verification of a hypothesis. 
% Biotechnology and bioengineering, 28(6), 927-937.

% Implemented 2019 (c) Julian Kager
%clear all
%clc
clear EQ model p u P x y
%% Define model parameters (retrieved from paper)
p.qGlc_max.Value = 3.5;      % maximal specific glucose uptake rate [gS/gX/h]
p.qO2_max.Value = 8;         % maximal specific oxygen uptake rate [mmol/gX/h]
p.muethanol_max.Value = 0.17; % maximal specific growth on ethanol
p.Ks.Value = 0.1;            % Ks value for glucose [g/L]
p.Ko.Value = 5;              % Ks value for glucose [%]
p.Ke.Value = 0.1;            % Ks value for glucose [g/L]
p.Ki.Value = 0.1;            % Ks value for glucose [g/L

% Define Yields (retrieved from paper)
p.Yoxidative.Value = 0.49;      %Biomass yield from oxidative Glucose consumption [g/g]
p.Yreductive.Value = 0.05;      %Biomass yield from reductive Glucose consumption [g/g]
p.Yethanol.Value = 0.72;        %Biomass yield from ethnaol oxidation [g/g]

%Define process constants
p.c_glu.Value = 230;        % conc. of glucose in Feed [g/L]
p.kla.Value = 250;          % specific oxygen transfer rate of reactor [h-1]
p.c_O2_max.Value = 0.2188;  % maximum Oxygen solubility in fermetnaiton broth [mmol/L]
p.MW_S.Value = 180.06;      % Molecular weight glucose [g/mol]
p.MW_E.Value = 46.02;       % Molecular weight of ethanol [g/mol]
p.MW_O.Value = 32;          % Molecular weight of oxygen [g/mol]
p.MW_CO2.Value = 44;        % Molecular weight of CO2
p.OX.Value = 0.57;          % oxygen content of biomass [mol/C-mol]
p.HX.Value = 1.79;          % hydrogen content of biomass [mol/C-mol]
p.NX.Value = 0.15;          % nitrogen content of biomass [mol/C-mol]
%% Predefined dynamic Model inputs
%EQ.F_in = EQ.F_in % Glucose Feed
%EQ.F_out = EQ.F_out 
EQ.dO2_act = 'EQ.dO2'%'x(5)/p.c_O2_max.Value*100'%'EQ.dO2'%

%% Reaction Stoichiometry
    % Molecular weight of biomass
    EQ.MW_B = '12+p.HX.Value+p.OX.Value*16+p.NX.Value*14'; % [g/C-mole]

    
    % calculating yield coefficients based on C/N/H balances - units of a,b,c,... [mol/mol]
    EQ.b = 'p.Yoxidative.Value * p.MW_S.Value / EQ.MW_B';
    EQ.g = 'p.Yreductive.Value * p.MW_S.Value / EQ.MW_B';
    EQ.l = 'p.Yethanol.Value * p.MW_E.Value / EQ.MW_B';
    % Pathway 1 --> glucose + o2 + NH3 --> biomass + CO2 + H2O
    EQ.a = '(3*p.NX.Value*EQ.b)/4 - (p.HX.Value*EQ.b)/4 - EQ.b + (p.OX.Value*EQ.b)/2 + 6';
    EQ.c = '6 - EQ.b';
    EQ.d = '(3*p.NX.Value*EQ.b)/2 - (p.HX.Value*EQ.b)/2 + 6';
    % Pathway 2 --> glucose  + NH3 --> biomass + CO2 + EtOH
    EQ.h = '(p.HX.Value*EQ.g)/6 - EQ.g/3 - (p.NX.Value*EQ.g)/2 - (p.OX.Value*EQ.g)/3 + 2';
    EQ.x = 'EQ.g - (p.HX.Value*EQ.g)/4 + (3*p.NX.Value*EQ.g)/4 - (p.OX.Value*EQ.g)/2';
    EQ.j = '(p.NX.Value*EQ.g)/4 - (p.HX.Value*EQ.g)/12 - EQ.g/3 + (p.OX.Value*EQ.g)/6 + 2';
    % Pathway 3 --> EtOH  + O2 + NH3 --> biomass + CO2 + H2O
    EQ.k = '(3*p.NX.Value*EQ.l)/4 - (p.HX.Value*EQ.l)/4 - EQ.l + (p.OX.Value*EQ.l)/2 + 3';
    EQ.m = '2 - EQ.l';
    EQ.n = '(3*p.NX.Value*EQ.l)/2 - (p.HX.Value*EQ.l)/2 + 3';

%% Calculated Yields from reaction stoichiometry
EQ.Y_O2_Glc = 'EQ.a/p.MW_S.Value*1000'; %mmol/g Yield O2 glucose oxidative
EQ.Y_O2_EtOH = 'EQ.k/p.MW_E.Value*1000'; %mmol/g Yield O2 ethanol oxidative
EQ.Y_EtOH_Glc = 'EQ.j*p.MW_E.Value/p.MW_S.Value'; %g/g Yield ethanol/glucose
EQ.Y_CO2_Glc_O = 'EQ.c/p.MW_S.Value*1000'; % mmol/g Yield CO2 glucose oxidative
EQ.Y_CO2_Glc_R = 'EQ.h/p.MW_S.Value*1000';   % mmol/g Yield CO2 glucose reductive
EQ.Y_CO2_EtOH= 'EQ.m/p.MW_E.Value*1000';     % mmol/g Yield CO2 product

%% Reaction Kinetics (Sates: x(1) = Biomass; x(2) = Glucose; x(3) = Ethanol; x(4) = Volume
% Glucose uptake (4)
EQ.q_Glc = 'p.qGlc_max.Value * x(2) / (x(2) + p.Ks.Value) '

% Ethanol uptake (5)
EQ.mu_EtOH = 'p.muethanol_max.Value * x(3) / (x(3) + p.Ke.Value) * p.Ki.Value / (x(2) + p.Ki.Value)'
EQ.qs_EtOH = 'EQ.mu_EtOH/p.Yethanol.Value'

% Full respiratory capacity (6)

EQ.qO2 = 'p.qO2_max.Value * EQ.dO2_act / (EQ.dO2_act + p.Ko.Value)'

% max Oxidative glucose usage
EQ.q_Glc_oxidative_max = 'EQ.qO2/EQ.Y_O2_Glc'

% current oxidative glucose consumption
EQ.q_Glc_oxidative = 'min(EQ.q_Glc, EQ.q_Glc_oxidative_max)'

% max oxidative ethanol consumption 
EQ.q_EtOH_oxidative_max = '(EQ.qO2 - EQ.Y_O2_Glc * EQ.q_Glc_oxidative)/EQ.Y_O2_EtOH'

% current oxidative ethanol consumption
EQ.q_EtOH = 'min(EQ.qs_EtOH, EQ.q_EtOH_oxidative_max)'

% reductive glucose consumption
EQ.q_Glc_reductive = 'EQ.q_Glc - EQ.q_Glc_oxidative'

% overall growth
EQ.mu = 'p.Yoxidative.Value * EQ.q_Glc_oxidative + p.Yreductive.Value * EQ.q_Glc_reductive + p.Yethanol.Value * EQ.q_EtOH'

% OUR (mol/h)
EQ.OUR = '(EQ.q_Glc_oxidative *EQ.Y_O2_Glc + EQ.q_EtOH * EQ.Y_O2_EtOH)* x(1)*x(4)/1000' 

% CER (mol/h)
EQ.CER = '(EQ.q_Glc_oxidative*EQ.Y_CO2_Glc_O + EQ.q_EtOH*EQ.Y_CO2_EtOH + EQ.q_Glc_reductive*EQ.Y_CO2_Glc_R)* x(1)*x(4)/1000'
%% Differential equations

% dX/dt --> Biomass
x.X.dxdt = 'x(1) *  EQ.mu - x(1) * EQ.F_in / x(4)' 
% dGlc/dt --> Substrate
x.Glc.dxdt = 'EQ.F_in * p.c_glu.Value / x(4)  - (EQ.q_Glc_reductive+EQ.q_Glc_oxidative)* x(1) - x(2) * EQ.F_in / x(4)' 
% detOH/dt --> Product (growth related)
x.EtOH.dxdt = '(-EQ.q_EtOH + EQ.q_Glc_reductive * EQ.Y_EtOH_Glc)*x(1) -x(3) * EQ.F_in / x(4) '
% dV/dt --> Volume
x.V.dxdt = 'EQ.F_in - EQ.F_out'
%dDO2/dt
%x.DO2.dxdt = '(p.kla.Value*(p.c_O2_max.Value-x(5)))-(EQ.q_Glc_oxidative*EQ.Y_O2_Glc + EQ.q_EtOH*EQ.Y_O2_EtOH)*x(1)'

%% Model outputs
y.V.eq = 'x(4)'
y.V.Unit = 'L'

y.X.eq = 'x(1)'
y.X.Unit = 'g/L'

y.S.eq = 'x(2)'
y.S.Unit = 'g/L'

y.P.eq = 'x(3)'
y.P.Unit = 'g/L'

y.q_ox.eq = 'EQ.q_Glc_oxidative '
%y.P.Unit = 'g/g/h'

y.q_red.eq = 'EQ.q_Glc_reductive'
%y.P.Unit = 'g/g/h'

y.q_EtOH.eq = 'EQ.q_EtOH'
%y.P.Unit = 'g/g/h
y.mu.eq = 'EQ.mu'

y.DO2.eq = 'EQ.dO2_act'

y.q_O2.eq = 'EQ.qO2'

y.OUR.eq = '-1*EQ.OUR'

y.CER.eq = 'EQ.CER'

y.RQ.eq = 'EQ.CER/EQ.OUR*-1'
%%

model.Name = 'SoKa';          % name of the model
model.Type = 'ode';                     % only ode is supported
model.p = p;                            % model parameters
model.EQ = EQ;                          % equations of the model
model.x = x;                            % states & differential equations & information
model.y = y;                            % output equations & information

% define the inputs required by the model
model.u_model = {'F_in', 'dO2','F_out'}; 

% names of the input data timevariables
model.u_data = { 'glucose_feed', 'DORedox', 'Harvest'};           


model.recompile = 0;                   % 0 or 1 - recompile model every time?
model.solvertype = '15s';              % solver type for matlab solving or failover of C, supports: 23, 15s
model.C_filename = '';                 % currently not used
model.simmode = 'memory';              % can be Matlab or memory
model.interp ='interpfast'             % selct fast or slow matlab buildin interpolation
