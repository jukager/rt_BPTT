
% This file uses MA algorithms using the JK model

%load SS19_BioVTSIM1_1
%load BIOVT_Gr3_SoKA

model.p.qGlc_max.Value= 3.5;
model.p.Yoxidative.Value = 0.47;

% Add the JK model to the MA object
MA = BPTT.ModelAnalysis; % create a model analysis structure
MA.Model = model;

xint = [0.65 20 1.4 1.5]

MA.xint = xint;
MA.Timeframe = 0:0.1:22; % Set Simulation Timeframe and intervals [h]

% Add the experimental data
B.MetaData.StartTime = B.MinTime;
MA.AddExperiment(B);            % Set Experiment to MA Object

%%
% Testsimulation
tic
[t,y,x]=MA.simulate(1); % 0: only states are calculated 1: states and outputs are calculated
toc
%% change Model outputs to be included in Analysis
MA.selectmodeloutputs(model) % shows available outputs
%% select needed outputs
MA.selectmodeloutputs(model,['X , S , P , q_ox , q_red , q_EtOH ,q_O2 , OUR , CER '])
MA.y_error = [0.08, 0.1, 0.02, 0.2]; %relative error of seleccted outputs
%% Sensitivity analysis
MA.pert = 1e-2; % relative perturbation on parameters
MA.ParEst = {'qGlc_max' , 'qO2_max' , 'muethanol_max','Ks' , 'Ko' , 'Ke' , 'Ki','Yoxidative','Yreductive','Yethanol'};
MA.Sensitivity; % execute sensitivity analysis

%% Scale Sensitivity
MA.ScaleSensitivity(1) %(1 = no scaling; 2 = scaling with state value; 3 = max - min scaling)
%% show SensTimecourse
MA.plot_sensitivitytimecourse
% shows timecourse of scaled sensitivity
% clicable legend not available for Version R2013 (Warning appears)

%%
figure
MA.plot_sensitivity(1) % plot sensitivity and indicate headmaplimits

%% Sensitivity Time Course
% MA.sens_timecourse contains the 3D sensitivity matrix
size(MA.sens_timecourse);
plot(MA.Timeframe, squeeze(sum(MA.sens_timecourse,1)))
legend(strrep(MA.ParEst,'_',' '))

%% Plotting parameter importance ranking
MA.plot_parameter_importance({'S', 'X', 'P'})

%% Identifiability analysis
clc
MA.Identifiability({'X','S','P','CER','OUR'},3,10) % select available state measurements, biggest parameters set, and identifiability treshold 

%
%% Parameter estimation

MA.ParEst = {'qGlc_max' , 'qO2_max' , 'muethanol_max','Ks' , 'Ko' , 'Ke' , 'Ki','Yoxidative','Yreductive','Yethanol'  };      % Set of identifiable Set with highest sensitivity
%MA.ParEst = { 'qGlc_max' ,'muethanol_max','Yoxidative'};

MA.ParEst_UB = ones(1,length(MA.ParEst)).* 2;       % upper bound multiplier for allowed parameter value range
MA.ParEst_LB = ones(1,length(MA.ParEst)).* 0.5;       % lower bound multiplier for allowed parameter value range

% name of data and model outputs to be compared for parameter estimation
MA.comp_data = {'CDW','Glucose','Ethanol',};
MA.comp_model = {'X','S','P'};
MA.meas_error = [0.01 0.05 0.03];    % relative error on measurements

MA.verbose = 1;

 %only needed for optmethod=2
MA.PopulationSize = 50;
MA.Generations = 50000;

% 0: fminsearch 1: patternsearch 2: genetic algorithm 3:global fmincon 
MA.optmethod = 0;
MA.ParameterEstimation

%% Update model structure
p_new = model.p ; %create new parameter set

% overwrite estimated parameters with last entries
for k=1:length(MA.ParEst)                  
p_new.(MA.ParEst{k}).Value = MA.estimated_p(end,(k)); 
end

model.p = p_new                     ; % Set updated parameter Set
model.Name = 'Ecoli_model_calibrated' ; % rename Model
MA.SetModel(model);       % set model to MA Object