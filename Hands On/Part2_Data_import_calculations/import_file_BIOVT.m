% DASbox_importfile
% Vienna University of Technology, Bioprocess Engineering
% (c) Julian Kager
% Last modified: 22.11.2017

clear all
clc

%% Configuration
% Set Meta information
%MetaData.GlucoseFeed = 320;
%MetaData.rho= 1.1515;
MetaData.Stamm = 'Saccharomyces Cerivsae'
%MetaData.POXFeed_gL = 80;
%MetaData.NH3Feed_gL = 100;

batch_name = 'BioVTGR3';

%cd ('S:\FB4\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Experiments\WR20_AP4\Matlab Auswertung'); %but here path were excel is stored
%cd ('\\ds4.vt.tuwien.ac.at\DATEN\FB4\BPT\BPT SHARED DATA\Projects\IMPACTS\01_MP2\Experiments\20180219_AJ7\results');
file_name = ['BioVT_Gruppe3_18042018']; % insert file name

% Important info!
% Do not use any special characters like � in Excel Header it will Cause an
% Error
% Place Time in h in the first column 
% Working example: AgitatorSpeed [rpm]
% ProcTime is recognized as the TimeVariable Name
% rpm is recognized as the TimeVariable Unit

% get file Information
[t,sheet_dat]=xlsfinfo(file_name); 
%% Import online Data
    
    % Searches indicated Excel sheets --> could be extended to more sheets
    % and Experiments
    Experiment = num2str(1);
    idxprocessdata = find(strcmp(['Data' Experiment] ,sheet_dat));
    idxoffline = find(strcmp(['Offline' Experiment] ,sheet_dat));
    onlineraw=sheet_dat{idxprocessdata};
    offlineraw=sheet_dat{idxoffline};

    % for Online data to BPTT timevariables
    [data,Header]=xlsread(file_name,onlineraw);
    Header= Header (1,1:end); % only first row
    [Header, Units]= strtok(Header); 
    % Change Headers to be compatible ewith BPTT
    Header = BPTT.tools.create_valid_name(Header);
    % replace nan with 0
    data(isnan(data)) = 0; 
    % indicate timecolumn (hardcoded first --> can be changed)
    time=data(:,1); % column 2 = ProcTime/ 3 = InocTime
    starttimeindex = min(find(time));
    % cut of starttime
    time = time(starttimeindex:end)./24;
    data = data(starttimeindex:end, :);
    % initialize Bioprocess
    B = BPTT.Bioprocess;
    B.Name = [batch_name '_' Experiment];
    B.MetaData = MetaData;
    % loop through the headers to get the data for each signal
    for i=1:length(Header)
       data_idx = strcmp(Header,Header{i});
       d = data(:,data_idx);

       rawvar = BPTT.TimeVariable;
       varname = Header{i}; % remove the '-MEAS (HK)' string from the name of signals to avoid long signal names
       %varname = strrep(varname,'-','_');
       rawvar.Name = varname;
       rawvar.Type = 'raw';
       rawvar.Time = time;
       rawvar.Data = d;
       rawvar.DataInfo.Unit = BPTT.tools.remove_brackets_and_whitespace(Units{i});

       B.AddVariable(rawvar);

    end
    
    %%  import offline Data

    [data,Header]=xlsread(file_name,offlineraw);
    Header= Header (1,1:end);
    [Header, Units]= strtok(Header); 

    data(isnan(data)) = 0; 
    time=data(:,1)./24;

    % loop through the headers to get the data for each signal
    for i=1:length(Header)
       data_idx = strcmp(Header,Header{i});
       d = data(:,data_idx);

       rawvar = BPTT.TimeVariable;
       varname = Header{i}; % remove the '-MEAS (HK)' string from the name of signals to avoid long signal names
       %varname = strrep(varname,'-','_');
       rawvar.Name = varname;
       rawvar.Type = 'raw';
       rawvar.Time = time;%./24;
       rawvar.Data = d;
       rawvar.DataInfo.Unit = BPTT.tools.remove_brackets_and_whitespace(Units{i});

       B.AddVariable(rawvar);

    end
%%
%load models/BPT_BA
%run BPT_BA_offline
    %% save Bioprocess
    
    save([B.Name],'B')

 %%
 clear all
 clc
