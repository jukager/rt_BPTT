%% Bioprocess monitoring and control
%  configuration file for the P. chrysogenum fed-batch process 
% DASGip Setup for DJANGo
%(c) Julian Kager June 2016
%% Convert units of POX feed from g/h to l/h
% conversion is carried using the calc_math calculator

clear c
clear p

P.exp = 'x{1} .*0';                         % mathematical expression
P.output_name = 'O2';                     % specify the name of the output signal
P.output_units = 'l/min';                         % specify the units of the output signal
P.min = 0;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_O2_in';                  % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'AIR'}; % specify the names of the input signals

c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations


B.AddCalculator(c);

%% Example Calculators for online Calculation
% Offgas calculator

P.o2_in_o2 = 0.98;                              % Oxygen content in the oxygen supply tank
P.yO2AIR = 0.2095;                              % yO2AIR    [-]
P.yCO2AIR = 0.0004;                             % yCO2AIR   [-]
P.yO2wet = 0.207;                              % yO2wet    [-]
P.VnM = 22.414;                                 % VnM volume of O2 under normal conditions [l/mol] 
P.InputUnit = 'l/min';
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_offgas';                         % specify a name for the calculator
c.f = @BPTT.calculators.calc_offgas;            % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'AIR','O2','O','CO'};            % specify the names of the input signals

c.Mode = 'online';                              % calculation mode online vs offline
c.minpoints = 1;                                % minimum points to perform a partial calc.
c.pastpoints = 0;                               % number of historical points to consider when performing partial calculations

B.AddCalculator(c);

%% Feed rate calculation for glucose

clear c
clear P

P.SG_window = 10;                      % SG window for taking derivative in number of points - must be odd
P.SG_pol_order = 1;                             % SG polynomial order
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_Glucose_feedrate';    	% specify a name for the calculator
c.f = @BPTT.calculators.calc_sagofeed;          % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'WeightFeed'};           % specify the names of the input signals / for this calc. input must have units of g

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);


%% Convert units of glucose feed from g/h to l/h

clear c
clear p

P.exp = 'x{1} ./ 1150';                         % mathematical expression
P.output_name = 'glucose_feed';                 % specify the name of the output signal
P.output_units = 'l/h';                         % specify the units of the output signal
P.min = 0;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_glucose_feed';              % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'WeightFeed_feedrate'};                    % specify the names of the input signals

c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations

B.AddCalculator(c);

%bp.phaseplot('ProcTime','glucose_feed')

%% Feed rate calculation for Base

clear c
clear P

P.SG_window = 10;                      % SG window for taking derivative in number of points - must be odd
P.SG_pol_order = 1;                             % SG polynomial order
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_Base_feedrate';    	% specify a name for the calculator
c.f = @BPTT.calculators.calc_sagofeed;          % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = { 'WeightBase'};           % specify the names of the input signals / for this calc. input must have units of g

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);

clear c
clear p
%%
P.exp = 'x{1} ./ 950';                         % mathematical expression
P.output_name = 'base_feed';                     % specify the name of the output signal
P.output_units = 'l/h';                         % specify the units of the output signal
P.min = 0;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_nh3_feed';                  % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'WeightBase_feedrate'}; % specify the names of the input signals

c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations


B.AddCalculator(c);
%bp.phaseplot('ProcTime','pox_feed')

%% Convert units 
% conversion is carried using the calc_math calculator

clear c
clear p

P.exp = '1.5+x{1}./-950 +x{2}./-1150';                         % mathematical expression
P.output_name = 'Volume';                     % specify the name of the output signal
P.output_units = 'l';                         % specify the units of the output signal
P.interp1 = 'highest';
P.min = 0;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_Volume';                  % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'WeightBase','WeightFeed'}; % specify the names of the input signals

c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations


B.AddCalculator(c);

%% Convert units of glucose feed from g/h to l/h

clear c
clear p

P.exp = 'x{1} .* 0';                         % mathematical expression
P.output_name = 'Harvest';                 % specify the name of the output signal
P.output_units = 'l/h';                         % specify the units of the output signal
P.min = 0;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_Harvest';              % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'glucose_feed'};                    % specify the names of the input signals

c.Mode = 'online';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
c.minpoints = 1;                            % minimum points to perform a partial calc.
c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations

B.AddCalculator(c);

 %% model simulation calculator to get model outputs as BPTT timevariables
clear c P
%load models/JKA_model_V2

c = BPTT.Calculator;
c.Name = 'sim_calc';
c.calctimer = [0 0];
P.Prediction = 0; % prediction in hours with actual setpoints
P.Model = model; % set model
P.output_freq=[];
P.xinit = [0.65 20 1.4 1.5] ; % Set initial conditions
P.reftime = B.MinTime;
P.sim_Start= P.reftime;
P.output_error = ones(1,length(fieldnames(P.Model.y))) .* 0.0;
%P.u_opt = u_opt;
c.x = P.Model.u_data;
c.p = P;
c.f = @BPTT.calculators.calc_model_sim_conf;
c.Version= 2;
c.Mode = 'online';                              % calculation mode: online vs. offline
c.minpoints = 1;                                % minimum points to perform a partial calc.
c.pastpoints = 0;                               % number of historical points to consider when performing partial calculations

%B.AddCalculator(c);

%% Conver Feed
clear c P

P.exp = 'x{1}.*24';                             % mathematical expression
P.output_name = 'Feed1';                     % specify the name of the output signal
P.output_units = 'L/days';                         % specify the units of the output signal
%P.max = 4;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_substrate_feed_conv';            % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'glucose_feed'}; 

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);

%% Conver Feed
clear c P
P.exp = 'x{1}.*24';                             % mathematical expression
P.output_name = 'Feed2';                     % specify the name of the output signal
P.output_units = 'L/days';                         % specify the units of the output signal
P.max = 4;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_substrate_feed_conv2';            % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'base_feed'}; 

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);
%% Conver Feed
clear c P
P.exp = 'x{1}.*0';                             % mathematical expression
P.output_name = 'Feed3';                     % specify the name of the output signal
P.output_units = 'L/days';                         % specify the units of the output signal
P.max = 4;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_substrate_feed_conv3';            % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'glucose_feed'}; 

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);
%% Conver Feed
clear c P
P.exp = 'x{1}.*0';                             % mathematical expression
P.output_name = 'Feed4';                     % specify the name of the output signal
P.output_units = 'L/days';                         % specify the units of the output signal
P.max = 4;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_substrate_feed_conv4';            % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'glucose_feed'}; 

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);
%% Conver Feed
clear c P
P.exp = 'x{1}.*0';                             % mathematical expression
P.output_name = 'Feed5';                     % specify the name of the output signal
P.output_units = 'L/days';                         % specify the units of the output signal
P.max = 4;
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'calc_math_substrate_feed_conv5';            % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'glucose_feed'}; 

c.Mode = 'offline';                              % calculation mode online vs offline
B.AddCalculator(c);
%% calculate growth_rate

    P.output_name = 'spec. growth rate \mu';
    P.plot = 1;
    c = BPTT.Calculator;
    c.Name = 'calc_specific_growth_rate';
    c.f = @BPTT.calculators.calc_specific_growth_rate;
    c.p = P;
    c.x = {'CDW','Volume','Feed1','Feed2','Feed3','Feed4','Feed5'};

    c.Mode = 'offline';

    B.AddCalculator(c);
    clear c P ans

%% calculate qs

    P.output_name = 'qs';
    P.plot = 1;
    P.glc_feed_conc = 230; % g/L
    c = BPTT.Calculator;
    c.Name = 'calc_specific_qs';
    c.f = @BPTT.calculators.calc_basic_rate;
    c.p = P;
    c.x = {'Glucose','CDW','Volume','Feed1','Feed2','Feed3','Feed4','Feed5','spec_growth_rate_mu'};

    c.Mode = 'offline';

    B.AddCalculator(c);
    clear c P ans

%% calculate qp

    P.output_name = 'qp';
    P.plot = 1;
    P.glc_feed_conc = 0; % g/L
    c = BPTT.Calculator;
    c.Name = 'calc_specific_qs';
    c.f = @BPTT.calculators.calc_basic_rate;
    c.p = P;
    c.x = {'Ethanol','CDW','Volume','Feed1','Feed2','Feed3','Feed4','Feed5','spec_growth_rate_mu'};

    c.Mode = 'offline';

    B.AddCalculator(c);
    clear c P ans

%%    
feeds = {'spec_growth_rate_mu','qs','qp'};
    for index = 1:numel(feeds)
        P.exp = 'x{1}./24'; 
        P.interp1 = 'off';
        P.output_name = [feeds{index} '_h'];
        P.output_units = 'g/g/h';
        c = BPTT.Calculator;
        c.Name = ['calc_convert_feed_' feeds{index}];
        c.f = @BPTT.calculators.calc_math;
        c.p = P;
        c.x = feeds(index);

        c.Mode = 'offline';
        c.minpoints = 1;
        c.pastpoints = 0;

        B.AddCalculator(c);
        clear c P ans
    end  
    