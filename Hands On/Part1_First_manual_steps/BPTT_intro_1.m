% create bioprocess object
MYBP = BPTT.Bioprocess

%% create TimeVariable object
 pH = BPTT.TimeVariable
 pH.Data = [ 7.1 7.05 7.03 7.12]'
 pH.Time = [ 1 1.5 4 5] % i am always days
 pH.Name = 'pH'
 pH.DataInfo.Unit = 'pH'
 
 Temp = BPTT.TimeVariable
 Temp.Data = [ 37 34 35 36]'
 Temp.Time = [ 1 1.5 4 5]
 Temp.Name = 'Temp'
 Temp.DataInfo.Unit = '�C'
 
 % adding a feed
 N_Feed = BPTT.TimeVariable
 N_Feed.Time = [0:0.1:8]
 N_Feed.Data = ones((length(N_Feed.Time)),1).*0.05
 N_Feed.Name = 'N_Feed'
 N_Feed.DataInfo.Unit = 'l/h' 
 
 SubstrateFeed_spt = BPTT.TimeVariable
 SubstrateFeed_spt.Time = [0:0.01:8]
 SubstrateFeed_spt.Data = ones((length(SubstrateFeed_spt.Time)),1).*0.5
 SubstrateFeed_spt.Name = 'SubstrateFeed_spt'
 SubstrateFeed_spt.DataInfo.Unit = 'l/h'
 
 SubstrateFeed = BPTT.TimeVariable
 SubstrateFeed.Time = [0:0.1:8]
 SubstrateFeed.Data = rand((length(SubstrateFeed.Time)),1)
 SubstrateFeed.Name = 'SubstrateFeed'
 SubstrateFeed.DataInfo.Unit = 'l/h'
 
 
 
 %% adding offline measurements
 CDW = BPTT.TimeVariable
 CDW.Data = [ 0 5 10 17 24 29 30 35 40]' 
 CDW.Time = [ 0 1 2 3 4 5 6 7 8 ]
 CDW.sdev = rand((length(CDW.Time)),1)
 CDW.Name = 'CDW'
 CDW.DataInfo.Unit = 'g'
 
 N = BPTT.TimeVariable
 N.Data = [ 18 15 10 7 6 5 5 4 2]' 
 N.Time = [ 0 1 2 3 4 5 6 7 8 ]
 N.sdev = rand((length(N.Time)),1)
 N.Name = 'N'
 N.DataInfo.Unit = 'g'
 
 % Add TimeVariable object to bioprocess
MYBP.AddVariable(pH)
MYBP.AddVariable(Temp)
MYBP.AddVariable(N_Feed)
MYBP.AddVariable(SubstrateFeed_spt)
MYBP.AddVariable(SubstrateFeed)
MYBP.AddVariable(CDW)
MYBP.AddVariable(N)
  
%% Bioprocess Object offers useful functions
 % some plotting
MYBP.plot('SubstrateFeed')

MYBP.phaseplot('SubstrateFeed','CDW')
MYBP.plot_sdev('CDW','o')
%MYBP.plot_timecoverage 
 
%% error evaluation
MYBP.RMSE('SubstrateFeed_spt','SubstrateFeed')

% ConvertTime
MYBP.ConvertTime %convert always back to days (all functions assume time to be in days)
% CopyBioprocess
MYBP_cut = MYBP.CopyBioprocess(1, 6,'raw','all')
MYBP_cut.plot('SubstrateFeed')

 %% Adding a calculator
 % SIMPLE CALCULATION
clear c
clear p

P.exp = 'x{2}-x{1}' ;                         % mathematical expression
%P.interp1 = 'highest'
P.output_name = 'SubstrateFeed_residuals';                 % specify the name of the output signal
P.output_units = 'l/h';                         % specify the units of the output signal
c = BPTT.Calculator;                            % define a new Calculator object
c.Name = 'residual_calc';              % specify a name for the calculator
c.f = @BPTT.calculators.calc_math;              % specify which function file to use for calculating outputs
c.p = P;                                        % assign parameter values
c.x = {'SubstrateFeed_spt','SubstrateFeed'};                    % specify the names of the input signals

c.Mode = 'offline';                          % calculation mode: online vs. offline
%c.deadband= 300;                            % calculation interval in [s]
%c.minpoints = 1;                            % minimum points to perform a partial calc.
%c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations

MYBP.AddCalculator(c);
MYBP.plot('SubstrateFeed_residuals','x')
%% model simulation calculator to get model outputs as BPTT timevariables
clear c P
% Define calculator function specific paramaters 
%load models/TrendsinBiotech %Load your model
%model.simmode = 'memory'    % memory --> no function compiler (slow, but safe), Matlab function compiler (fast, recompilation needed when model is changed)
%
P.Prediction = 24;                          % prediction horizon in hours
P.Model = model;                            % set model
          % V X S 
P.xinit = [20 5 2]  ;     % initial conditions at process start
P.reftime = 0;                              % reference time of process start [days] -> needed if times are in days since year O
P.sim_Start = 0;                            % simulation startpoint according to xinit [hours]
P.output_error = ones(1,length(fieldnames(P.Model.y))) .* 0.0; % simulate noise for model outputs (insilico data generation/ deactive with ones)
P.output_freq=[];                           % not enabled fixed value to 0.2 h

% Define calculator properties
c = BPTT.Calculator;
c.Name = 'sim_calc';                        % calculator name
c.calctimer = [0 0];                        % calctimer reset
c.x = {'SubstrateFeed'};           % define inputs -> derive from model struct -> rename if necceessary
c.p = P;                                    % stores parameters in calc obj
c.f = @BPTT.calculators.calc_model_sim_conf; % function handle to sim function

c.Mode = 'offline';                          % calculation mode: online vs. offline
%c.deadband= 5*60;                            % calculation interval in [s] higest hierachy
%c.minpoints = 3;                            % minimum points to perform a partial calc.
%c.pastpoints = 0;                           % number of historical points to consider when performing partial calculations

MYBP.AddCalculator(c);